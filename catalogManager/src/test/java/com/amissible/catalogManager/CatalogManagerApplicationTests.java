package com.amissible.catalogManager;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "E:\\JavaCodes\\Amissible\\ecommerce\\catalogManager\\src\\test\\java\\Features\\Catalog.feature",
		glue={"StepDefn"},
		plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"})
class CatalogManagerApplicationTests {

}
