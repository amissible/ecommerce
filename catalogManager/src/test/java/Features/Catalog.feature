Feature: Catalog Feature
  Scenario: Catalog test
    Given the website is up
    When the url is http://localhost:8082/db/catalog and response is 200
    When the url is http://localhost:8082/db/getHeaderMenu and response is 200
    When the url is http://localhost:8082/db/getFooterMenu and response is 200
    When the url is http://localhost:8082/db/quickview and response is 200
    When the url is http://localhost:8082/db/search and response is 200
    Then display returned response