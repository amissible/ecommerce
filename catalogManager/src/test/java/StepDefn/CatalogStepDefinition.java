package StepDefn;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class CatalogStepDefinition {

    WebDriver driver;
    RestTemplate restTemplate = new RestTemplate();

    @Given("the website is up")
    public void userLocation() {
        //
    }

    @When("the url is http:\\\\/\\\\/localhost:{int}\\\\/db\\\\/getHeaderMenu and response is {int}")
    public void header(Integer int1, Integer int2) {
        ResponseEntity<String> res = restTemplate.getForEntity("http://localhost:8082/db/getHeaderMenu", String.class);
        Assert.assertEquals(HttpStatus.OK, res.getStatusCode());
    }

    @When("the url is http:\\\\/\\\\/localhost:{int}\\\\/db\\\\/getFooterMenu and response is {int}")
    public void footer(Integer int1, Integer int2) {
        ResponseEntity<String> res = restTemplate.getForEntity("http://localhost:8082/db/getFooterMenu", String.class);
        Assert.assertEquals(HttpStatus.OK, res.getStatusCode());
    }

    @When("the url is http:\\\\/\\\\/localhost:{int}\\\\/db\\\\/catalog and response is {int}")
    public void catalog(Integer int1, Integer int2) {
        ResponseEntity<String> res = restTemplate.getForEntity("http://localhost:8082/db/catalog", String.class);
        Assert.assertEquals(HttpStatus.OK, res.getStatusCode());
    }

    @When("the url is http:\\\\/\\\\/localhost:{int}\\\\/db\\\\/quickview and response is {int}")
    public void quickview(Integer int1, Integer int2) {
        ResponseEntity<String> res = restTemplate.getForEntity("http://localhost:8082/db//quickview/blue-sanitizer-bracelet", String.class);
        Assert.assertEquals(HttpStatus.OK, res.getStatusCode());
    }

    @When("the url is http:\\\\/\\\\/localhost:{int}\\\\/db\\\\/search and response is {int}")
    public void search(Integer int1, Integer int2) {
        ResponseEntity<String> res = restTemplate.getForEntity("http://localhost:8082/db/search/blue-bracelet", String.class);
        Assert.assertEquals(HttpStatus.OK, res.getStatusCode());
    }

    @Then("display returned response")
    public void display() {

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Sparkie\\Downloads\\chromedriver1.exe");
        driver = new ChromeDriver();
        driver.get("http://localhost:8082/db/catalog");
    }
}
