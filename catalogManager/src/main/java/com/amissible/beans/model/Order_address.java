package com.amissible.beans.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Order_address {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long order_id;
    private String addr_type;
    private String display_name;
    private String company;
    private String country;
    private String state;
    private String city;
    private String postcode;
    private String line1;
    private String line2;
    private String contact_cc;
    private String contact_cn;
    private String email;

    public Order_address() {
    }

    public Order_address(Long id, Long order_id, String addr_type, String display_name, String company, String country, String state, String city, String postcode, String line1, String line2, String contact_cc, String contact_cn, String email) {
        this.id = id;
        this.order_id = order_id;
        this.addr_type = addr_type;
        this.display_name = display_name;
        this.company = company;
        this.country = country;
        this.state = state;
        this.city = city;
        this.postcode = postcode;
        this.line1 = line1;
        this.line2 = line2;
        this.contact_cc = contact_cc;
        this.contact_cn = contact_cn;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrder_id() {
        return order_id;
    }

    public void setOrder_id(Long order_id) {
        this.order_id = order_id;
    }

    public String getAddr_type() {
        return addr_type;
    }

    public void setAddr_type(String addr_type) {
        this.addr_type = addr_type;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getContact_cc() {
        return contact_cc;
    }

    public void setContact_cc(String contact_cc) {
        this.contact_cc = contact_cc;
    }

    public String getContact_cn() {
        return contact_cn;
    }

    public void setContact_cn(String contact_cn) {
        this.contact_cn = contact_cn;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Order_address{" +
                "id=" + id +
                ", order_id=" + order_id +
                ", addr_type='" + addr_type + '\'' +
                ", display_name='" + display_name + '\'' +
                ", company='" + company + '\'' +
                ", country='" + country + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", postcode='" + postcode + '\'' +
                ", line1='" + line1 + '\'' +
                ", line2='" + line2 + '\'' +
                ", contact_cc='" + contact_cc + '\'' +
                ", contact_cn='" + contact_cn + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
