package com.amissible.beans.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Product_price {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long product_id;
    private String currency;
    private Float marked_price;
    private Float sale_price;

    public Product_price() {
    }

    public Product_price(Long id, Long product_id, String currency, Float marked_price, Float sale_price) {
        this.id = id;
        this.product_id = product_id;
        this.currency = currency;
        this.marked_price = marked_price;
        this.sale_price = sale_price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Float getMarked_price() {
        return marked_price;
    }

    public void setMarked_price(Float marked_price) {
        this.marked_price = marked_price;
    }

    public Float getSale_price() {
        return sale_price;
    }

    public void setSale_price(Float sale_price) {
        this.sale_price = sale_price;
    }

    @Override
    public String toString() {
        return "Product_price{" +
                "id=" + id +
                ", product_id=" + product_id +
                ", currency='" + currency + '\'' +
                ", marked_price=" + marked_price +
                ", sale_price=" + sale_price +
                '}';
    }
}
