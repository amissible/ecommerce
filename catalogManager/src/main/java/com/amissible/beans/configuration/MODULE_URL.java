package com.amissible.beans.configuration;

public class MODULE_URL {
    public static String CATALOG_BASE_URL = "http://www.amissible.com:8082/";
    public static String PAYMENT_BASE_URL = "http://www.amissible.com:8083/";
    public static String ORDER_BASE_URL = "http://www.amissible.com:8085/";
    public static String ACCOUNT_BASE_URL = "http://www.amissible.com:8086/";
}
