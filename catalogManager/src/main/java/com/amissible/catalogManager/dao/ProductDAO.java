package com.amissible.catalogManager.dao;

import com.amissible.beans.exception.AppException;
import com.amissible.beans.model.Customer;
import com.amissible.beans.model.Product;
import com.amissible.catalogManager.dao_interface.ProductInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class ProductDAO implements ProductInterface {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Product> getAllProducts(){
        List<Product> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM product", new Object[] { },
                (rs, rowNum) -> new Product(
                        rs.getLong("id"),
                        rs.getString("product_name"),
                        rs.getString("alias"),
                        rs.getString("brand"),
                        rs.getString("category"),
                        rs.getString("url"),
                        rs.getString("icon1_path"),
                        rs.getString("icon2_path"),
                        rs.getString("image_path"),
                        rs.getString("datetimestamp"),
                        rs.getString("product_status")
                )
        ).forEach(record -> list.add(record));
        return list;
    }

    public Long getProductIdByAlias(String alias){
        List<Product> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT id FROM product WHERE alias = ?", new Object[] { alias },
                (rs, rowNum) -> new Product(
                        rs.getLong("id")
                )
        ).forEach(record -> list.add(record));
        if (list.size() == 0)
            return 0L;
        else
            return list.get(0).getId();
    }

    public Product getProductById(Long id){
        List<Product> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM product WHERE id = ?", new Object[] { id },
                (rs, rowNum) -> new Product(
                        rs.getLong("id"),
                        rs.getString("product_name"),
                        rs.getString("alias"),
                        rs.getString("brand"),
                        rs.getString("category"),
                        rs.getString("url"),
                        rs.getString("icon1_path"),
                        rs.getString("icon2_path"),
                        rs.getString("image_path"),
                        rs.getString("datetimestamp"),
                        rs.getString("product_status")
                )
        ).forEach(record -> list.add(record));
        if (list.size() == 0)
            return null;
        else
            return list.get(0);
    }

    public List<Product> getProductByIdList(List<Long> id){
        List<Product> list = new ArrayList<>();
        String inSql = String.join(",", Collections.nCopies(id.size(), "?"));
        jdbcTemplate.query(
                "SELECT * FROM product WHERE  id IN ("+inSql+")", id.toArray(),
                (rs, rowNum) -> new Product(
                        rs.getLong("id"),
                        rs.getString("product_name"),
                        rs.getString("alias"),
                        rs.getString("brand"),
                        rs.getString("category"),
                        rs.getString("url"),
                        rs.getString("icon1_path"),
                        rs.getString("icon2_path"),
                        rs.getString("image_path"),
                        rs.getString("datetimestamp"),
                        rs.getString("product_status")
                )
        ).forEach(record -> list.add(record));
        return list;
    }

    public Product getProductByAlias(String alias){
        List<Product> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM product WHERE alias = ?", new Object[] { alias },
                (rs, rowNum) -> new Product(
                        rs.getLong("id"),
                        rs.getString("product_name"),
                        rs.getString("alias"),
                        rs.getString("brand"),
                        rs.getString("category"),
                        rs.getString("url"),
                        rs.getString("icon1_path"),
                        rs.getString("icon2_path"),
                        rs.getString("image_path"),
                        rs.getString("datetimestamp"),
                        rs.getString("product_status")
                )
        ).forEach(record -> list.add(record));
        if (list.size() == 0)
            return null;
        else
            return list.get(0);
    }

    public Long insertProduct(String product_name,String alias,String brand,String category,String url,
                              String icon1_path,String icon2_path,String image_path,String product_status){
        if(getProductByAlias(alias) != null)
            throw new AppException("Product Already Present",HttpStatus.CONFLICT);
        jdbcTemplate.update(
                "INSERT INTO product(product_name,alias,brand,category,url,icon1_path,icon2_path,image_path,product_status) VALUES (?,?,?,?,?,?,?,?,?)",
                new Object[] {
                        product_name,alias,brand,category,url,
                        icon1_path,icon2_path,image_path,product_status
                });
        return getProductByAlias(alias).getId();
    }

    public Long getCount(){
        List<Product> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT COUNT(id) AS product_count FROM product", new Object[] { },
                (rs, rowNum) -> new Product(
                        rs.getLong("product_count")
                )
        ).forEach(record -> list.add(record));
        return list.get(0).getId();
    }
}
