package com.amissible.catalogManager.dao;

import com.amissible.beans.exception.AppException;
import com.amissible.beans.model.Category;
import com.amissible.beans.model.Menu;
import com.amissible.catalogManager.dao_interface.MenuInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MenuDAO implements MenuInterface {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Menu> getAllMenuRecords(){
        List<Menu> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM menu", new Object[] {  },
                (rs, rowNum) -> new Menu(
                        rs.getLong("id"),
                        rs.getString("item_name"),
                        rs.getString("icon"),
                        rs.getString("route"),
                        rs.getLong("parent_id"),
                        rs.getInt("sort_order")
                )
        ).forEach(record -> list.add(record));
        return list;
    }

    public Menu getMenuById(Long id){
        List<Menu> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM menu WHERE id = ?", new Object[] { id },
                (rs, rowNum) -> new Menu(
                        rs.getString("item_name"),
                        rs.getString("icon"),
                        rs.getString("route"),
                        rs.getLong("parent_id"),
                        rs.getInt("sort_order")
                )
        ).forEach(record -> list.add(record));
        if (list.size() == 0)
            return null;
        else
            return list.get(0);
    }

    public Menu getMenuByItemName(String item_name){
        List<Menu> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM menu WHERE item_name = ?", new Object[] { item_name },
                (rs, rowNum) -> new Menu(
                        rs.getLong("id"),
                        rs.getString("item_name"),
                        rs.getString("icon"),
                        rs.getString("route"),
                        rs.getLong("parent_id"),
                        rs.getInt("sort_order")
                )
        ).forEach(record -> list.add(record));
        if (list.size() == 0)
            return null;
        else
            return list.get(0);
    }

    public List<Menu> getMenuListByParentId(Long parent_id){
        List<Menu> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM menu WHERE parent_id = ? ORDER BY sort_order ASC", new Object[] { parent_id },
                (rs, rowNum) -> new Menu(
                        rs.getLong("id"),
                        rs.getString("item_name"),
                        rs.getString("icon"),
                        rs.getString("route"),
                        rs.getLong("parent_id"),
                        rs.getInt("sort_order")
                )
        ).forEach(record -> list.add(record));
        if (list.size() == 0)
            return null;
        else
            return list;
    }

    public Long insertMenu(String item_name,String icon,String route,Long parent_id,Integer sort_order){
        if(parent_id != 0L && getMenuById(parent_id) == null)
            throw new AppException("Parent Menu not found", HttpStatus.NOT_FOUND);
        if(getMenuByItemName(item_name) != null)
            throw new AppException("Menu Already Present",HttpStatus.CONFLICT);
        jdbcTemplate.update(
                "INSERT INTO menu(item_name,icon,route,parent_id,sort_order) VALUES (?,?,?,?,?)",
                new Object[] {
                        item_name,
                        icon,
                        route,
                        parent_id,
                        sort_order
                });
        return getMenuByItemName(item_name).getId();
    }

    //Update and Delete are pending


}
