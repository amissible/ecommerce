package com.amissible.catalogManager.dao;

import com.amissible.beans.exception.AppException;
import com.amissible.beans.model.Category;
import com.amissible.beans.model.Product;
import com.amissible.catalogManager.dao_interface.CategoryInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryDAO implements CategoryInterface {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public Category getCategoryById(Long id){
        List<Category> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT category_name,parent_id,sort_order FROM category WHERE id = ?", new Object[] { id },
                (rs, rowNum) -> new Category(
                        rs.getString("category_name"),
                        rs.getLong("parent_id"),
                        rs.getInt("sort_order")
                )
        ).forEach(record -> list.add(record));
        if (list.size() == 0)
            return null;
        else
            return list.get(0);
    }

    public List<Category> getCategoryListByParentId(Long parent_id){
        List<Category> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT id, category_name,sort_order FROM category WHERE parent_id = ?", new Object[] { parent_id },
                (rs, rowNum) -> new Category(
                        rs.getLong("id"),
                        rs.getString("category_name"),
                        rs.getInt("sort_order")
                )
        ).forEach(record -> list.add(record));
        if (list.size() == 0)
            return null;
        else
            return list;
    }

    public Category getCategoryByCategoryName(String category){
        List<Category> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT id,parent_id,sort_order FROM category WHERE category_name = ?", new Object[] { category },
                (rs, rowNum) -> new Category(
                        rs.getLong("id"),
                        rs.getLong("parent_id"),
                        rs.getInt("sort_order")
                )
        ).forEach(record -> list.add(record));
        if (list.size() == 0)
            return null;
        else
            return list.get(0);
    }

    public Long insertCategory(String category_name,Long parent_id,Integer sort_order){
        if(parent_id != 0L && getCategoryById(parent_id) == null)
            throw new AppException("Parent Category not found", HttpStatus.NOT_FOUND);
        if(getCategoryByCategoryName(category_name) != null)
            throw new AppException("Category Already Present",HttpStatus.CONFLICT);
        jdbcTemplate.update(
                "INSERT INTO category(category_name,parent_id,sort_order) VALUES (?,?,?)",
                new Object[] {
                        category_name,
                        parent_id,
                        sort_order
                });
        return getCategoryByCategoryName(category_name).getId();
    }

    public Boolean updateCategoryName(Long id,String category_name){
        if(getCategoryById(id) == null)
            throw new AppException("Category not found", HttpStatus.NOT_FOUND);
        jdbcTemplate.update(
                "UPDATE category SET category_name = ? WHERE id = ?",
                new Object[] {
                        category_name,
                        id
                });
        return true;
    }

    public Boolean updateCategoryParent(Long id,Long parent_id){
        if(getCategoryById(id) == null)
            throw new AppException("Category not found", HttpStatus.NOT_FOUND);
        if(getCategoryById(parent_id) == null)
            throw new AppException("Parent Category not found", HttpStatus.NOT_FOUND);
        jdbcTemplate.update(
                "UPDATE category SET parent_id = ? WHERE id = ?",
                new Object[] {
                        parent_id,
                        id
                });
        return true;
    }

    public Boolean updateCategorySortOrder(Long id,Integer sort_order){
        if(getCategoryById(id) == null)
            throw new AppException("Category not found", HttpStatus.NOT_FOUND);
        jdbcTemplate.update(
                "UPDATE category SET sort_order = ? WHERE id = ?",
                new Object[] {
                        sort_order,
                        id
                });
        return true;
    }

    public Boolean deleteCategory(Long id){
        if(getCategoryById(id) == null)
            throw new AppException("Category not found", HttpStatus.NOT_FOUND);
        if(getCategoryListByParentId(id) != null)
            throw new AppException("Child Category detected", HttpStatus.NOT_FOUND);
        jdbcTemplate.update(
                "DELETE FROM category WHERE id = ?",
                new Object[] {
                        id
                });
        return true;
    }

}
