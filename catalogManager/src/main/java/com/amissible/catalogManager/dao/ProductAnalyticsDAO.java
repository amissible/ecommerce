package com.amissible.catalogManager.dao;

import com.amissible.beans.model.ProductAnalytics;
import com.amissible.catalogManager.dao_interface.ProductAnalyticsInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class ProductAnalyticsDAO implements ProductAnalyticsInterface {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public ProductAnalytics getAnalyticsByProductId(Long id){
        List<ProductAnalytics> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM product_analytics WHERE product_id = ?", new Object[] { id },
                (rs, rowNum) -> new ProductAnalytics(
                        rs.getLong("product_id"),
                        rs.getLong("hits"),
                        rs.getFloat("average_rating"),
                        rs.getString("search")
                )
        ).forEach(record -> list.add(record));
        if(list.size() == 0)
            return null;
        else
            return list.get(0);
    }

    public List<ProductAnalytics>  getAllProductAnalytics(){
        List<ProductAnalytics> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM product_analytics", new Object[] { },
                (rs, rowNum) -> new ProductAnalytics(
                        rs.getLong("product_id"),
                        rs.getLong("hits"),
                        rs.getFloat("average_rating"),
                        rs.getString("search")
                )
        ).forEach(record -> list.add(record));
        return list;
    }

    public List<Long> getProductBySearchQuery(String query){
        query = "%" + query + "%";
        List<Long> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT product_id FROM product_analytics WHERE search LIKE ?", new Object[] { query },
                (rs, rowNum) -> list.add(rs.getLong("product_id"))
        );
        return list;
    }

    public List<Long> getProductBySearchQueryArray(String query[]){
        String inSql = String.join("OR", Collections.nCopies(query.length, " search LIKE ? "));
        inSql = inSql + " ORDER BY average_rating DESC,hits DESC";
        for(int i=0;i<query.length;i++)
            query[i] = "%" + query[i] + "%";
        List<Long> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT product_id FROM product_analytics WHERE"+inSql, query,
                (rs, rowNum) -> list.add(rs.getLong("product_id"))
        );
        return list;
    }

    public List<Long> getProductBySearchQueryArrayHard(String query[]){
        String inSql = String.join("AND", Collections.nCopies(query.length, " search LIKE ? "));
        inSql = inSql + " ORDER BY average_rating DESC,hits DESC";
        for(int i=0;i<query.length;i++)
            query[i] = "%" + query[i] + "%";
        List<Long> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT product_id FROM product_analytics WHERE"+inSql, query,
                (rs, rowNum) -> list.add(rs.getLong("product_id"))
        );
        return list;
    }



    public void insertProductAnalytics(Long product_id,Long hits,Float average_rating,String search){
        jdbcTemplate.update(
                "INSERT INTO product_analytics(product_id,hits,average_rating,search) VALUES (?,?,?,?)",
                new Object[] {
                        product_id,hits,average_rating,search
                });
    }
}
