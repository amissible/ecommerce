package com.amissible.catalogManager.dao;

import com.amissible.beans.model.Product;
import com.amissible.catalogManager.dao_interface.ProductInventoryInterface;
import com.amissible.catalogManager.model.Inventory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductInventoryDAO implements ProductInventoryInterface {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Inventory> getInventoryDetailsByProductId(Long id){
        List<Inventory> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM product_inventory a INNER JOIN product_source b ON a.source_id=b.id WHERE a.product_id = ?", new Object[] { id },
                (rs, rowNum) -> new Inventory(
                        rs.getLong("product_id"),
                        rs.getInt("quantity"),
                        rs.getString("source_name"),
                        rs.getString("country"),
                        rs.getString("state"),
                        rs.getString("city"),
                        rs.getString("postcode"),
                        rs.getString("line1"),
                        rs.getString("line2"),
                        rs.getString("contact_cc"),
                        rs.getString("contact_cn"),
                        rs.getString("email")
                )
        ).forEach(record -> list.add(record));
        return list;
    }

    public List<Inventory> getAllInventorys(){
        List<Inventory> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM product_inventory a INNER JOIN product_source b ON a.source_id=b.id ", new Object[] {},
                (rs, rowNum) -> new Inventory(
                        rs.getLong("product_id"),
                        rs.getInt("quantity"),
                        rs.getString("source_name"),
                        rs.getString("country"),
                        rs.getString("state"),
                        rs.getString("city"),
                        rs.getString("postcode"),
                        rs.getString("line1"),
                        rs.getString("line2"),
                        rs.getString("contact_cc"),
                        rs.getString("contact_cn"),
                        rs.getString("email")
                )
        ).forEach(record -> list.add(record));
        return list;
    }

    public Integer getInventoryCountByProductId(Long id){
        List<Inventory> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT SUM(a.quantity) AS total FROM product_inventory a INNER JOIN product_source b ON a.source_id=b.id WHERE a.product_id = ?", new Object[] { id },
                (rs, rowNum) -> new Inventory(
                        rs.getInt("total")
                )
        ).forEach(record -> list.add(record));
        return list.get(0).getQuantity();
    }


    public void insertProductInventoryDU(Long product_id,Long source_id,Integer quantity){
        jdbcTemplate.update(
                "INSERT INTO product_inventory(product_id,source_id,quantity) VALUES (?,?,?)",
                new Object[] {
                        product_id,source_id,quantity
                });
    }
}
