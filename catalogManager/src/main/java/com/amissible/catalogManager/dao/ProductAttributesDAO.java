package com.amissible.catalogManager.dao;

import com.amissible.catalogManager.dao_interface.ProductAttributesInterface;
import com.amissible.catalogManager.model.Attribute;
import com.amissible.catalogManager.model.Info;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductAttributesDAO implements ProductAttributesInterface {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Attribute> getAttributeListByProductId(Long id){
        List<Attribute> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM product_attributes WHERE product_id = ?", new Object[] { id },
                (rs, rowNum) -> new Attribute(
                        rs.getLong("product_id"),
                        rs.getString("attribute"),
                        rs.getString("attr_value")
                )
        ).forEach(record -> list.add(record));
        if(list.size() == 0)
            return null;
        else
            return list;
    }

    public List<Attribute> getAttributeListByProductId(){
        List<Attribute> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM product_attributes ", new Object[] {  },
                (rs, rowNum) -> new Attribute(
                        rs.getLong("product_id"),
                        rs.getString("attribute"),
                        rs.getString("attr_value")
                )
        ).forEach(record -> list.add(record));
        if(list.size() == 0)
            return null;
        else
            return list;
    }

    public void insertProductAttributeDU(Long product_id,String attribute,String attr_value){
        jdbcTemplate.update(
                "INSERT INTO product_attributes(product_id,attribute,attr_value) VALUES (?,?,?)",
                new Object[] {
                        product_id,attribute,attr_value
                });
    }
}
