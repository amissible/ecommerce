package com.amissible.catalogManager.dao;

import com.amissible.catalogManager.dao_interface.ProductReviewsInterface;
import com.amissible.catalogManager.model.Info;
import com.amissible.catalogManager.model.Review;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductReviewsDAO implements ProductReviewsInterface {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Review> getReviewByProductId(Long id,Integer count){
        List<Review> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT a.rating ,a.review,a.image_path,a.datetimestamp AS dt,b.first_name,b.last_name,b.image FROM product_reviews a INNER JOIN customer b ON a.user_id=b.id WHERE product_id = ? LIMIT ?", new Object[] { id, count},
                (rs, rowNum) -> new Review(
                        rs.getString("image"),
                        rs.getString("first_name")+" "+rs.getString("last_name"),
                        rs.getString("dt"),
                        rs.getFloat("rating"),
                        rs.getString("review"),
                        rs.getString("image_path")
                )
        ).forEach(record -> list.add(record));
        if(list.size() == 0)
            return null;
        else
            return list;
    }
}
