package com.amissible.catalogManager.dao;

import com.amissible.catalogManager.dao_interface.ProductInfoInterface;
import com.amissible.catalogManager.model.Info;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductInfoDAO implements ProductInfoInterface {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Info> getInfoListByProductId(Long id,Integer count){
        List<Info> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM product_info WHERE product_id = ? LIMIT ?", new Object[] { id, count},
                (rs, rowNum) -> new Info(
                        rs.getLong("product_id"),
                        rs.getString("info_head"),
                        rs.getString("info_body")
                )
        ).forEach(record -> list.add(record));
        return list;
    }

    public List<Info> getAllProductInfos(){
        List<Info> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM product_info ", new Object[] { },
                (rs, rowNum) -> new Info(
                        rs.getLong("product_id"),
                        rs.getString("info_head"),
                        rs.getString("info_body")
                )
        ).forEach(record -> list.add(record));
        return list;
    }


    public void insertProductInfoDU(Long product_id,String info_head,String info_body){
        jdbcTemplate.update(
                "INSERT INTO product_info(product_id,info_head,info_body) VALUES (?,?,?)",
                new Object[] {
                        product_id,info_head,info_body
                });
    }
}
