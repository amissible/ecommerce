package com.amissible.catalogManager.dao;

import com.amissible.catalogManager.dao_interface.ProductPriceInterface;
import com.amissible.catalogManager.model.Inventory;
import com.amissible.catalogManager.model.Price;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class ProductPriceDAO implements ProductPriceInterface {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Price> getPriceListByProductId(Long id){
        List<Price> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM product_price WHERE product_id = ?", new Object[] { id },
                (rs, rowNum) -> new Price(
                        rs.getLong("product_id"),
                        rs.getString("currency"),
                        rs.getFloat("marked_price"),
                        rs.getFloat("sale_price")
                )
        ).forEach(record -> list.add(record));
        return list;
    }

    public void insertProductPriceDU(Long product_id,String currency,Float marked_price,Float sale_price){
        jdbcTemplate.update(
                "INSERT INTO product_price(product_id,currency,marked_price,sale_price) VALUES (?,?,?,?)",
                new Object[] {
                        product_id,currency,marked_price,sale_price
                });
    }

    public List<Price> getAllPrices() {
        List<Price> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM product_price ", new Object[] { },
                (rs, rowNum) -> new Price(
                        rs.getLong("product_id"),
                        rs.getString("currency"),
                        rs.getFloat("marked_price"),
                        rs.getFloat("sale_price")
                )
        ).forEach(record -> list.add(record));
        return list;
    }
}
