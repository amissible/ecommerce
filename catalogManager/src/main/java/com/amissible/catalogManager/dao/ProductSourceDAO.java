package com.amissible.catalogManager.dao;

import com.amissible.beans.exception.AppException;
import com.amissible.beans.model.Category;
import com.amissible.beans.model.Product_source;
import com.amissible.catalogManager.dao_interface.ProductSourceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductSourceDAO implements ProductSourceInterface {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public Product_source getProductSourceById(Long id){
        List<Product_source> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM product_source WHERE id = ?", new Object[] { id },
                (rs, rowNum) -> new Product_source(
                        rs.getString("source_name"),
                        rs.getString("country"),
                        rs.getString("state"),
                        rs.getString("city"),
                        rs.getString("postcode"),
                        rs.getString("line1"),
                        rs.getString("line2"),
                        rs.getString("contact_cc"),
                        rs.getString("contact_cn"),
                        rs.getString("email")
                )
        ).forEach(record -> list.add(record));
        if (list.size() == 0)
            return null;
        else
            return list.get(0);
    }

    public Product_source getProductSourceBySourceName(String source_name){
        List<Product_source> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM product_source WHERE source_name = ?", new Object[] { source_name },
                (rs, rowNum) -> new Product_source(
                        rs.getLong("id"),
                        rs.getString("source_name"),
                        rs.getString("country"),
                        rs.getString("state"),
                        rs.getString("city"),
                        rs.getString("postcode"),
                        rs.getString("line1"),
                        rs.getString("line2"),
                        rs.getString("contact_cc"),
                        rs.getString("contact_cn"),
                        rs.getString("email")
                )
        ).forEach(record -> list.add(record));
        if (list.size() == 0)
            return null;
        else
            return list.get(0);
    }

    public Long insertProductSource(String source_name,String country,String state,String city,String postcode,
                                    String line1,String line2,String contact_cc,String contact_cn,String email){
        if(getProductSourceBySourceName(source_name) != null)
            throw new AppException("Product Source Already Present",HttpStatus.CONFLICT);
        jdbcTemplate.update(
                "INSERT INTO product_source(source_name,country,state,city,postcode,line1,line2,contact_cc,contact_cn,email) VALUES (?,?,?,?,?,?,?,?,?,?)",
                new Object[] {
                        source_name,
                        country,
                        state,
                        city,
                        postcode,
                        line1,
                        line2,
                        contact_cc,
                        contact_cn,
                        email
                });
        return getProductSourceBySourceName(source_name).getId();
    }
}
