package com.amissible.catalogManager.service;

import com.amissible.beans.exception.AppException;
import com.amissible.beans.model.*;
import com.amissible.catalogManager.dao.*;
import com.amissible.catalogManager.model.*;
import com.opencsv.CSVWriter;
import static org.apache.logging.log4j.util.Strings.EMPTY;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class CatalogService {

    private static final Logger logger = LoggerFactory.getLogger(CatalogService.class);

    @Autowired
    private CategoryDAO categoryDAO;

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private ProductInventoryDAO productInventoryDAO;

    @Autowired
    private ProductPriceDAO productPriceDAO;

    @Autowired
    private ProductAnalyticsDAO productAnalyticsDAO;

    @Autowired
    private ProductInfoDAO productInfoDAO;

    @Autowired
    private ProductAttributesDAO productAttributesDAO;

    @Autowired
    private ProductReviewsDAO productReviewsDAO;

    @Autowired
    private ProductSourceDAO productSourceDAO;

    @Autowired
    private MenuDAO menuDAO;

    @Value("${exportDir}")
    String exportDir;

    @Value("${exportFileName}")
    String exportFileName;

    @Value("${exportMenuFileName}")
    String exportMenuFileName;

    public ResponseEntity exportProducts(){
        Path filePath = Paths.get(exportDir+File.separator+ exportFileName);
        logger.debug("Export file path = {}",filePath);
        try{
            CSVWriter writer = new CSVWriter(Files.newBufferedWriter(filePath, StandardCharsets.UTF_8));
            String header[] = {"PRODUCT_NAME","ALIAS","BRAND","CATEGORY","SUB_CATEGORY","URL","IMAGE_1","IMAGE_2","IMAGE_LIST","STATUS","HITS","RATING","SEARCH",
                    "CURRENCY_1","MARKED_PRICE_1","SALE_PRICE_1","CURRENCY_2","MARKED_PRICE_2","SALE_PRICE_2","CURRENCY_3","MARKED_PRICE_3","SALE_PRICE_3",
                    "INFO_HEAD_1","INFO_BODY_1","INFO_HEAD_2","INFO_BODY_2","INFO_HEAD_3","INFO_BODY_3",
                    "QUANTITY_1","SOURCE_NAME_1","SOURCE_COUNTRY_1","SOURCE_STATE_1","SOURCE_CITY_1","SOURCE_POSTCODE_1","SOURCE_LINE1_1","SOURCE_LINE2_1","CONTACT_CC_1","CONTACT_CN_1","SOURCE_EMAIL_1",
                    "QUANTITY_2","SOURCE_NAME_2","SOURCE_COUNTRY_2","SOURCE_STATE_2","SOURCE_CITY_2","SOURCE_POSTCODE_2","SOURCE_LINE1_2","SOURCE_LINE2_2","CONTACT_CC_2","CONTACT_CN_2","SOURCE_EMAIL_2",
                    "QUANTITY_3","SOURCE_NAME_3","SOURCE_COUNTRY_3","SOURCE_STATE_3","SOURCE_CITY_3","SOURCE_POSTCODE_3","SOURCE_LINE1_3","SOURCE_LINE2_3","CONTACT_CC_3","CONTACT_CN_3","SOURCE_EMAIL_3",
                    "ATTRIBUTE_1","ATTR_VALUE_1","ATTRIBUTE_2","ATTR_VALUE_2","ATTRIBUTE_3","ATTR_VALUE_3","ATTRIBUTE_4","ATTR_VALUE_4","ATTRIBUTE_5","ATTR_VALUE_5","ATTRIBUTE_6","ATTR_VALUE_6","ATTRIBUTE_7","ATTR_VALUE_7","ATTRIBUTE_8","ATTR_VALUE_8","ATTRIBUTE_9","ATTR_VALUE_9","ATTRIBUTE_10","ATTR_VALUE_10"};
            writer.writeNext(header);

            List<Product> productList = productDAO.getAllProducts();

            logger.debug("Number of products being exported = {} ProductList = {}",productList.size(),productList);

            Map<Long,ProductAnalytics> analytics = productAnalyticsDAO
                    .getAllProductAnalytics().stream()
                    .collect(Collectors.toConcurrentMap(
                            productAnalytic->productAnalytic.getProduct_id(),
                            productAnalytic->productAnalytic,
                            (x,y)->x)
                    );

            logger.debug("Number of Product Analytics being exported = {}",analytics.size());


            Map<Long,List<Price>> priceMap = productPriceDAO
                    .getAllPrices().stream()
                    .collect(Collectors.groupingBy(
                            price->price.getProduct_Id())
                    );
            logger.debug("Number of Price being exported = {}",priceMap.size());

            Map<Long,List<Info>> infoMap =productInfoDAO.getAllProductInfos().stream()
                    .collect(Collectors.groupingBy(
                            info->info.getProduct_Id())
                    );
            logger.debug("Number of Price being exported = {}",infoMap.size());

            Map<Long,List<Inventory>> inventoryMap = productInventoryDAO.getAllInventorys().stream()
                    .collect(Collectors.groupingBy(
                            inventory->inventory.getProduct_Id())
                    );
            logger.debug("Number of Price being exported = {}",inventoryMap.size());

            Map<Long,List<Attribute>> attributesMap = productAttributesDAO.getAttributeListByProductId().stream()
                    .collect(Collectors.groupingBy(
                            attribute->attribute.getProduct_Id())
                    );
            logger.debug("Number of Price being exported = {}",attributesMap.size());

            long counter =0;
            for(Product product : productList){
                logger.debug("Current row = {} and product = {}",++counter, product.getProduct_name());
                List<String> record = new ArrayList<>();
                // Initialize Product and cotegory
                record.add(product.getProduct_name());
                record.add(product.getAlias());
                record.add(product.getBrand());
                String category = product.getCategory();
                logger.debug("category = {}",category);
                if (category.indexOf('>')<0){
                    record.add(category);
                    record.add(EMPTY);
                }else{
                    record.add( category.substring(0,category.indexOf('>')).trim());
                    record.add( category.substring(category.indexOf('>')+1).trim());
                }
                record.add( product.getUrl());
                record.add( product.getIcon1_path());
                record.add( product.getIcon2_path());
                record.add( product.getImage_path());
                record.add( product.getProduct_status());
                logger.debug("product table exported");
                ProductAnalytics analytic = analytics.get(product.getId());
                if(analytic!=null){
                    record.add( analytic.getHits().toString());
                    record.add( analytic.getAverage_rating().toString());
                    record.add( analytic.getSearch());
                }else{
                    record.add(EMPTY);
                    record.add(EMPTY);
                    record.add(EMPTY);
                }
                logger.debug("analytic exported");


                //Get prices from priceMap and set into array
                List<Price> prices = priceMap.get(product.getId());
                for(int i=0;i<3;i++){
                    if(prices!=null&&prices.size()>i) {
                        Price price = prices.get(i);
                        record.add(price.getCurrency());
                        record.add(price.getMarked_price()!=null?price.getMarked_price().toString():EMPTY);
                        record.add(price.getSale_price()!=null?price.getSale_price().toString():EMPTY);
                    }else{
                        record.add(EMPTY);
                        record.add(EMPTY);
                        record.add(EMPTY);
                    }
                }
                logger.debug("prices exported");
                List<Info> infos = infoMap.get(product.getId());
                for(int i=0;i<3;i++){
                    String infoHead = EMPTY;
                    String infoBody = EMPTY;
                    if(infos!=null&&infos.size()>i) {
                        Info info = infos.get(i);
                        record.add(info.getInfo_head());
                        record.add(info.getInfo_body());
                    }else{
                        record.add(EMPTY);
                        record.add(EMPTY);
                    }

                }
                logger.debug("infos exported");
                List<Inventory> inventories = inventoryMap.get(product.getId());
                for(int i=0;i<3;i++){
                    if(inventories!=null&&inventories.size()>i) {
                        Inventory inventory = inventories.get(i);
                        record.add(inventory.getQuantity().toString());
                        record.add(inventory.getSource_name());
                        record.add(inventory.getCountry());
                        record.add(inventory.getState());
                        record.add(inventory.getCity());
                        record.add(inventory.getPostcode());
                        record.add(inventory.getLine1());
                        record.add(inventory.getLine2());
                        record.add(inventory.getContact_cc());
                        record.add(inventory.getContact_cn());
                        record.add(inventory.getEmail());
                    }else{
                        IntStream.rangeClosed(1,11).boxed()
                                .forEach(index->record.add(EMPTY));
                    }

                }
                logger.debug("inventories exported");
                List<Attribute> attributes = attributesMap.get(product.getId());
                for(int i=0; i<10;i++){
                    if(attributes!=null&&attributes.size()>i) {
                        Attribute attribute = attributes.get(i);
                        record.add(attribute.getAttribute());
                        record.add(attribute.getAttr_value());
                    }else{
                        record.add(EMPTY);
                        record.add(EMPTY);
                    }
                }
                logger.debug("attributes exported");
                String[] row = new String[record.size()];
                record.toArray(row);
                logger.debug("Exported row content value = {}", row);
                writer.writeNext(row);
            }

            writer.close();
        }
        catch(Exception e){
            logger.error("Error occurred during export process.", e);
        }


        File file = new File(filePath.toString());
        if (Files.exists(filePath)) {
            MediaType mediaType = MediaType.APPLICATION_OCTET_STREAM;
            try {
                return ResponseEntity.ok()
                        .contentType(mediaType)
                        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filePath.getFileName() + "\"")
                        .body(Files.readString(filePath));
            } catch (IOException e) {
                throw new AppException("File Not Found = "+filePath.toAbsolutePath().toString(),HttpStatus.NOT_FOUND);
            }
        }else{
            throw new AppException("File Not Found = "+filePath.toAbsolutePath().toString(),HttpStatus.NOT_FOUND);
        }

    }



    public ResponseEntity exportMenuRecords(){
        String fileName = exportDir+File.separator+ exportMenuFileName;

        try{
            CSVWriter writer = new CSVWriter(new OutputStreamWriter(new FileOutputStream(fileName), StandardCharsets.UTF_8));

            List<Menu> rows = menuDAO.getAllMenuRecords();

            String header[] = {"ID","Item Name","Icon","Route","Parent ID","Sort Order"};
            writer.writeNext(header);

            for(Menu row : rows){
                String entry[] =  { row.getId().toString(), row.getItem_name(), row.getIcon(), row.getRoute(), row.getParent_id().toString(), row.getSort_order().toString()};
                writer.writeNext(entry,true);
            }

            writer.close();
        }
        catch(Exception e){
            logger.error("Error occurred during menu export process.", e);
        }

        Path path = Paths.get(fileName);
        Resource resource = null;
        try {
            resource = new UrlResource(path.toUri());
        }
        catch (MalformedURLException e) {
            logger.error("File could not be located.", e);
        }
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType("text/csv"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }









    /*
    public List<Category> getCategoryByParentID(Long parent_id){
        List<Category> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM category WHERE parent_id = ? ORDER BY sort_order ASC", new Object[] { parent_id },
                (rs, rowNum) -> new Category(rs.getLong("id"), rs.getString("category_name"), rs.getLong("parent_id"), rs.getInt("sort_order"))
        ).forEach(category -> list.add(category));
        return list;
    }*/


    public List<Menu> getHeaderMenu(){
        return getChildMenu(0L);
    }

    public List<Menu> getFooterMenu(){
        return menuDAO.getMenuListByParentId(0L);
    }

    public List<Menu> getChildMenu(Long parent_id){
        List<Menu> menu = new ArrayList<>();
        menu = menuDAO.getMenuListByParentId(parent_id);
        if(menu != null) {
            for (int i = 0; i < menu.size(); i++) {
                menu.get(i).setSubmenu(getChildMenu(menu.get(i).getId()));
            }
        }
        return menu;
    }

    public Long addMenuItem(String item_name,String icon,String route,Long parent_id,Integer sort_order){
        return menuDAO.insertMenu(item_name, icon, route, parent_id, sort_order);
    }




    //Major Services

    // Needs Enhancement for High Volume Data
    public List<Catalog> getCatalogData(){
        List<Catalog> list = new ArrayList<>();
        Long len = productDAO.getCount();
        for(Long i=1l;i<=len;i++) {
            Catalog catalog = new Catalog();
            Product product = productDAO.getProductById(i);
            if (product == null)
                continue;
                //throw new AppException("Product Not Found", HttpStatus.NOT_FOUND);
            if (!product.getProduct_status().equals("ACTIVE"))
                throw new AppException("Product Status Not ACTIVE", HttpStatus.BAD_REQUEST);
            //System.out.println(product.toString());
            catalog.setCategory(product.getCategory());
            catalog.setProduct_name(product.getProduct_name());
            catalog.setAlias(product.getAlias());
            catalog.setIcon1_path(product.getIcon1_path());
            catalog.setIcon2_path(product.getIcon2_path());
            //quickView.setImage_path(Arrays.asList(product.getImage_path().split(",")));
            if (product.getUrl().length() != 0) {
                catalog.setPromotional(true);
                //quickView.setUrl(product.getUrl());
            } else {
                catalog.setPromotional(false);
                //detailView.setInventoryList(productInventoryDAO.getInventoryDetailsByProductId(product.getId()));
                //detailView.evaluateQuantity();
                catalog.setStock_quantity(productInventoryDAO.getInventoryCountByProductId(product.getId()));
                catalog.setPrice(productPriceDAO.getPriceListByProductId(product.getId()));
                catalog.evaluateDiscount();
            }
            //Product_analytics productAnalytics = productAnalyticsDAO.getAnalyticsByProductId(product.getId());
            //detailView.setHits(productAnalytics.getHits());
            //detailView.setAverage_rating(productAnalytics.getAverage_rating());
            //quickView.setInfo(productInfoDAO.getInfoListByProductId(product.getId(), 2));
            //detailView.setAttributes(productAttributesDAO.getAttributeListByProductId(product.getId()));
            //detailView.setReviews(productReviewsDAO.getReviewByProductId(product.getId(),5));
            list.add(catalog);
        }
        return list;
    }


    public QuickView getQuickViewData(String alias){
        QuickView quickView = new QuickView();
        Product product = productDAO.getProductByAlias(alias);
        if(product == null)
            throw new AppException("Product Not Found",HttpStatus.NOT_FOUND);
        if(!product.getProduct_status().equals("ACTIVE"))
            throw new AppException("Product Status Not ACTIVE", HttpStatus.BAD_REQUEST);
        //detailView.setCategory(product.getCategory());
        quickView.setProduct_name(product.getProduct_name());
        quickView.setAlias(product.getAlias());
        quickView.setBrand(product.getBrand());
        quickView.setImage_path(Arrays.asList(product.getImage_path().split(",")));
        if(product.getUrl().length() != 0){
            quickView.setPromotional(true);
            quickView.setUrl(product.getUrl());
        }
        else{
            quickView.setPromotional(false);
            //detailView.setInventoryList(productInventoryDAO.getInventoryDetailsByProductId(product.getId()));
            //detailView.evaluateQuantity();
            quickView.setStock_quantity(productInventoryDAO.getInventoryCountByProductId(product.getId()));
            quickView.setPrice(productPriceDAO.getPriceListByProductId(product.getId()));
            quickView.evaluateDiscount();
        }
        ProductAnalytics productAnalytics = productAnalyticsDAO.getAnalyticsByProductId(product.getId());
        //detailView.setHits(productAnalytics.getHits());
        //detailView.setAverage_rating(productAnalytics.getAverage_rating());
        quickView.setInfo(productInfoDAO.getInfoListByProductId(product.getId(),2));
        //detailView.setAttributes(productAttributesDAO.getAttributeListByProductId(product.getId()));
        //detailView.setReviews(productReviewsDAO.getReviewByProductId(product.getId(),5));
        return quickView;
    }

    public DetailView getDetailViewData(String alias){
        DetailView detailView = new DetailView();
        Product product = productDAO.getProductByAlias(alias);
        if(product == null)
            throw new AppException("Product Not Found",HttpStatus.NOT_FOUND);
        if(!product.getProduct_status().equals("ACTIVE"))
            throw new AppException("Product Status Not ACTIVE", HttpStatus.BAD_REQUEST);
        detailView.setCategory(product.getCategory());
        detailView.setProduct_name(product.getProduct_name());
        detailView.setAlias(product.getAlias());
        detailView.setBrand(product.getBrand());
        detailView.setImage_path(Arrays.asList(product.getImage_path().split(",")));
        if(product.getUrl().length() != 0){
            detailView.setPromotional(true);
            detailView.setUrl(product.getUrl());
        }
        else{
            detailView.setPromotional(false);
            detailView.setInventoryList(productInventoryDAO.getInventoryDetailsByProductId(product.getId()));
            detailView.evaluateQuantity();
            detailView.setPrice(productPriceDAO.getPriceListByProductId(product.getId()));
            detailView.evaluateDiscount();
        }
        ProductAnalytics productAnalytics = productAnalyticsDAO.getAnalyticsByProductId(product.getId());
        detailView.setHits(productAnalytics.getHits());
        detailView.setAverage_rating(productAnalytics.getAverage_rating());
        detailView.setInfo(productInfoDAO.getInfoListByProductId(product.getId(),100));
        detailView.setAttributes(productAttributesDAO.getAttributeListByProductId(product.getId()));
        detailView.setReviews(productReviewsDAO.getReviewByProductId(product.getId(),5));
        return detailView;
    }

    public List<Catalog> getSearchData(String query){
        query = query.trim();
        if(query.length() == 0)
            throw new AppException("Empty Search Request",HttpStatus.BAD_REQUEST);
        String search[] = query.split(" ");
        List<Catalog> catalogList = new ArrayList<>();
        List<Product> productList = new ArrayList<>();
        //List<SearchProduct> productSet = new ArrayList<>();
        List<Product> productSet = new ArrayList<>();/*
        for(int i=0; i < search.length;i++)
            productSet.addAll(productDAO.getProductByIdList(productAnalyticsDAO.getProductBySearchQuery(search[i])));*/
        productSet.addAll(productDAO.getProductByIdList(productAnalyticsDAO.getProductBySearchQueryArray(search)));

        /*for(Product p : productList){
            if(productSet.contains(p)){
                int position = productSet.indexOf(p);
                productSet.get(position).rankUpBy1();
            }
            else{
                productSet.add(new SearchProduct(p));
            }
        }*/

        for(Product product : productSet){
            Catalog catalog = new Catalog();
            if (!product.getProduct_status().equals("ACTIVE"))
                continue;
            catalog.setCategory(product.getCategory());
            catalog.setProduct_name(product.getProduct_name());
            catalog.setAlias(product.getAlias());
            catalog.setIcon1_path(product.getIcon1_path());
            catalog.setIcon2_path(product.getIcon2_path());
            if (product.getUrl().length() != 0) {
                catalog.setPromotional(true);
            } else {
                catalog.setPromotional(false);
                catalog.setStock_quantity(productInventoryDAO.getInventoryCountByProductId(product.getId()));
                catalog.setPrice(productPriceDAO.getPriceListByProductId(product.getId()));
                catalog.evaluateDiscount();
            }
            catalogList.add(catalog);
        }
        return catalogList;
    }

    public List<Catalog> getHardSearchData(String query){
        query = query.trim();
        if(query.length() == 0)
            throw new AppException("Empty Search Request",HttpStatus.BAD_REQUEST);
        String search[] = query.split(" ");
        List<Catalog> catalogList = new ArrayList<>();
        List<Product> productList = new ArrayList<>();
        //List<SearchProduct> productSet = new ArrayList<>();
        List<Product> productSet = new ArrayList<>();/*
        for(int i=0; i < search.length;i++)
            productSet.addAll(productDAO.getProductByIdList(productAnalyticsDAO.getProductBySearchQuery(search[i])));*/
        productSet.addAll(productDAO.getProductByIdList(productAnalyticsDAO.getProductBySearchQueryArrayHard(search)));

        /*for(Product p : productList){
            if(productSet.contains(p)){
                int position = productSet.indexOf(p);
                productSet.get(position).rankUpBy1();
            }
            else{
                productSet.add(new SearchProduct(p));
            }
        }*/

        for(Product product : productSet){
            Catalog catalog = new Catalog();
            if (!product.getProduct_status().equals("ACTIVE"))
                continue;
            catalog.setCategory(product.getCategory());
            catalog.setProduct_name(product.getProduct_name());
            catalog.setAlias(product.getAlias());
            catalog.setIcon1_path(product.getIcon1_path());
            catalog.setIcon2_path(product.getIcon2_path());
            if (product.getUrl().length() != 0) {
                catalog.setPromotional(true);
            } else {
                catalog.setPromotional(false);
                catalog.setStock_quantity(productInventoryDAO.getInventoryCountByProductId(product.getId()));
                catalog.setPrice(productPriceDAO.getPriceListByProductId(product.getId()));
                catalog.evaluateDiscount();
            }
            catalogList.add(catalog);
        }
        return catalogList;
    }






    public Long getProductIdByAlias(String alias){
        return productDAO.getProductIdByAlias(alias);
    }

    public String uploadRecord(String row[]){
        for(int i=0;i<row.length;i++) {
            if (row[i] != null)
                row[i] = row[i].trim();
        }
        //Alias Validation
        if(productDAO.getProductIdByAlias(row[1]) != 0L)
            throw new AppException("Alias not Available",HttpStatus.CONFLICT);
        //Category Validation
        String category = row[3];
        Category parent = categoryDAO.getCategoryByCategoryName(row[3]);
        if(parent == null){
            categoryDAO.insertCategory(row[3],0L,0);
            parent = categoryDAO.getCategoryByCategoryName(row[3]);
        }
        if(row[4] != null && row[4].length() > 0){
            Category child = categoryDAO.getCategoryByCategoryName(row[4]);
            if(child == null)
                categoryDAO.insertCategory(row[4],parent.getId(),0);
            else if(child.getParent_id() != parent.getId())
                throw new AppException("Parent-Child Category Mismatch",HttpStatus.CONFLICT);
            category += " > "+row[4];
        }
        //Source Validation
        Product_source source1 = null;
        if(row[29] != null && row[29].length() >0) {
            source1 = productSourceDAO.getProductSourceBySourceName(row[29]);
            if(source1 == null){
                productSourceDAO.insertProductSource(row[29],row[30],row[31],row[32],row[33],row[34],row[35],row[36],row[37],row[38]);
                source1 = productSourceDAO.getProductSourceBySourceName(row[29]);
            }
        }
        Product_source source2 = null;
        if(row[40] != null && row[40].length() >0) {
            source2 = productSourceDAO.getProductSourceBySourceName(row[40]);
            if(source2 == null){
                productSourceDAO.insertProductSource(row[40],row[41],row[42],row[43],row[44],row[45],row[46],row[47],row[48],row[49]);
                source2 = productSourceDAO.getProductSourceBySourceName(row[40]);
            }
        }
        Product_source source3 = null;
        if(row[51] != null && row[51].length() >0) {
            source3 = productSourceDAO.getProductSourceBySourceName(row[51]);
            if(source3 == null){
                productSourceDAO.insertProductSource(row[51],row[52],row[53],row[54],row[55],row[56],row[57],row[58],row[59],row[60]);
                source3 = productSourceDAO.getProductSourceBySourceName(row[51]);
            }
        }
        Long id = productDAO.insertProduct(row[0],row[1],row[2],category,row[5],row[6],row[7],row[8],row[9]);
        productAnalyticsDAO.insertProductAnalytics(id,Long.parseLong(row[10]),Float.parseFloat(row[11]),row[12]);
        if(row[13].length() > 0)
            productPriceDAO.insertProductPriceDU(id,row[13],Float.parseFloat(row[14]),Float.parseFloat(row[15]));
        if(row[16].length() > 0)
            productPriceDAO.insertProductPriceDU(id,row[16],Float.parseFloat(row[17]),Float.parseFloat(row[18]));
        if(row[19].length() > 0)
            productPriceDAO.insertProductPriceDU(id,row[19],Float.parseFloat(row[20]),Float.parseFloat(row[21]));
        if(row[22].length() > 0)
            productInfoDAO.insertProductInfoDU(id,row[22],row[23]);
        if(row[24].length() > 0)
            productInfoDAO.insertProductInfoDU(id,row[24],row[25]);
        if(row[26].length() > 0)
            productInfoDAO.insertProductInfoDU(id,row[26],row[27]);
        if(source1 != null)
            productInventoryDAO.insertProductInventoryDU(id,source1.getId(),Integer.parseInt(row[28]));
        if(source2 != null)
            productInventoryDAO.insertProductInventoryDU(id,source2.getId(),Integer.parseInt(row[39]));
        if(source3 != null)
            productInventoryDAO.insertProductInventoryDU(id,source3.getId(),Integer.parseInt(row[50]));
        if(row[61].length() > 0)
            productAttributesDAO.insertProductAttributeDU(id,row[61],row[62]);
        if(row[63].length() > 0)
            productAttributesDAO.insertProductAttributeDU(id,row[63],row[64]);
        if(row[65].length() > 0)
            productAttributesDAO.insertProductAttributeDU(id,row[65],row[66]);
        if(row[67].length() > 0)
            productAttributesDAO.insertProductAttributeDU(id,row[67],row[68]);
        if(row[69].length() > 0)
            productAttributesDAO.insertProductAttributeDU(id,row[69],row[70]);
        if(row[71].length() > 0)
            productAttributesDAO.insertProductAttributeDU(id,row[71],row[72]);
        if(row[73].length() > 0)
            productAttributesDAO.insertProductAttributeDU(id,row[73],row[74]);
        if(row[75].length() > 0)
            productAttributesDAO.insertProductAttributeDU(id,row[75],row[76]);
        if(row[77].length() > 0)
            productAttributesDAO.insertProductAttributeDU(id,row[77],row[78]);
        if(row[79].length() > 0)
            productAttributesDAO.insertProductAttributeDU(id,row[79],row[80]);
        return "SUCCESS";
    }

    /*

    public String uploadRecord(String row[]){
        for(int i=0;i<row.length;i++) {
            if (row[i] != null)
                row[i] = row[i].trim();
        }
        //Alias Validation
        if(productDAO.getProductIdByAlias(row[1]) != 0L)
            throw new AppException("Alias not Available",HttpStatus.CONFLICT);
        //Category Validation
        String category = row[3];
        Category parent = categoryDAO.getCategoryByCategoryName(row[3]);
        if(parent == null){
            categoryDAO.insertCategory(row[3],0L,0);
            parent = categoryDAO.getCategoryByCategoryName(row[3]);
        }
        if(row[4] != null && row[4].length() > 0){
            Category child = categoryDAO.getCategoryByCategoryName(row[4]);
            if(child == null)
                categoryDAO.insertCategory(row[4],parent.getId(),0);
            else if(child.getParent_id() != parent.getId())
                throw new AppException("Parent-Child Category Mismatch",HttpStatus.CONFLICT);
            category += " > "+row[4];
        }
        //Source Validation
        Product_source source1 = null;
        if(row[49] != null && row[49].length() >0) {
            source1 = productSourceDAO.getProductSourceBySourceName(row[49]);
            if(source1 == null){
                productSourceDAO.insertProductSource(row[49],row[50],row[51],row[52],row[53],row[54],row[55],row[56],row[57],row[58]);
                source1 = productSourceDAO.getProductSourceBySourceName(row[49]);
            }
        }
        Product_source source2 = null;
        if(row[60] != null && row[60].length() >0) {
            source2 = productSourceDAO.getProductSourceBySourceName(row[60]);
            if(source2 == null){
                productSourceDAO.insertProductSource(row[60],row[61],row[62],row[63],row[64],row[65],row[66],row[67],row[68],row[69]);
                source2 = productSourceDAO.getProductSourceBySourceName(row[60]);
            }
        }
        Product_source source3 = null;
        if(row[71] != null && row[71].length() >0) {
            source3 = productSourceDAO.getProductSourceBySourceName(row[71]);
            if(source3 == null){
                productSourceDAO.insertProductSource(row[71],row[72],row[73],row[74],row[75],row[76],row[77],row[78],row[79],row[80]);
                source3 = productSourceDAO.getProductSourceBySourceName(row[71]);
            }
        }
        Long id = productDAO.insertProduct(row[0],row[1],row[2],category,row[5],row[6],row[7],row[8],row[9]);
        productAnalyticsDAO.insertProductAnalytics(id,Long.parseLong(row[10]),Float.parseFloat(row[11]),row[12]);
        if(row[13].length() > 0)
            productPriceDAO.insertProductPriceDU(id,row[13],Float.parseFloat(row[14]),Float.parseFloat(row[15]));
        if(row[16].length() > 0)
            productPriceDAO.insertProductPriceDU(id,row[16],Float.parseFloat(row[17]),Float.parseFloat(row[18]));
        if(row[19].length() > 0)
            productPriceDAO.insertProductPriceDU(id,row[19],Float.parseFloat(row[20]),Float.parseFloat(row[21]));
        if(row[22].length() > 0)
            productInfoDAO.insertProductInfoDU(id,row[22],row[23]);
        if(row[24].length() > 0)
            productInfoDAO.insertProductInfoDU(id,row[24],row[25]);
        if(row[26].length() > 0)
            productInfoDAO.insertProductInfoDU(id,row[26],row[27]);
        if(row[28].length() > 0)
            productAttributesDAO.insertProductAttributeDU(id,row[28],row[29]);
        if(row[30].length() > 0)
            productAttributesDAO.insertProductAttributeDU(id,row[30],row[31]);
        if(row[32].length() > 0)
            productAttributesDAO.insertProductAttributeDU(id,row[32],row[33]);
        if(row[34].length() > 0)
            productAttributesDAO.insertProductAttributeDU(id,row[34],row[35]);
        if(row[36].length() > 0)
            productAttributesDAO.insertProductAttributeDU(id,row[36],row[37]);
        if(row[38].length() > 0)
            productAttributesDAO.insertProductAttributeDU(id,row[38],row[39]);
        if(row[40].length() > 0)
            productAttributesDAO.insertProductAttributeDU(id,row[40],row[41]);
        if(row[42].length() > 0)
            productAttributesDAO.insertProductAttributeDU(id,row[42],row[43]);
        if(row[44].length() > 0)
            productAttributesDAO.insertProductAttributeDU(id,row[44],row[45]);
        if(row[46].length() > 0)
            productAttributesDAO.insertProductAttributeDU(id,row[46],row[47]);
        if(source1 != null)
            productInventoryDAO.insertProductInventoryDU(id,source1.getId(),Integer.parseInt(row[48]));
        if(source2 != null)
            productInventoryDAO.insertProductInventoryDU(id,source2.getId(),Integer.parseInt(row[59]));
        if(source3 != null)
            productInventoryDAO.insertProductInventoryDU(id,source3.getId(),Integer.parseInt(row[70]));
        return "SUCCESS";
    }*/
}
