package com.amissible.catalogManager.controller;

import com.amissible.catalogManager.model.*;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
This Controller will provide predefined data to UI
 */

@RestController
@CrossOrigin
@RequestMapping("/static")
public class CatalogStaticController {

    @RequestMapping(value = "/catalog")
    @ResponseBody
    List<Catalog> getCatalog(){
        List<Catalog> cat = new ArrayList<>();
        cat.add(
                new Catalog(
                        "Blue Sanitizer Bracelet",
                        "blue-sanitizer-bracelet",
                        "https://shieldhands.com/wp-content/uploads/2020/10/Blue-Watch-Side-View.jpg",
                        "https://shieldhands.com/wp-content/uploads/2020/10/Blue-Watch-Side-View-02.jpg",
                        false,
                        70,
                        new ArrayList<>(
                                Arrays.asList(
                                        new Price(1l,"USD",9.87f,7.59f),
                                        new Price(1l,"CAD",12.99f,9.99f)
                                )
                        ),
                        false
                )
        );
        cat.add(
                new Catalog(
                        "Blue Sanitizer Bracelet on Amazon",
                        "blue-sanitizer-bracelet-amazon",
                        "https://shieldhands.com/wp-content/uploads/2020/10/Blue-Watch-Side-View.jpg",
                        "https://shieldhands.com/wp-content/uploads/2020/10/Blue-Watch-Side-View-02.jpg",
                        true,
                        false
                )
        );
        cat.add(
                new Catalog(
                        "Blue Sanitizer Bracelet on Canada",
                        "blue-sanitizer-bracelet-amazon-ca",
                        "https://shieldhands.com/wp-content/uploads/2020/10/Blue-Watch-Side-View.jpg",
                        "https://shieldhands.com/wp-content/uploads/2020/10/Blue-Watch-Side-View-02.jpg",
                        true,
                        false
                )
        );
        cat.add(
                new Catalog(
                        "Grey Sanitizer Bracelet",
                        "grey-sanitizer-bracelet",
                        "https://shieldhands.com/wp-content/uploads/2020/10/Grey-Bracelet-scaled.jpg",
                        "https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg",
                        false,
                        110,
                        new ArrayList<>(
                                Arrays.asList(
                                        new Price(1l,"USD",9.87f,7.59f),
                                        new Price(1l,"CAD",12.99f,9.99f)
                                )
                        ),
                        true
                )
        );
        cat.add(
                new Catalog(
                        "Grey Sanitizer Bracelet on Amazon",
                        "grey-sanitizer-bracelet-amazon",
                        "https://shieldhands.com/wp-content/uploads/2020/10/Grey-Bracelet-scaled.jpg",
                        "https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg",
                        true,
                        false
                )
        );
        cat.add(
                new Catalog(
                        "Grey Sanitizer Bracelet on Canada",
                        "grey-sanitizer-bracelet-amazon-ca",
                        "https://shieldhands.com/wp-content/uploads/2020/10/Grey-Bracelet-scaled.jpg",
                        "https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg",
                        true,
                        false
                )
        );
        cat.add(
                new Catalog(
                        "Peach Sanitizer Bracelet",
                        "peach-sanitizer-bracelet",
                        "https://shieldhands.com/wp-content/uploads/2020/10/Yellow-Watch-Side-View.jpg",
                        "https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg",
                        false,
                        150,
                        new ArrayList<>(
                                Arrays.asList(
                                        new Price(1l,"USD",9.87f,7.59f),
                                        new Price(1l,"CAD",12.99f,9.99f)
                                )
                        ),
                        false
                )
        );
        cat.add(
                new Catalog(
                        "Peach Sanitizer Bracelet on Amazon",
                        "peach-sanitizer-bracelet-amazon",
                        "https://shieldhands.com/wp-content/uploads/2020/10/Yellow-Watch-Side-View.jpg",
                        "https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg",
                        true,
                        false
                )
        );
        cat.add(
                new Catalog(
                        "Peach Sanitizer Bracelet on Canada",
                        "peach-sanitizer-bracelet-amazon-ca",
                        "https://shieldhands.com/wp-content/uploads/2020/10/Yellow-Watch-Side-View.jpg",
                        "https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg",
                        true,
                        true
                )
        );
        cat.add(
                new Catalog(
                        "PVC Blue Sanitizer Wristband [Pack of 2]",
                        "pvc-blue-sanitizer-wristband",
                        "https://shieldhands.com/wp-content/uploads/2020/10/Blue-side.jpg",
                        "https://shieldhands.com/wp-content/uploads/2020/10/All-snazy-colors-scaled.jpg",
                        false,
                        0,
                        new ArrayList<>(
                                Arrays.asList(
                                        new Price(1l,"USD",3.8f,3.8f),
                                        new Price(1l,"CAD",5f,5f)
                                )
                        ),
                        false
                )
        );
        cat.add(
                new Catalog(
                        "PVC Grey Sanitizer Wristband [Pack of 2]",
                        "pvc-grey-sanitizer-wristband",
                        "https://shieldhands.com/wp-content/uploads/2020/10/Grey-side.jpg",
                        "https://shieldhands.com/wp-content/uploads/2020/10/All-snazy-colors-scaled.jpg",
                        false,
                        3,
                        new ArrayList<>(
                                Arrays.asList(
                                        new Price(1l,"USD",3.8f,3.8f),
                                        new Price(1l,"CAD",5f,5f)
                                )
                        ),
                        true
                )
        );
        cat.add(
                new Catalog(
                        "PVC Green Sanitizer Wristband [Pack of 2]",
                        "pvc-green-sanitizer-wristband",
                        "https://shieldhands.com/wp-content/uploads/2020/10/Green-side.jpg",
                        "https://shieldhands.com/wp-content/uploads/2020/10/All-snazy-colors-scaled.jpg",
                        false,
                        8,
                        new ArrayList<>(
                                Arrays.asList(
                                        new Price(1l,"USD",3.8f,3.8f),
                                        new Price(1l,"CAD",5f,5f)
                                )
                        ),
                        false
                )
        );
        cat.add(
                new Catalog(
                        "PVC Pink Sanitizer Wristband [Pack of 2]",
                        "pvc-pink-sanitizer-wristband",
                        "https://shieldhands.com/wp-content/uploads/2020/10/pink-side-view-scaled.jpg",
                        "https://shieldhands.com/wp-content/uploads/2020/10/All-snazy-colors-scaled.jpg",
                        true,
                        false
                )
        );
        cat.add(
                new Catalog(
                        "PVC White Sanitizer Wristband [Pack of 2]",
                        "pvc-white-sanitizer-wristband",
                        "https://shieldhands.com/wp-content/uploads/2020/10/white-side.jpg",
                        "https://shieldhands.com/wp-content/uploads/2020/10/All-snazy-colors-scaled.jpg",
                        true,
                        true
                )
        );
        return cat;
    }

    @RequestMapping(path = "/quickview/{alias}")
    @ResponseBody
    QuickView getQuickViewData(@PathVariable("alias") String alias){
        QuickView quickView = null;
        if(alias.equals("blue-sanitizer-bracelet"))
            quickView = new QuickView("Blue Sanitizer Bracelet",
                    "blue-sanitizer-bracelet",
                    "Shield Hands",
                    new ArrayList<>(
                            Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/Blue-Watch-Side-View.jpg",
                                    "https://shieldhands.com/wp-content/uploads/2020/10/Blue-Watch-Side-View-02.jpg",
                                    "https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg",
                                    "https://shieldhands.com/wp-content/uploads/2020/10/Easy-to-Refill-scaled.jpg",
                                    "https://shieldhands.com/wp-content/uploads/2020/10/Product-Features-04-scaled.jpg",
                                    "https://shieldhands.com/wp-content/uploads/2020/10/Where-is-it-needed-scaled.jpg"
                            )
                    ),
                    false,
                    70,
                    new ArrayList<>(
                            Arrays.asList(
                                    new Price(1l,"USD",9.87f,7.59f),
                                    new Price(1l,"CAD",12.99f,9.99f)
                            )
                    ),
                    new ArrayList<>(Arrays.asList(new Info(1l,
                            "Product description",
                            "<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and soft silicone. Adjustable strap makes it ideal for the whole family. The package do not contain any liquids fill the empty refill bottle with the fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. If you intend to travel, just fill in your refill with the necessary product and enjoy your trip."
                    )))
            );
        else if(alias.equals("blue-sanitizer-bracelet-amazon"))
            quickView = new QuickView("Blue Sanitizer Bracelet on Amazon",
                    "blue-sanitizer-bracelet-amazon",
                    "Shield Hands",
                    new ArrayList<>(
                            Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/Blue-Watch-Side-View.jpg",
                                    "https://shieldhands.com/wp-content/uploads/2020/10/Blue-Watch-Side-View-02.jpg",
                                    "https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg",
                                    "https://shieldhands.com/wp-content/uploads/2020/10/Easy-to-Refill-scaled.jpg",
                                    "https://shieldhands.com/wp-content/uploads/2020/10/Product-Features-04-scaled.jpg",
                                    "https://shieldhands.com/wp-content/uploads/2020/10/Where-is-it-needed-scaled.jpg"
                            )
                    ),
                    true,
                    "https://www.amazon.com/Sanitizer-Wristband-Refillable-Container-ShieldHands/dp/B08HY6YLM2/",
                    new ArrayList<>(Arrays.asList(new Info(1l,
                            "Product description",
                            "<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and soft silicone. Adjustable strap makes it ideal for the whole family. The package do not contain any liquids fill the empty refill bottle with the fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. If you intend to travel, just fill in your refill with the necessary product and enjoy your trip."
                    )))
            );
        else if(alias.equals("blue-sanitizer-bracelet-amazon-ca"))
            quickView = new QuickView("Blue Sanitizer Bracelet on Canada",
                    "blue-sanitizer-bracelet-amazon-ca",
                    "Shield Hands",
                    new ArrayList<>(
                            Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/Blue-Watch-Side-View.jpg",
                                    "https://shieldhands.com/wp-content/uploads/2020/10/Blue-Watch-Side-View-02.jpg",
                                    "https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg",
                                    "https://shieldhands.com/wp-content/uploads/2020/10/Easy-to-Refill-scaled.jpg",
                                    "https://shieldhands.com/wp-content/uploads/2020/10/Product-Features-04-scaled.jpg",
                                    "https://shieldhands.com/wp-content/uploads/2020/10/Where-is-it-needed-scaled.jpg"
                            )
                    ),
                    true,
                    "https://www.amazon.ca/dp/B08HY6YLM2",
                    new ArrayList<>(Arrays.asList(new Info(1l,
                            "Product description",
                            "<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and soft silicone. Adjustable strap makes it ideal for the whole family. The package do not contain any liquids fill the empty refill bottle with the fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. If you intend to travel, just fill in your refill with the necessary product and enjoy your trip."
                    )))
            );
        else if(alias.equals("grey-sanitizer-bracelet")){
            quickView = new QuickView("Grey Sanitizer Bracelet",
                    "grey-sanitizer-bracelet",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/Grey-Bracelet-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Easy-to-Refill-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Product-Features-04-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Where-is-it-needed-scaled.jpg")),
                    false,
                    110,
                    new ArrayList<>(Arrays.asList(
                            new Price(1l,"USD",9.87f,7.59f),
                            new Price(1l,"CAD",12.99f,9.99f)
                    )),
                    new ArrayList<>(Arrays.asList(new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and soft silicone. Adjustable strap makes it ideal for the whole family. The package do not contain any liquids fill the empty refill bottle with the fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. If you intend to travel, just fill in your refill with the necessary product and enjoy your trip.")))
            );
        }
        else if(alias.equals("grey-sanitizer-bracelet-amazon")){
            quickView = new QuickView("Grey Sanitizer Bracelet on Amazon",
                    "grey-sanitizer-bracelet-amazon",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/Grey-Bracelet-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Easy-to-Refill-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Product-Features-04-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Where-is-it-needed-scaled.jpg")),
                    true,
                    "https://www.amazon.com/Sanitizer-Wristband-Refillable-Container-ShieldHands/dp/B08HY92GL4/",
                    new ArrayList<>(Arrays.asList(new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and soft silicone. Adjustable strap makes it ideal for the whole family. The package do not contain any liquids fill the empty refill bottle with the fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. If you intend to travel, just fill in your refill with the necessary product and enjoy your trip.")))
            );
        }
        else if(alias.equals("grey-sanitizer-bracelet-amazon-ca")){
            quickView = new QuickView("Grey Sanitizer Bracelet on Canada",
                    "grey-sanitizer-bracelet-amazon-ca",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/Grey-Bracelet-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Easy-to-Refill-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Product-Features-04-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Where-is-it-needed-scaled.jpg")),
                    true,
                    "https://www.amazon.ca/dp/B08HY92GL4?",
                    new ArrayList<>(Arrays.asList(new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and soft silicone. Adjustable strap makes it ideal for the whole family. The package do not contain any liquids fill the empty refill bottle with the fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. If you intend to travel, just fill in your refill with the necessary product and enjoy your trip.")))
            );
        }
        else if(alias.equals("peach-sanitizer-bracelet")){
            quickView = new QuickView("Peach Sanitizer Bracelet",
                    "peach-sanitizer-bracelet",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/Yellow-Watch-Side-View.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Easy-to-Refill-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Product-Features-04-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Where-is-it-needed-scaled.jpg")),
                    false,
                    150,
                    new ArrayList<>(Arrays.asList(
                            new Price(1l,"USD",9.87f,7.59f),
                            new Price(1l,"CAD",12.99f,9.99f)
                    )),
                    new ArrayList<>(Arrays.asList(new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and soft silicone. Adjustable strap makes it ideal for the whole family. The package do not contain any liquids fill the empty refill bottle with the fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. If you intend to travel, just fill in your refill with the necessary product and enjoy your trip.")))
            );
        }
        else if(alias.equals("peach-sanitizer-bracelet-amazon")){
            quickView = new QuickView("Peach Sanitizer Bracelet on Amazon",
                    "peach-sanitizer-bracelet-amazon",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/Yellow-Watch-Side-View.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Easy-to-Refill-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Product-Features-04-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Where-is-it-needed-scaled.jpg")),
                    true,
                    "https://www.amazon.com/Sanitizer-Wristband-Refillable-Container-ShieldHands/dp/B08HY86JPH/",
                    new ArrayList<>(Arrays.asList(new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and soft silicone. Adjustable strap makes it ideal for the whole family. The package do not contain any liquids fill the empty refill bottle with the fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. If you intend to travel, just fill in your refill with the necessary product and enjoy your trip.")))
            );
        }
        else if(alias.equals("peach-sanitizer-bracelet-amazon-ca")){
            quickView = new QuickView("Peach Sanitizer Bracelet on Canada",
                    "peach-sanitizer-bracelet-amazon-ca",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/Yellow-Watch-Side-View.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Easy-to-Refill-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Product-Features-04-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Where-is-it-needed-scaled.jpg")),
                    true,
                    "https://www.amazon.ca/dp/B08HY86JPH",
                    new ArrayList<>(Arrays.asList(new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and soft silicone. Adjustable strap makes it ideal for the whole family. The package do not contain any liquids fill the empty refill bottle with the fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. If you intend to travel, just fill in your refill with the necessary product and enjoy your trip.")))
            );
        }
        else if(alias.equals("pvc-blue-sanitizer-wristband")){
            quickView = new QuickView("PVC Blue Sanitizer Wristband [Pack of 2]",
                    "pvc-blue-sanitizer-wristband",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/Blue-side.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazy-colors-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/product-features-scaled.jpg")),
                    false,
                    0,
                    new ArrayList<>(Arrays.asList(
                            new Price(1l,"USD",3.8f,3.8f),
                            new Price(1l,"CAD",5.0f,5.0f)
                    )),
                    new ArrayList<>(Arrays.asList(new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and washable PVC. Adjustable strap makes it ideal for the whole family. The package does not contain any liquid or refill bottle. Fill the wristband with fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. Fill it up before sleeping at night or leaving home some you are never unprotected.")))
            );
        }

        else if(alias.equals("pvc-grey-sanitizer-wristband")){
            quickView = new QuickView("PVC Grey Sanitizer Wristband [Pack of 2]",
                    "pvc-grey-sanitizer-wristband",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/Grey-side.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazy-colors-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/product-features-scaled.jpg")),
                    false,
                    3,
                    new ArrayList<>(Arrays.asList(
                            new Price(1l,"USD",3.8f,3.8f),
                            new Price(1l,"CAD",5.0f,5.0f)
                    )),
                    new ArrayList<>(Arrays.asList(new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and washable PVC. Adjustable strap makes it ideal for the whole family. The package does not contain any liquid or refill bottle. Fill the wristband with fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. Fill it up before sleeping at night or leaving home some you are never unprotected.")))
            );
        }

        else if(alias.equals("pvc-green-sanitizer-wristband")){
            quickView = new QuickView("PVC Green Sanitizer Wristband [Pack of 2]",
                    "pvc-green-sanitizer-wristband",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/Green-side.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazy-colors-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/product-features-scaled.jpg")),
                    false,
                    8,
                    new ArrayList<>(Arrays.asList(
                            new Price(1l,"USD",3.8f,3.8f),
                            new Price(1l,"CAD",5.0f,5.0f)
                    )),
                    new ArrayList<>(Arrays.asList(new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and washable PVC. Adjustable strap makes it ideal for the whole family. The package does not contain any liquid or refill bottle. Fill the wristband with fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. Fill it up before sleeping at night or leaving home some you are never unprotected.")))
            );
        }
        else if(alias.equals("pvc-pink-sanitizer-wristband")){
            quickView = new QuickView("PVC Pink Sanitizer Wristband [Pack of 2]",
                    "pvc-pink-sanitizer-wristband",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/pink-side-view-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Blue-Watch-Side-View-02.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Product-Features-04-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Where-is-it-needed-scaled.jpg")),
                    true,
                    "https://www.amazon.com/dp/B08JK7JTMP",
                    new ArrayList<>(Arrays.asList(new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and soft silicone. Adjustable strap makes it ideal for the whole family. The package do not contain any liquids fill the empty refill bottle with the fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. If you intend to travel, just fill in your refill with the necessary product and enjoy your trip.")))
            );
        }
        else if(alias.equals("pvc-white-sanitizer-wristband")){
            quickView = new QuickView("PVC White Sanitizer Wristband [Pack of 2]",
                    "pvc-white-sanitizer-wristband",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/white-side.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Blue-Watch-Side-View-02.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Product-Features-04-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Where-is-it-needed-scaled.jpg")),
                    true,
                    "https://www.amazon.com/dp/B08JK78JZV",
                    new ArrayList<>(Arrays.asList(new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and soft silicone. Adjustable strap makes it ideal for the whole family. The package do not contain any liquids fill the empty refill bottle with the fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. If you intend to travel, just fill in your refill with the necessary product and enjoy your trip.")))
            );
        }
        return quickView;
    }

    @RequestMapping(path = "/product/{alias}")
    @ResponseBody
    DetailView getDetailViewData(@PathVariable("alias") String alias){
        DetailView detailView = null;
        if(alias.equals("blue-sanitizer-bracelet")){
            detailView = new DetailView("Bracelet > Silicone Bracelet",
                    "Blue Sanitizer Bracelet",
                    "blue-sanitizer-bracelet",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/Blue-Watch-Side-View.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Blue-Watch-Side-View-02.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Easy-to-Refill-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Product-Features-04-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Where-is-it-needed-scaled.jpg")),
                    new ArrayList<>(Arrays.asList(
                                    new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and soft silicone. Adjustable strap makes it ideal for the whole family. The package do not contain any liquids fill the empty refill bottle with the fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. If you intend to travel, just fill in your refill with the necessary product and enjoy your trip."),
                                    new Info(1l,"Quick Description","<b>Step 1:</b> Fill the bottle with the liquid of your choice (fill it up before sleeping at night or leaving home some you are never unprotected)<br><br><b>Step 2:</b> Remove bracelet cover and insert the bottle nozzle in the hole, pressing the bottle until the tank is full<br><br><b>Step 3:</b> After filling the tank, replace the cover and wear the band on your wrist<br><br><b>Step 4:</b> To use remove the cover, squeeze to dispense some amount on your palm and apply as per need.<br><br><b>Step 5:</b> Remember to replace the cover after use.<br><br><b>✅✅Protect yourself by placing an order today, go ahead and click ‘Add to Cart’ right away! ✅✅</b>"),
                                    new Info(1l,"Product Highlight","✅ <b>SANITIZE WHILE YOU MOVE:</b> Your hand sanitizer will be with you whenever you need it. Fill it up with your own sanitiser in the morning and use it throughout the day.<br>✅ <b>EASY TO USE:</b> The wristband is hassle free. Remove the stopper and press the tank to dispense as much liquid as needed.<br>✅ <b>VERSATILE & REUSABLE:</b> The silicone bracelet can hold up to 15 ml of sanitizer (at least 10-15 uses per fill!). Not only that, you can fill it with moisturizing cream or sunscreen too.<br>✅ <b>PROTECTION FOR THE WHOLE FAMILY EVERYWHERE:</b> Encourage the whole family to use the wearable. The strap fits children, teens, men and women. Use it in school, at work; while shopping, traveling and workout.<br>✅ <b>HIGH QUALITY:</b> Silicon used for the bracelet is durable, flexible, and waterproof. Love it or we’ll buy it back from you, no questions asked!")
                    )),
                    false,
                    70,
                    new ArrayList<>(Arrays.asList(
                                    new Inventory(1l,30,"Boston Warehouse","United States of America","Massachusetts","Boston","02111","Boston Lane","New Highway","1","7899877898","usaconnect@gmail.com"),
                                    new Inventory(1l,30,"Seattle Warehouse","United States of America","Washington","Seattle","98114","Seattle Lane","New Highway","1","9874563214","usaconnect@gmail.com")
                    )),
                    new ArrayList<>(Arrays.asList(
                                    new Price(1l,"USD",9.87f,7.59f),
                                    new Price(1l,"CAD",12.99f,9.99f)
                    )),
                    100L,
                    4.5f,
                    new ArrayList<>(
                            Arrays.asList(
                                    new Attribute(1l,"TAG","Blue Sanitizer Bracelet"),
                                    new Attribute(1l,"SKU","CZ-0VUN-TOTD"),
                                    new Attribute(1l,"Weight","0.088 kg"),
                                    new Attribute(1l,"Color","Blue"),
                                    new Attribute(1l,"Dimensions","11.811 × 3.543 × 0.951 cm")
                            )
                    ),
                    null
            );
        }
        else if(alias.equals("blue-sanitizer-bracelet-amazon")){
            detailView = new DetailView("Bracelet > Silicone Bracelet on Amazon",
                    "Blue Sanitizer Bracelet on Amazon",
                    "blue-sanitizer-bracelet-amazon",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/Blue-Watch-Side-View.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Blue-Watch-Side-View-02.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Product-Features-04-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Where-is-it-needed-scaled.jpg")),
                    new ArrayList<>(Arrays.asList(
                            new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and soft silicone. Adjustable strap makes it ideal for the whole family. The package do not contain any liquids fill the empty refill bottle with the fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. If you intend to travel, just fill in your refill with the necessary product and enjoy your trip."),
                            new Info(1l,"Quick Description","<b>Step 1:</b> Fill the bottle with the liquid of your choice (fill it up before sleeping at night or leaving home some you are never unprotected)<br><br><b>Step 2:</b> Remove bracelet cover and insert the bottle nozzle in the hole, pressing the bottle until the tank is full<br><br><b>Step 3:</b> After filling the tank, replace the cover and wear the band on your wrist<br><br><b>Step 4:</b> To use remove the cover, squeeze to dispense some amount on your palm and apply as per need.<br><br><b>Step 5:</b> Remember to replace the cover after use.<br><br><b>✅✅Protect yourself by placing an order today, go ahead and click ‘Add to Cart’ right away! ✅✅</b>"),
                            new Info(1l,"Product Highlight","✅ <b>SANITIZE WHILE YOU MOVE:</b> Your hand sanitizer will be with you whenever you need it. Fill it up with your own sanitiser in the morning and use it throughout the day.<br>✅ <b>EASY TO USE:</b> The wristband is hassle free. Remove the stopper and press the tank to dispense as much liquid as needed.<br>✅ <b>VERSATILE & REUSABLE:</b> The silicone bracelet can hold up to 15 ml of sanitizer (at least 10-15 uses per fill!). Not only that, you can fill it with moisturizing cream or sunscreen too.<br>✅ <b>PROTECTION FOR THE WHOLE FAMILY EVERYWHERE:</b> Encourage the whole family to use the wearable. The strap fits children, teens, men and women. Use it in school, at work; while shopping, traveling and workout.<br>✅ <b>HIGH QUALITY:</b> Silicon used for the bracelet is durable, flexible, and waterproof. Love it or we’ll buy it back from you, no questions asked!")
                    )),
                    true,
                    "https://www.amazon.com/Sanitizer-Wristband-Refillable-Container-ShieldHands/dp/B08HY6YLM2/",
                    100L,
                    0f,
                    new ArrayList<>(Arrays.asList(
                            new Attribute(1l,"TAG","Blue Sanitizer Bracelet"),
                            new Attribute(1l,"SKU","CZ-0VUN-TOTD-1"),
                            new Attribute(1l,"Weight","0.088 kg"),
                            new Attribute(1l,"Color","Blue"),
                            new Attribute(1l,"Dimensions","11.811 × 3.543 × 0.951 cm")
                    )),
                    null
            );
        }
        else if(alias.equals("blue-sanitizer-bracelet-amazon-ca")){
            detailView = new DetailView("Bracelet > Silicone Bracelet on Amazon Canada",
                    "Blue Sanitizer Bracelet on Canada",
                    "blue-sanitizer-bracelet-amazon-ca",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/Blue-Watch-Side-View.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Blue-Watch-Side-View-02.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Product-Features-04-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Where-is-it-needed-scaled.jpg")),
                    new ArrayList<>(Arrays.asList(
                            new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and soft silicone. Adjustable strap makes it ideal for the whole family. The package do not contain any liquids fill the empty refill bottle with the fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. If you intend to travel, just fill in your refill with the necessary product and enjoy your trip."),
                            new Info(1l,"Quick Description","<b>Step 1:</b> Fill the bottle with the liquid of your choice (fill it up before sleeping at night or leaving home some you are never unprotected)<br><br><b>Step 2:</b> Remove bracelet cover and insert the bottle nozzle in the hole, pressing the bottle until the tank is full<br><br><b>Step 3:</b> After filling the tank, replace the cover and wear the band on your wrist<br><br><b>Step 4:</b> To use remove the cover, squeeze to dispense some amount on your palm and apply as per need.<br><br><b>Step 5:</b> Remember to replace the cover after use.<br><br><b>✅✅Protect yourself by placing an order today, go ahead and click ‘Add to Cart’ right away! ✅✅</b>"),
                            new Info(1l,"Product Highlight","✅ <b>SANITIZE WHILE YOU MOVE:</b> Your hand sanitizer will be with you whenever you need it. Fill it up with your own sanitiser in the morning and use it throughout the day.<br>✅ <b>EASY TO USE:</b> The wristband is hassle free. Remove the stopper and press the tank to dispense as much liquid as needed.<br>✅ <b>VERSATILE & REUSABLE:</b> The silicone bracelet can hold up to 15 ml of sanitizer (at least 10-15 uses per fill!). Not only that, you can fill it with moisturizing cream or sunscreen too.<br>✅ <b>PROTECTION FOR THE WHOLE FAMILY EVERYWHERE:</b> Encourage the whole family to use the wearable. The strap fits children, teens, men and women. Use it in school, at work; while shopping, traveling and workout.<br>✅ <b>HIGH QUALITY:</b> Silicon used for the bracelet is durable, flexible, and waterproof. Love it or we’ll buy it back from you, no questions asked!")
                    )),
                    true,
                    "https://www.amazon.ca/dp/B08HY6YLM2",
                    100L,
                    0f,
                    new ArrayList<>(Arrays.asList(
                            new Attribute(1l,"TAG","Blue Sanitizer Bracelet"),
                            new Attribute(1l,"SKU","CZ-0VUN-TOTD-1-1"),
                            new Attribute(1l,"Weight","0.088 kg"),
                            new Attribute(1l,"Color","Blue"),
                            new Attribute(1l,"Dimensions","11.811 × 3.543 × 0.951 cm")
                    )),
                    null
            );
        }
        else if(alias.equals("grey-sanitizer-bracelet")){
            detailView = new DetailView("Bracelet > Silicone Bracelet",
                    "Grey Sanitizer Bracelet",
                    "grey-sanitizer-bracelet",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/Grey-Bracelet-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Easy-to-Refill-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Product-Features-04-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Where-is-it-needed-scaled.jpg")),
                    new ArrayList<>(Arrays.asList(
                            new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and soft silicone. Adjustable strap makes it ideal for the whole family. The package do not contain any liquids fill the empty refill bottle with the fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. If you intend to travel, just fill in your refill with the necessary product and enjoy your trip."),
                            new Info(1l,"Quick Description","<b>Step 1:</b> Fill the bottle with the liquid of your choice (fill it up before sleeping at night or leaving home some you are never unprotected)<br><br><b>Step 2:</b> Remove bracelet cover and insert the bottle nozzle in the hole, pressing the bottle until the tank is full<br><br><b>Step 3:</b> After filling the tank, replace the cover and wear the band on your wrist<br><br><b>Step 4:</b> To use remove the cover, squeeze to dispense some amount on your palm and apply as per need.<br><br><b>Step 5:</b> Remember to replace the cover after use.<br><br><b>✅✅Protect yourself by placing an order today, go ahead and click ‘Add to Cart’ right away! ✅✅</b>"),
                            new Info(1l,"Product Highlight","✅ <b>SANITIZE WHILE YOU MOVE:</b> Your hand sanitizer will be with you whenever you need it. Fill it up with your own sanitiser in the morning and use it throughout the day.<br>✅ <b>EASY TO USE:</b> The wristband is hassle free. Remove the stopper and press the tank to dispense as much liquid as needed.<br>✅ <b>VERSATILE & REUSABLE:</b> The silicone bracelet can hold up to 15 ml of sanitizer (at least 10-15 uses per fill!). Not only that, you can fill it with moisturizing cream or sunscreen too.<br>✅ <b>PROTECTION FOR THE WHOLE FAMILY EVERYWHERE:</b> Encourage the whole family to use the wearable. The strap fits children, teens, men and women. Use it in school, at work; while shopping, traveling and workout.<br>✅ <b>HIGH QUALITY:</b> Silicon used for the bracelet is durable, flexible, and waterproof. Love it or we’ll buy it back from you, no questions asked!")
                    )),
                    false,
                    110,
                    new ArrayList<>(Arrays.asList(
                            new Inventory(1l,50,"Seattle Warehouse","United States of America","Washington","Seattle","98114","Seattle Lane","New Highway","1","9874563214","usaconnect@gmail.com"),
                            new Inventory(1l,60,"Chicago Warehouse","United States of America","Illinois","Chicago","60603","Chicago Lane","New Highway","1","8521456753","usaconnect@gmail.com")
                    )),
                    new ArrayList<>(Arrays.asList(
                            new Price(1l,"USD",9.87f,7.59f),
                            new Price(1l,"CAD",12.99f,9.99f)
                    )),
                    200L,
                    4.1f,
                    new ArrayList<>(
                            Arrays.asList(
                                    new Attribute(1l,"TAG","Grey Sanitizer Bracelet"),
                                    new Attribute(1l,"SKU","SB-480V-I82H"),
                                    new Attribute(1l,"Weight","0.088 kg"),
                                    new Attribute(1l,"Color","Grey"),
                                    new Attribute(1l,"Dimensions","11.811 × 3.543 × 0.951 cm")
                            )
                    ),
                    null
            );
        }
        else if(alias.equals("grey-sanitizer-bracelet-amazon")){
            detailView = new DetailView("Bracelet > Silicone Bracelet on Amazon",
                    "Grey Sanitizer Bracelet on Amazon",
                    "grey-sanitizer-bracelet-amazon",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/Grey-Bracelet-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Easy-to-Refill-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Product-Features-04-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Where-is-it-needed-scaled.jpg")),
                    new ArrayList<>(Arrays.asList(
                            new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and soft silicone. Adjustable strap makes it ideal for the whole family. The package do not contain any liquids fill the empty refill bottle with the fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. If you intend to travel, just fill in your refill with the necessary product and enjoy your trip."),
                            new Info(1l,"Quick Description","<b>Step 1:</b> Fill the bottle with the liquid of your choice (fill it up before sleeping at night or leaving home some you are never unprotected)<br><br><b>Step 2:</b> Remove bracelet cover and insert the bottle nozzle in the hole, pressing the bottle until the tank is full<br><br><b>Step 3:</b> After filling the tank, replace the cover and wear the band on your wrist<br><br><b>Step 4:</b> To use remove the cover, squeeze to dispense some amount on your palm and apply as per need.<br><br><b>Step 5:</b> Remember to replace the cover after use.<br><br><b>✅✅Protect yourself by placing an order today, go ahead and click ‘Add to Cart’ right away! ✅✅</b>"),
                            new Info(1l,"Product Highlight","✅ <b>SANITIZE WHILE YOU MOVE:</b> Your hand sanitizer will be with you whenever you need it. Fill it up with your own sanitiser in the morning and use it throughout the day.<br>✅ <b>EASY TO USE:</b> The wristband is hassle free. Remove the stopper and press the tank to dispense as much liquid as needed.<br>✅ <b>VERSATILE & REUSABLE:</b> The silicone bracelet can hold up to 15 ml of sanitizer (at least 10-15 uses per fill!). Not only that, you can fill it with moisturizing cream or sunscreen too.<br>✅ <b>PROTECTION FOR THE WHOLE FAMILY EVERYWHERE:</b> Encourage the whole family to use the wearable. The strap fits children, teens, men and women. Use it in school, at work; while shopping, traveling and workout.<br>✅ <b>HIGH QUALITY:</b> Silicon used for the bracelet is durable, flexible, and waterproof. Love it or we’ll buy it back from you, no questions asked!")
                    )),
                    true,
                    "https://www.amazon.com/Sanitizer-Wristband-Refillable-Container-ShieldHands/dp/B08HY92GL4/",
                    100L,
                    0f,
                    new ArrayList<>(Arrays.asList(
                            new Attribute(1l,"TAG","Grey Sanitizer Bracelet"),
                            new Attribute(1l,"SKU","SB-480V-I82H-1"),
                            new Attribute(1l,"Weight","0.088 kg"),
                            new Attribute(1l,"Color","Grey"),
                            new Attribute(1l,"Dimensions","11.811 × 3.543 × 0.951 cm")
                    )),
                    null
            );
        }
        else if(alias.equals("grey-sanitizer-bracelet-amazon-ca")){
            detailView = new DetailView("Bracelet > Silicone Bracelet on Amazon Canada",
                    "Grey Sanitizer Bracelet on Canada",
                    "grey-sanitizer-bracelet-amazon-ca",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/Grey-Bracelet-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Easy-to-Refill-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Product-Features-04-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Where-is-it-needed-scaled.jpg")),
                    new ArrayList<>(Arrays.asList(
                            new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and soft silicone. Adjustable strap makes it ideal for the whole family. The package do not contain any liquids fill the empty refill bottle with the fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. If you intend to travel, just fill in your refill with the necessary product and enjoy your trip."),
                            new Info(1l,"Quick Description","<b>Step 1:</b> Fill the bottle with the liquid of your choice (fill it up before sleeping at night or leaving home some you are never unprotected)<br><br><b>Step 2:</b> Remove bracelet cover and insert the bottle nozzle in the hole, pressing the bottle until the tank is full<br><br><b>Step 3:</b> After filling the tank, replace the cover and wear the band on your wrist<br><br><b>Step 4:</b> To use remove the cover, squeeze to dispense some amount on your palm and apply as per need.<br><br><b>Step 5:</b> Remember to replace the cover after use.<br><br><b>✅✅Protect yourself by placing an order today, go ahead and click ‘Add to Cart’ right away! ✅✅</b>"),
                            new Info(1l,"Product Highlight","✅ <b>SANITIZE WHILE YOU MOVE:</b> Your hand sanitizer will be with you whenever you need it. Fill it up with your own sanitiser in the morning and use it throughout the day.<br>✅ <b>EASY TO USE:</b> The wristband is hassle free. Remove the stopper and press the tank to dispense as much liquid as needed.<br>✅ <b>VERSATILE & REUSABLE:</b> The silicone bracelet can hold up to 15 ml of sanitizer (at least 10-15 uses per fill!). Not only that, you can fill it with moisturizing cream or sunscreen too.<br>✅ <b>PROTECTION FOR THE WHOLE FAMILY EVERYWHERE:</b> Encourage the whole family to use the wearable. The strap fits children, teens, men and women. Use it in school, at work; while shopping, traveling and workout.<br>✅ <b>HIGH QUALITY:</b> Silicon used for the bracelet is durable, flexible, and waterproof. Love it or we’ll buy it back from you, no questions asked!")
                    )),
                    true,
                    "https://www.amazon.ca/dp/B08HY92GL4?",
                    100L,
                    0f,
                    new ArrayList<>(Arrays.asList(
                            new Attribute(1l,"TAG","Grey Sanitizer Bracelet"),
                            new Attribute(1l,"SKU","SB-480V-I82H-1-1"),
                            new Attribute(1l,"Weight","0.088 kg"),
                            new Attribute(1l,"Color","Grey"),
                            new Attribute(1l,"Dimensions","11.811 × 3.543 × 0.951 cm")
                    )),
                    null
            );
        }
        else if(alias.equals("peach-sanitizer-bracelet")){
            detailView = new DetailView("Bracelet > Silicone Bracelet",
                    "Peach Sanitizer Bracelet",
                    "peach-sanitizer-bracelet",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/Yellow-Watch-Side-View.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Easy-to-Refill-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Product-Features-04-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Where-is-it-needed-scaled.jpg")),
                    new ArrayList<>(Arrays.asList(
                            new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and soft silicone. Adjustable strap makes it ideal for the whole family. The package do not contain any liquids fill the empty refill bottle with the fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. If you intend to travel, just fill in your refill with the necessary product and enjoy your trip."),
                            new Info(1l,"Quick Description","<b>Step 1:</b> Fill the bottle with the liquid of your choice (fill it up before sleeping at night or leaving home some you are never unprotected)<br><br><b>Step 2:</b> Remove bracelet cover and insert the bottle nozzle in the hole, pressing the bottle until the tank is full<br><br><b>Step 3:</b> After filling the tank, replace the cover and wear the band on your wrist<br><br><b>Step 4:</b> To use remove the cover, squeeze to dispense some amount on your palm and apply as per need.<br><br><b>Step 5:</b> Remember to replace the cover after use.<br><br><b>✅✅Protect yourself by placing an order today, go ahead and click ‘Add to Cart’ right away! ✅✅</b>"),
                            new Info(1l,"Product Highlight","✅ <b>SANITIZE WHILE YOU MOVE:</b> Your hand sanitizer will be with you whenever you need it. Fill it up with your own sanitiser in the morning and use it throughout the day.<br>✅ <b>EASY TO USE:</b> The wristband is hassle free. Remove the stopper and press the tank to dispense as much liquid as needed.<br>✅ <b>VERSATILE & REUSABLE:</b> The silicone bracelet can hold up to 15 ml of sanitizer (at least 10-15 uses per fill!). Not only that, you can fill it with moisturizing cream or sunscreen too.<br>✅ <b>PROTECTION FOR THE WHOLE FAMILY EVERYWHERE:</b> Encourage the whole family to use the wearable. The strap fits children, teens, men and women. Use it in school, at work; while shopping, traveling and workout.<br>✅ <b>HIGH QUALITY:</b> Silicon used for the bracelet is durable, flexible, and waterproof. Love it or we’ll buy it back from you, no questions asked!")
                    )),
                    false,
                    150,
                    new ArrayList<>(Arrays.asList(
                            new Inventory(1l,70,"Chicago Warehouse","United States of America","Illinois","Chicago","60603","Chicago Lane","New Highway","1","8521456753","usaconnect@gmail.com"),
                            new Inventory(1l,80,"New York Warehouse","United States of America","New York","New York","10009","New York Lane","New Highway","1","2456895123","usaconnect@gmail.com")
                    )),
                    new ArrayList<>(Arrays.asList(
                            new Price(1l,"USD",9.87f,7.59f),
                            new Price(1l,"CAD",12.99f,9.99f)
                    )),
                    100L,
                    4.5f,
                    new ArrayList<>(
                            Arrays.asList(
                                    new Attribute(1l,"TAG","Peach Sanitizer Bracelet"),
                                    new Attribute(1l,"SKU","H4-480O-I86H"),
                                    new Attribute(1l,"Weight","0.088 kg"),
                                    new Attribute(1l,"Color","Peach"),
                                    new Attribute(1l,"Dimensions","11.811 × 3.543 × 0.951 cm")
                            )
                    ),
                    null
            );
        }
        else if(alias.equals("peach-sanitizer-bracelet-amazon")){
            detailView = new DetailView("Bracelet > Silicone Bracelet on Amazon",
                    "Peach Sanitizer Bracelet on Amazon",
                    "peach-sanitizer-bracelet-amazon",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/Yellow-Watch-Side-View.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Easy-to-Refill-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Product-Features-04-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Where-is-it-needed-scaled.jpg")),
                    new ArrayList<>(Arrays.asList(
                            new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and soft silicone. Adjustable strap makes it ideal for the whole family. The package do not contain any liquids fill the empty refill bottle with the fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. If you intend to travel, just fill in your refill with the necessary product and enjoy your trip."),
                            new Info(1l,"Quick Description","<b>Step 1:</b> Fill the bottle with the liquid of your choice (fill it up before sleeping at night or leaving home some you are never unprotected)<br><br><b>Step 2:</b> Remove bracelet cover and insert the bottle nozzle in the hole, pressing the bottle until the tank is full<br><br><b>Step 3:</b> After filling the tank, replace the cover and wear the band on your wrist<br><br><b>Step 4:</b> To use remove the cover, squeeze to dispense some amount on your palm and apply as per need.<br><br><b>Step 5:</b> Remember to replace the cover after use.<br><br><b>✅✅Protect yourself by placing an order today, go ahead and click ‘Add to Cart’ right away! ✅✅</b>"),
                            new Info(1l,"Product Highlight","✅ <b>SANITIZE WHILE YOU MOVE:</b> Your hand sanitizer will be with you whenever you need it. Fill it up with your own sanitiser in the morning and use it throughout the day.<br>✅ <b>EASY TO USE:</b> The wristband is hassle free. Remove the stopper and press the tank to dispense as much liquid as needed.<br>✅ <b>VERSATILE & REUSABLE:</b> The silicone bracelet can hold up to 15 ml of sanitizer (at least 10-15 uses per fill!). Not only that, you can fill it with moisturizing cream or sunscreen too.<br>✅ <b>PROTECTION FOR THE WHOLE FAMILY EVERYWHERE:</b> Encourage the whole family to use the wearable. The strap fits children, teens, men and women. Use it in school, at work; while shopping, traveling and workout.<br>✅ <b>HIGH QUALITY:</b> Silicon used for the bracelet is durable, flexible, and waterproof. Love it or we’ll buy it back from you, no questions asked!")
                    )),
                    true,
                    "https://www.amazon.com/Sanitizer-Wristband-Refillable-Container-ShieldHands/dp/B08HY86JPH/",
                    100L,
                    0f,
                    new ArrayList<>(Arrays.asList(
                            new Attribute(1l,"TAG","Peach Sanitizer Bracelet"),
                            new Attribute(1l,"SKU","H4-480O-I86H-1"),
                            new Attribute(1l,"Weight","0.088 kg"),
                            new Attribute(1l,"Color","Peach"),
                            new Attribute(1l,"Dimensions","11.811 × 3.543 × 0.951 cm")
                    )),
                    null
            );
        }
        else if(alias.equals("peach-sanitizer-bracelet-amazon-ca")){
            detailView = new DetailView("Bracelet > Silicone Bracelet on Amazon Canada",
                    "Peach Sanitizer Bracelet on Canada",
                    "peach-sanitizer-bracelet-amazon-ca",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/Yellow-Watch-Side-View.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Easy-to-Refill-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Product-Features-04-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Where-is-it-needed-scaled.jpg")),
                    new ArrayList<>(Arrays.asList(
                            new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and soft silicone. Adjustable strap makes it ideal for the whole family. The package do not contain any liquids fill the empty refill bottle with the fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. If you intend to travel, just fill in your refill with the necessary product and enjoy your trip."),
                            new Info(1l,"Quick Description","<b>Step 1:</b> Fill the bottle with the liquid of your choice (fill it up before sleeping at night or leaving home some you are never unprotected)<br><br><b>Step 2:</b> Remove bracelet cover and insert the bottle nozzle in the hole, pressing the bottle until the tank is full<br><br><b>Step 3:</b> After filling the tank, replace the cover and wear the band on your wrist<br><br><b>Step 4:</b> To use remove the cover, squeeze to dispense some amount on your palm and apply as per need.<br><br><b>Step 5:</b> Remember to replace the cover after use.<br><br><b>✅✅Protect yourself by placing an order today, go ahead and click ‘Add to Cart’ right away! ✅✅</b>"),
                            new Info(1l,"Product Highlight","✅ <b>SANITIZE WHILE YOU MOVE:</b> Your hand sanitizer will be with you whenever you need it. Fill it up with your own sanitiser in the morning and use it throughout the day.<br>✅ <b>EASY TO USE:</b> The wristband is hassle free. Remove the stopper and press the tank to dispense as much liquid as needed.<br>✅ <b>VERSATILE & REUSABLE:</b> The silicone bracelet can hold up to 15 ml of sanitizer (at least 10-15 uses per fill!). Not only that, you can fill it with moisturizing cream or sunscreen too.<br>✅ <b>PROTECTION FOR THE WHOLE FAMILY EVERYWHERE:</b> Encourage the whole family to use the wearable. The strap fits children, teens, men and women. Use it in school, at work; while shopping, traveling and workout.<br>✅ <b>HIGH QUALITY:</b> Silicon used for the bracelet is durable, flexible, and waterproof. Love it or we’ll buy it back from you, no questions asked!")
                    )),
                    true,
                    "https://www.amazon.ca/dp/B08HY86JPH",
                    100L,
                    0f,
                    new ArrayList<>(Arrays.asList(
                            new Attribute(1l,"TAG","Peach Sanitizer Bracelet"),
                            new Attribute(1l,"SKU","H4-480O-I86H-1-1"),
                            new Attribute(1l,"Weight","0.088 kg"),
                            new Attribute(1l,"Color","Peach"),
                            new Attribute(1l,"Dimensions","11.811 × 3.543 × 0.951 cm")
                    )),
                    null
            );
        }
        else if(alias.equals("pvc-blue-sanitizer-wristband")){
            detailView = new DetailView("Wristband > PVC Wristband",
                    "PVC Blue Sanitizer Wristband [Pack of 2]",
                    "pvc-blue-sanitizer-wristband",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/Blue-side.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazy-colors-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/product-features-scaled.jpg")),
                    new ArrayList<>(Arrays.asList(
                            new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and washable PVC. Adjustable strap makes it ideal for the whole family. The package does not contain any liquid or refill bottle. Fill the wristband with fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. Fill it up before sleeping at night or leaving home some you are never unprotected."),
                            new Info(1l,"How to Use","✅ Insert the outlet of the hand sanitizer bottle into the inlet of the wristband.<br>✅ Press and fill 8-10 ml hand sanitizer into the wristband.<br>✅ Insert the entrance of the wristband into the pocket.<br>✅ Wear the wristband by adjusting to suitable position.<br>✅ Takeout the entrance of the wristband and press ShieldHands logo to squeeze some amount on your hand.<br>✅ Rub the hands together to spread all over.<br>✅ Put the entrance of the wristband back into the pocket.<br><br>✅✅Protect yourself and your loved ones by placing an order today. Go ahead and click ‘Add to Cart’ right away! ✅✅"),
                            new Info(1l,"Package Contents","2 x PVC Sanitizer Bracelet"),
                            new Info(1l,"Product Highlight","✅ <b>SANITIZE WHILE YOU MOVE:</b> Hand sanitizer with you whenever you need it. Fill it up with fluid of your choice in the morning and you are all set! Wear it on your hand or clip it to your backpack. 2PC means two mini sanitiser containers with you on the go (Dispenser comes empty)<br>✅ <b>EASY TO USE:</b> The wristband is hassle-free. Just fill the empty chamber with sanitizer fluid or gel using a bottle. To use, take out the entrance of the wristband and squeeze out some amount.<br>✅ <b>VERSATILE & REUSABLE:</b> The PVC bracelet can hold up to 8-10 ml of liquid (at least 8-10 uses per fill!). Not only that, you can fill it with moisturizing cream or sunscreen too. The innovative design ensures that liquid only comes out when you need it.<br>✅ <b>FOR THE FAMILY, PERFECT GIFT:</b> Encourage the whole family to use the wearable. The adjustable strap makes it ideal for kids, teens, and adults alike. Use it at school, parks, vacations, trips to the store, at work, while shopping, workout, or traveling; anywhere washing hands is not an option.<br>✅ <b>HIGH QUALITY:</b> PVC used for the wearable is flexible, durable, washable, and smooth to touch. Love it or we’ll buy it back from you, no questions asked! Perfect party favors for Birthday goody bags, Easter eggs, Gifts, Christmas presents and much more!")
                    )),
                    false,
                    0,
                    new ArrayList<>(Arrays.asList(
                            new Inventory(1l,0,"Boston Warehouse","United States of America","Massachusetts","Boston","02111","Boston Lane","New Highway","1","7899877898","usaconnect@gmail.com")
                    )),
                    new ArrayList<>(Arrays.asList(
                            new Price(1l,"USD",3.8f,3.8f),
                            new Price(1l,"CAD",5.0f,5.0f)
                    )),
                    100L,
                    4.5f,
                    new ArrayList<>(
                            Arrays.asList(
                                    new Attribute(1l,"TAG","Blue Sanitizer Wristband"),
                                    new Attribute(1l,"SKU","A7-S717-B0NK"),
                                    new Attribute(1l,"Weight","0.088 kg"),
                                    new Attribute(1l,"Color","Blue")
                            )
                    ),
                    null
            );
        }

        else if(alias.equals("pvc-grey-sanitizer-wristband")){
            detailView = new DetailView("Wristband > PVC Wristband",
                    "PVC Grey Sanitizer Wristband [Pack of 2]",
                    "pvc-grey-sanitizer-wristband",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/Grey-side.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazy-colors-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/product-features-scaled.jpg")),
                    new ArrayList<>(Arrays.asList(
                            new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and washable PVC. Adjustable strap makes it ideal for the whole family. The package does not contain any liquid or refill bottle. Fill the wristband with fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. Fill it up before sleeping at night or leaving home some you are never unprotected."),
                            new Info(1l,"How to Use","✅ Insert the outlet of the hand sanitizer bottle into the inlet of the wristband.<br>✅ Press and fill 8-10 ml hand sanitizer into the wristband.<br>✅ Insert the entrance of the wristband into the pocket.<br>✅ Wear the wristband by adjusting to suitable position.<br>✅ Takeout the entrance of the wristband and press ShieldHands logo to squeeze some amount on your hand.<br>✅ Rub the hands together to spread all over.<br>✅ Put the entrance of the wristband back into the pocket.<br><br>✅✅Protect yourself and your loved ones by placing an order today. Go ahead and click ‘Add to Cart’ right away! ✅✅"),
                            new Info(1l,"Package Contents","2 x PVC Sanitizer Bracelet"),
                            new Info(1l,"Product Highlight","✅ <b>SANITIZE WHILE YOU MOVE:</b> Hand sanitizer with you whenever you need it. Fill it up with fluid of your choice in the morning and you are all set! Wear it on your hand or clip it to your backpack. 2PC means two mini sanitiser containers with you on the go (Dispenser comes empty)<br>✅ <b>EASY TO USE:</b> The wristband is hassle-free. Just fill the empty chamber with sanitizer fluid or gel using a bottle. To use, take out the entrance of the wristband and squeeze out some amount.<br>✅ <b>VERSATILE & REUSABLE:</b> The PVC bracelet can hold up to 8-10 ml of liquid (at least 8-10 uses per fill!). Not only that, you can fill it with moisturizing cream or sunscreen too. The innovative design ensures that liquid only comes out when you need it.<br>✅ <b>FOR THE FAMILY, PERFECT GIFT:</b> Encourage the whole family to use the wearable. The adjustable strap makes it ideal for kids, teens, and adults alike. Use it at school, parks, vacations, trips to the store, at work, while shopping, workout, or traveling; anywhere washing hands is not an option.<br>✅ <b>HIGH QUALITY:</b> PVC used for the wearable is flexible, durable, washable, and smooth to touch. Love it or we’ll buy it back from you, no questions asked! Perfect party favors for Birthday goody bags, Easter eggs, Gifts, Christmas presents and much more!")
                    )),
                    false,
                    3,
                    new ArrayList<>(Arrays.asList(
                            new Inventory(1l,3,"Boston Warehouse","United States of America","Massachusetts","Boston","02111","Boston Lane","New Highway","1","7899877898","usaconnect@gmail.com")
                    )),
                    new ArrayList<>(Arrays.asList(
                            new Price(1l,"USD",3.8f,3.8f),
                            new Price(1l,"CAD",5.0f,5.0f)
                    )),
                    100L,
                    4.5f,
                    new ArrayList<>(
                            Arrays.asList(
                                    new Attribute(1l,"TAG","Grey Sanitizer Wristband"),
                                    new Attribute(1l,"SKU","PY-9XYU-N6GP"),
                                    new Attribute(1l,"Weight","0.088 kg"),
                                    new Attribute(1l,"Color","Grey")
                            )
                    ),
                    null
            );
        }

        else if(alias.equals("pvc-green-sanitizer-wristband")){
            detailView = new DetailView("Wristband > PVC Wristband",
                    "PVC Green Sanitizer Wristband [Pack of 2]",
                    "pvc-green-sanitizer-wristband",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/Green-side.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazy-colors-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/product-features-scaled.jpg")),
                    new ArrayList<>(Arrays.asList(
                            new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and washable PVC. Adjustable strap makes it ideal for the whole family. The package does not contain any liquid or refill bottle. Fill the wristband with fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. Fill it up before sleeping at night or leaving home some you are never unprotected."),
                            new Info(1l,"How to Use","✅ Insert the outlet of the hand sanitizer bottle into the inlet of the wristband.<br>✅ Press and fill 8-10 ml hand sanitizer into the wristband.<br>✅ Insert the entrance of the wristband into the pocket.<br>✅ Wear the wristband by adjusting to suitable position.<br>✅ Takeout the entrance of the wristband and press ShieldHands logo to squeeze some amount on your hand.<br>✅ Rub the hands together to spread all over.<br>✅ Put the entrance of the wristband back into the pocket.<br><br>✅✅Protect yourself and your loved ones by placing an order today. Go ahead and click ‘Add to Cart’ right away! ✅✅"),
                            new Info(1l,"Package Contents","2 x PVC Sanitizer Bracelet"),
                            new Info(1l,"Product Highlight","✅ <b>SANITIZE WHILE YOU MOVE:</b> Hand sanitizer with you whenever you need it. Fill it up with fluid of your choice in the morning and you are all set! Wear it on your hand or clip it to your backpack. 2PC means two mini sanitiser containers with you on the go (Dispenser comes empty)<br>✅ <b>EASY TO USE:</b> The wristband is hassle-free. Just fill the empty chamber with sanitizer fluid or gel using a bottle. To use, take out the entrance of the wristband and squeeze out some amount.<br>✅ <b>VERSATILE & REUSABLE:</b> The PVC bracelet can hold up to 8-10 ml of liquid (at least 8-10 uses per fill!). Not only that, you can fill it with moisturizing cream or sunscreen too. The innovative design ensures that liquid only comes out when you need it.<br>✅ <b>FOR THE FAMILY, PERFECT GIFT:</b> Encourage the whole family to use the wearable. The adjustable strap makes it ideal for kids, teens, and adults alike. Use it at school, parks, vacations, trips to the store, at work, while shopping, workout, or traveling; anywhere washing hands is not an option.<br>✅ <b>HIGH QUALITY:</b> PVC used for the wearable is flexible, durable, washable, and smooth to touch. Love it or we’ll buy it back from you, no questions asked! Perfect party favors for Birthday goody bags, Easter eggs, Gifts, Christmas presents and much more!")
                    )),
                    false,
                    8,
                    new ArrayList<>(Arrays.asList(
                            new Inventory(1l,8,"Boston Warehouse","United States of America","Massachusetts","Boston","02111","Boston Lane","New Highway","1","7899877898","usaconnect@gmail.com")
                    )),
                    new ArrayList<>(Arrays.asList(
                            new Price(1l,"USD",3.8f,3.8f),
                            new Price(1l,"CAD",5.0f,5.0f)
                    )),
                    100L,
                    4.5f,
                    new ArrayList<>(
                            Arrays.asList(
                                    new Attribute(1l,"TAG","Green Sanitizer Wristband"),
                                    new Attribute(1l,"SKU","5W-KV7O-E6NY"),
                                    new Attribute(1l,"Weight","0.088 kg"),
                                    new Attribute(1l,"Color","Green")
                            )
                    ),
                    null
            );
        }
        else if(alias.equals("pvc-pink-sanitizer-wristband")){
            detailView = new DetailView("Wristband > PVC Wristband on Amazon",
                    "PVC Pink Sanitizer Wristband [Pack of 2]",
                    "pvc-pink-sanitizer-wristband",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/pink-side-view-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Blue-Watch-Side-View-02.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Product-Features-04-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Where-is-it-needed-scaled.jpg")),
                    new ArrayList<>(Arrays.asList(
                            new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and soft silicone. Adjustable strap makes it ideal for the whole family. The package do not contain any liquids fill the empty refill bottle with the fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. If you intend to travel, just fill in your refill with the necessary product and enjoy your trip."),
                            new Info(1l,"Quick Description","<b>Step 1:</b> Fill the bottle with the liquid of your choice (fill it up before sleeping at night or leaving home some you are never unprotected)<br><br><b>Step 2:</b> Remove bracelet cover and insert the bottle nozzle in the hole, pressing the bottle until the tank is full<br><br><b>Step 3:</b> After filling the tank, replace the cover and wear the band on your wrist<br><br><b>Step 4:</b> To use remove the cover, squeeze to dispense some amount on your palm and apply as per need.<br><br><b>Step 5:</b> Remember to replace the cover after use.<br><br><b>✅✅Protect yourself by placing an order today, go ahead and click ‘Add to Cart’ right away! ✅✅</b>"),
                            new Info(1l,"Product Highlight","✅ <b>SANITIZE WHILE YOU MOVE:</b> Your hand sanitizer will be with you whenever you need it. Fill it up with your own sanitiser in the morning and use it throughout the day.<br>✅ <b>EASY TO USE:</b> The wristband is hassle free. Remove the stopper and press the tank to dispense as much liquid as needed.<br>✅ <b>VERSATILE & REUSABLE:</b> The silicone bracelet can hold up to 15 ml of sanitizer (at least 10-15 uses per fill!). Not only that, you can fill it with moisturizing cream or sunscreen too.<br>✅ <b>PROTECTION FOR THE WHOLE FAMILY EVERYWHERE:</b> Encourage the whole family to use the wearable. The strap fits children, teens, men and women. Use it in school, at work; while shopping, traveling and workout.<br>✅ <b>HIGH QUALITY:</b> Silicon used for the bracelet is durable, flexible, and waterproof. Love it or we’ll buy it back from you, no questions asked!")
                    )),
                    true,
                    "https://www.amazon.com/dp/B08JK7JTMP",
                    100L,
                    0f,
                    new ArrayList<>(Arrays.asList(
                            new Attribute(1l,"TAG","Pink Sanitizer Wristband"),
                            new Attribute(1l,"SKU","T1-J329-CHG2"),
                            new Attribute(1l,"Weight","0.088 kg"),
                            new Attribute(1l,"Color","Pink")
                    )),
                    null
            );
        }
        else if(alias.equals("pvc-white-sanitizer-wristband")){
            detailView = new DetailView("Wristband > PVC Wristband on Amazon",
                    "PVC White Sanitizer Wristband [Pack of 2]",
                    "pvc-white-sanitizer-wristband",
                    "Shield Hands",
                    new ArrayList<>(Arrays.asList("https://shieldhands.com/wp-content/uploads/2020/10/white-side.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Blue-Watch-Side-View-02.jpg","https://shieldhands.com/wp-content/uploads/2020/10/All-snazzy-colors-Final-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Product-Features-04-scaled.jpg","https://shieldhands.com/wp-content/uploads/2020/10/Where-is-it-needed-scaled.jpg")),
                    new ArrayList<>(Arrays.asList(
                            new Info(1l,"Product description","<b>Never forget to protect your hands in the hustle and bustle of daily activities.</b><br><br>Made with high quality, durable and soft silicone. Adjustable strap makes it ideal for the whole family. The package do not contain any liquids fill the empty refill bottle with the fluid of your choice (sanitizer, moisturizing cream, sunscreen lotion, etc.) and start using. If you intend to travel, just fill in your refill with the necessary product and enjoy your trip."),
                            new Info(1l,"Quick Description","<b>Step 1:</b> Fill the bottle with the liquid of your choice (fill it up before sleeping at night or leaving home some you are never unprotected)<br><br><b>Step 2:</b> Remove bracelet cover and insert the bottle nozzle in the hole, pressing the bottle until the tank is full<br><br><b>Step 3:</b> After filling the tank, replace the cover and wear the band on your wrist<br><br><b>Step 4:</b> To use remove the cover, squeeze to dispense some amount on your palm and apply as per need.<br><br><b>Step 5:</b> Remember to replace the cover after use.<br><br><b>✅✅Protect yourself by placing an order today, go ahead and click ‘Add to Cart’ right away! ✅✅</b>"),
                            new Info(1l,"Product Highlight","✅ <b>SANITIZE WHILE YOU MOVE:</b> Your hand sanitizer will be with you whenever you need it. Fill it up with your own sanitiser in the morning and use it throughout the day.<br>✅ <b>EASY TO USE:</b> The wristband is hassle free. Remove the stopper and press the tank to dispense as much liquid as needed.<br>✅ <b>VERSATILE & REUSABLE:</b> The silicone bracelet can hold up to 15 ml of sanitizer (at least 10-15 uses per fill!). Not only that, you can fill it with moisturizing cream or sunscreen too.<br>✅ <b>PROTECTION FOR THE WHOLE FAMILY EVERYWHERE:</b> Encourage the whole family to use the wearable. The strap fits children, teens, men and women. Use it in school, at work; while shopping, traveling and workout.<br>✅ <b>HIGH QUALITY:</b> Silicon used for the bracelet is durable, flexible, and waterproof. Love it or we’ll buy it back from you, no questions asked!")
                    )),
                    true,
                    "https://www.amazon.com/dp/B08JK78JZV",
                    100L,
                    0f,
                    new ArrayList<>(Arrays.asList(
                            new Attribute(1l,"TAG","White Sanitizer Bracelet"),
                            new Attribute(1l,"SKU","FA-U9FA-1ODE"),
                            new Attribute(1l,"Weight","0.088 kg"),
                            new Attribute(1l,"Color","White")
                    )),
                    null
            );
        }

        return detailView;
    }
}
