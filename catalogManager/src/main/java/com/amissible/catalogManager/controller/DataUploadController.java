package com.amissible.catalogManager.controller;

import com.amissible.beans.exception.AppException;
import com.amissible.catalogManager.service.CatalogService;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.apache.tomcat.util.json.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/dataImport")
public class DataUploadController {

    @Autowired
    private CatalogService catalogService;

    @RequestMapping("/uploadProducts")
    public Object uploadCSVFile(@RequestParam("file") MultipartFile file, Model model) {
        String response = "";
        Object obj = null;
        if (file.isEmpty()) {
            model.addAttribute("message", "Please select a CSV file to upload.");
            model.addAttribute("status", false);
        } else {
            try{
                CSVParser parser = new CSVParserBuilder().withSeparator(',').build();
                Reader br = new BufferedReader(new InputStreamReader(file.getInputStream()));
                CSVReader reader = new CSVReaderBuilder(br).withCSVParser(parser).build();
                List<String[]> rows = reader.readAll();
                for(int i=1;i<rows.size();i++){
                    String row[] = rows.get(i);
                    try{
                        catalogService.uploadRecord(rows.get(i));
                        response += ",{record:"+i+",status:\"SUCCESS\",message:null}";
                    }
                    catch (Exception e){
                        response += ",{record:"+i+",status:\"FAILURE\",message:\""+e.getMessage()+"\"}";
                    }
                }
                response = "["+response.substring(1)+"]";
                obj = (new JSONParser(response)).parse();
            }
            catch (Exception e){
                throw new AppException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return obj;
    }
}
