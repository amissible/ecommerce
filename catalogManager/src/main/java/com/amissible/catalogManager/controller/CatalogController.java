package com.amissible.catalogManager.controller;

import com.amissible.beans.model.Menu;
import com.amissible.catalogManager.model.Catalog;
import com.amissible.catalogManager.model.DetailView;
import com.amissible.catalogManager.model.QuickView;
import com.amissible.catalogManager.service.CatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/*
This Controller will provide database data to UI
 */

@RestController
@CrossOrigin
@RequestMapping("/db")
public class CatalogController {

    @Autowired
    private CatalogService catalogService;

    @RequestMapping(value = "/getHeaderMenu")
    @ResponseBody
    public List<Menu> getHeaderMenu(){
        return catalogService.getHeaderMenu();
    }

    @RequestMapping(value = "/getFooterMenu")
    @ResponseBody
    public List<Menu> getFooterMenu(){
        return catalogService.getFooterMenu();
    }

    @RequestMapping(value = "/addMenuItem")
    @ResponseBody
    public Long addMenuItem(@RequestParam String item_name,@RequestParam String icon,@RequestParam String route,@RequestParam Long parent_id,@RequestParam Integer sort_order){
        return catalogService.addMenuItem(item_name, icon, route, parent_id, sort_order);
    }

/*


    @RequestMapping(value = "/removeMenuItem")
    @ResponseBody
    public Long removeMenuItem(@RequestParam Long id){
        return catalogService.removeMenuItem(id);
    }
    */


    @RequestMapping(value = "/catalog")
    @ResponseBody
    List<Catalog> getCatalogData(){
        return catalogService.getCatalogData();
    }

    @RequestMapping(value = "/quickview/{alias}")
    @ResponseBody
    QuickView getQuickViewData(@PathVariable("alias") String alias){
        return catalogService.getQuickViewData(alias);
    }

    @RequestMapping(value = "/product/{alias}")
    @ResponseBody
    DetailView getDetailViewData(@PathVariable("alias") String alias){
        return catalogService.getDetailViewData(alias);
    }

    @RequestMapping(value = "/search/{query}")
    @ResponseBody
    List<Catalog> getSearchData(@PathVariable("query") String query){
        return catalogService.getSearchData(query);
    }

    @RequestMapping(value = "/hardsearch/{query}")
    @ResponseBody
    List<Catalog> getHardSearchData(@PathVariable("query") String query){
        return catalogService.getHardSearchData(query);
    }
/*
    @RequestMapping(method = RequestMethod.GET, path = "/category")
    @ResponseBody
    List<Category> getCategoryByParentID(@RequestParam("id") Long parent_id){

        return categoryService.getCategoryByParentID(parent_id);
    }
*/

    /*@RequestMapping(method = RequestMethod.GET, path = "/category")
    @ResponseBody
    List<Category> getOrganization(@RequestParam("id") Long parent_id){

        return categoryService.getCategoryByParentID(parent_id);
    }*/



    @RequestMapping(value = "/functions/getProductIdByAlias/{alias}")
    @ResponseBody
    Long getProductIdByAlias(@PathVariable("alias") String alias){
        return catalogService.getProductIdByAlias(alias);
    }

}
