package com.amissible.catalogManager.controller;

import com.amissible.beans.model.Menu;
import com.amissible.catalogManager.service.CatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/dataExport")
public class DataExportController {

    @Autowired
    private CatalogService catalogService;

    @RequestMapping(value = "/exportProducts")
    @ResponseBody
    public ResponseEntity exportProducts(){
        return catalogService.exportProducts();
    }

    @RequestMapping(value = "/exportMenuRecords")
    @ResponseBody
    public ResponseEntity exportMenuRecords(){
        return catalogService.exportMenuRecords();
    }

}
