package com.amissible.catalogManager.model;

public class Info {

    private String info_head;
    private String info_body;
    private Long product_Id;

    public Info(Long productId,String info_head, String info_body) {
        this.product_Id = productId;
        this.info_head = info_head;
        this.info_body = info_body;
    }

    public String getInfo_head() {
        return info_head;
    }

    public void setInfo_head(String info_head) {
        this.info_head = info_head;
    }

    public String getInfo_body() {
        return info_body;
    }

    public void setInfo_body(String info_body) {
        this.info_body = info_body;
    }

    public Long getProduct_Id() {
        return product_Id;
    }

    public void setProduct_Id(Long product_Id) {
        this.product_Id = product_Id;
    }

    @Override
    public String toString() {
        return "Info{" +
                "info_head='" + info_head + '\'' +
                ", info_body='" + info_body + '\'' +
                '}';
    }
}
