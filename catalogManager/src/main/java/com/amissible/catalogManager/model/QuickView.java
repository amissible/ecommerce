package com.amissible.catalogManager.model;

import java.util.List;

public class QuickView {

    private String product_name;
    private String alias;
    private String brand;
    private List<String> image_path;
    private Boolean isPromotional;
    private Integer stock_quantity;
    private List<Price> price;
    private String url;
    private List<Info> info;

    public QuickView() {
    }

    public QuickView(String product_name, String alias, String brand, List<String> image_path, Boolean isPromotional, Integer stock_quantity, List<Price> price, List<Info> info) {
        this.product_name = product_name;
        this.alias = alias;
        this.brand = brand;
        this.image_path = image_path;
        this.isPromotional = isPromotional;
        this.stock_quantity = stock_quantity;
        this.price = price;
        this.info = info;
    }

    public QuickView(String product_name, String alias, String brand, List<String> image_path, Boolean isPromotional, String url, List<Info> info) {
        this.product_name = product_name;
        this.alias = alias;
        this.brand = brand;
        this.image_path = image_path;
        this.isPromotional = isPromotional;
        this.url = url;
        this.info = info;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public List<String> getImage_path() {
        return image_path;
    }

    public void setImage_path(List<String> image_path) {
        this.image_path = image_path;
    }

    public Boolean getPromotional() {
        return isPromotional;
    }

    public void setPromotional(Boolean promotional) {
        isPromotional = promotional;
    }

    public Integer getStock_quantity() {
        return stock_quantity;
    }

    public void setStock_quantity(Integer stock_quantity) {
        this.stock_quantity = stock_quantity;
    }

    public List<Price> getPrice() {
        return price;
    }

    public void setPrice(List<Price> price) {
        this.price = price;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Info> getInfo() {
        return info;
    }

    public void setInfo(List<Info> info) {
        this.info = info;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    @Override
    public String toString() {
        return "QuickView{" +
                "product_name='" + product_name + '\'' +
                ", alias='" + alias + '\'' +
                ", brand='" + brand + '\'' +
                ", image_path=" + image_path +
                ", isPromotional=" + isPromotional +
                ", stock_quantity=" + stock_quantity +
                ", price=" + price +
                ", url='" + url + '\'' +
                ", info=" + info +
                '}';
    }


    public void evaluateDiscount(){
        int length = this.price.size();
        for(int i=0;i<length;i++)
            this.price.get(i).evaluateDiscount();
    }
}
