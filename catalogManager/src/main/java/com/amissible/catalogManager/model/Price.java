package com.amissible.catalogManager.model;

public class Price {
    private Long product_Id;
    private String currency;
    private Float marked_price;
    private Float sale_price;
    private Integer discount;

    public Price(Long productId,String currency, Float marked_price, Float sale_price) {
        this.product_Id=productId;
        this.currency = currency;
        this.marked_price = marked_price;
        this.sale_price = sale_price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Float getMarked_price() {
        return marked_price;
    }

    public void setMarked_price(Float marked_price) {
        this.marked_price = marked_price;
    }

    public Float getSale_price() {
        return sale_price;
    }

    public void setSale_price(Float sale_price) {
        this.sale_price = sale_price;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public void evaluateDiscount(){
        this.discount = (int)(100*(1-this.sale_price/this.marked_price));
    }

    public Long getProduct_Id() {
        return product_Id;
    }

    public void setProduct_Id(Long product_Id) {
        this.product_Id = product_Id;
    }

    @Override
    public String toString() {
        return "Price{" +
                "currency='" + currency + '\'' +
                ", marked_price=" + marked_price +
                ", sale_price=" + sale_price +
                ", discount=" + discount +
                '}';
    }
}
