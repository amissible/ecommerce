package com.amissible.catalogManager.model;

public class Inventory {

    private Long product_Id;
    private Integer quantity;
    private String source_name;
    private String country;
    private String state;
    private String city;
    private String postcode;
    private String line1;
    private String line2;
    private String contact_cc;
    private String contact_cn;
    private String email;

    public Inventory(Integer quantity) {
        this.quantity = quantity;
    }

    public Inventory(Long product_Id,Integer quantity, String source_name, String country, String state, String city, String postcode, String line1, String line2, String contact_cc, String contact_cn, String email) {
        this.product_Id = product_Id;
        this.quantity = quantity;
        this.source_name = source_name;
        this.country = country;
        this.state = state;
        this.city = city;
        this.postcode = postcode;
        this.line1 = line1;
        this.line2 = line2;
        this.contact_cc = contact_cc;
        this.contact_cn = contact_cn;
        this.email = email;
    }

    public Long getProduct_Id() {
        return product_Id;
    }

    public void setProduct_Id(Long product_Id) {
        this.product_Id = product_Id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getSource_name() {
        return source_name;
    }

    public void setSource_name(String source_name) {
        this.source_name = source_name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getContact_cc() {
        return contact_cc;
    }

    public void setContact_cc(String contact_cc) {
        this.contact_cc = contact_cc;
    }

    public String getContact_cn() {
        return contact_cn;
    }

    public void setContact_cn(String contact_cn) {
        this.contact_cn = contact_cn;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Inventory{" +
                "quantity=" + quantity +
                ", source_name='" + source_name + '\'' +
                ", country='" + country + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", postcode='" + postcode + '\'' +
                ", line1='" + line1 + '\'' +
                ", line2='" + line2 + '\'' +
                ", contact_cc='" + contact_cc + '\'' +
                ", contact_cn='" + contact_cn + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
