package com.amissible.catalogManager.model;

import com.amissible.beans.model.Product;

public class SearchProduct extends Product {
    private Long rank;

    public SearchProduct() {
    }

    public SearchProduct(Long rank) {
        this.rank = rank;
    }

    public SearchProduct(Long id, Long rank) {
        super(id);
        this.rank = rank;
    }

    public SearchProduct(Product p){
        super(p.getId(),p.getProduct_name(),p.getAlias(),p.getBrand(),p.getCategory(),p.getUrl(),p.getIcon1_path(),p.getIcon2_path(),p.getImage_path(),p.getDatetimestamp(),p.getProduct_status());
        this.rank = 1L;
    }

    public void rankUpBy1(){
        this.rank = this.rank + 1L;
    }
}
