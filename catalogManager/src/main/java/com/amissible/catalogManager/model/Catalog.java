package com.amissible.catalogManager.model;

import com.amissible.beans.model.Product_price;

import java.util.List;

public class Catalog {

    private String category;
    private String product_name;//
    private String alias;//
    private String icon1_path;//
    private String icon2_path;//
    private Boolean isPromotional;//
    private Integer stock_quantity;//
    private List<Price> price;//
    private Boolean heart;

    public Catalog() {
    }

    public Catalog(String product_name, String alias, String icon1_path, String icon2_path, Boolean isPromotional, Boolean heart) {
        this.product_name = product_name;
        this.alias = alias;
        this.icon1_path = icon1_path;
        this.icon2_path = icon2_path;
        this.isPromotional = isPromotional;
        this.heart = heart;
    }

    public Catalog(String product_name, String alias, String icon1_path, String icon2_path, Boolean isPromotional, Integer stock_quantity, List<Price> price, Boolean heart) {
        this.product_name = product_name;
        this.alias = alias;
        this.icon1_path = icon1_path;
        this.icon2_path = icon2_path;
        this.isPromotional = isPromotional;
        this.stock_quantity = stock_quantity;
        this.price = price;
        this.heart = heart;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getIcon1_path() {
        return icon1_path;
    }

    public void setIcon1_path(String icon1_path) {
        this.icon1_path = icon1_path;
    }

    public String getIcon2_path() {
        return icon2_path;
    }

    public void setIcon2_path(String icon2_path) {
        this.icon2_path = icon2_path;
    }

    public Boolean getPromotional() {
        return isPromotional;
    }

    public void setPromotional(Boolean promotional) {
        isPromotional = promotional;
    }

    public Integer getStock_quantity() {
        return stock_quantity;
    }

    public void setStock_quantity(Integer stock_quantity) {
        this.stock_quantity = stock_quantity;
    }

    public List<Price> getPrice() {
        return price;
    }

    public void setPrice(List<Price> price) {
        this.price = price;
    }

    public Boolean getHeart() {
        return heart;
    }

    public void setHeart(Boolean heart) {
        this.heart = heart;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Catalog{" +
                "product_name='" + product_name + '\'' +
                ", alias='" + alias + '\'' +
                ", icon1_path='" + icon1_path + '\'' +
                ", icon2_path='" + icon2_path + '\'' +
                ", isPromotional=" + isPromotional +
                ", stock_quantity=" + stock_quantity +
                ", price=" + price +
                ", heart=" + heart +
                '}';
    }

    public void evaluateDiscount(){
        int length = this.price.size();
        for(int i=0;i<length;i++)
            this.price.get(i).evaluateDiscount();
    }
}
