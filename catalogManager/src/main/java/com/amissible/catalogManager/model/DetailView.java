package com.amissible.catalogManager.model;

import java.util.List;

public class DetailView {

    private String category;
    private String product_name;
    private String alias;
    private String brand;
    private List<String> image_path;
    private List<Info> info;

    private Boolean isPromotional;
    private Integer stock_quantity;
    private List<Inventory> inventoryList;
    private List<Price> price;
    private String url;

    private Long hits;
    private Float average_rating;
    private List<Attribute> attributes;
    private List<Review> reviews;

    public DetailView() {
    }

    public DetailView(String category, String product_name, String alias, String brand, List<String> image_path, List<Info> info, Boolean isPromotional, String url, Long hits, Float average_rating, List<Attribute> attributes, List<Review> reviews) {
        this.category = category;
        this.product_name = product_name;
        this.alias = alias;
        this.brand = brand;
        this.image_path = image_path;
        this.info = info;
        this.isPromotional = isPromotional;
        this.url = url;
        this.hits = hits;
        this.average_rating = average_rating;
        this.attributes = attributes;
        this.reviews = reviews;
    }

    public DetailView(String category, String product_name, String alias, String brand, List<String> image_path, List<Info> info, Boolean isPromotional, Integer stock_quantity, List<Inventory> inventoryList, List<Price> price, Long hits, Float average_rating, List<Attribute> attributes, List<Review> reviews) {
        this.category = category;
        this.product_name = product_name;
        this.alias = alias;
        this.brand = brand;
        this.image_path = image_path;
        this.info = info;
        this.isPromotional = isPromotional;
        this.stock_quantity = stock_quantity;
        this.inventoryList = inventoryList;
        this.price = price;
        this.hits = hits;
        this.average_rating = average_rating;
        this.attributes = attributes;
        this.reviews = reviews;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public List<String> getImage_path() {
        return image_path;
    }

    public void setImage_path(List<String> image_path) {
        this.image_path = image_path;
    }

    public List<Info> getInfo() {
        return info;
    }

    public void setInfo(List<Info> info) {
        this.info = info;
    }

    public Boolean getPromotional() {
        return isPromotional;
    }

    public void setPromotional(Boolean promotional) {
        isPromotional = promotional;
    }

    public Integer getStock_quantity() {
        return stock_quantity;
    }

    public void setStock_quantity(Integer stock_quantity) {
        this.stock_quantity = stock_quantity;
    }

    public List<Inventory> getInventoryList() {
        return inventoryList;
    }

    public void setInventoryList(List<Inventory> inventoryList) {
        this.inventoryList = inventoryList;
    }

    public List<Price> getPrice() {
        return price;
    }

    public void setPrice(List<Price> price) {
        this.price = price;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getHits() {
        return hits;
    }

    public void setHits(Long hits) {
        this.hits = hits;
    }

    public Float getAverage_rating() {
        return average_rating;
    }

    public void setAverage_rating(Float average_rating) {
        this.average_rating = average_rating;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public void evaluateQuantity(){
        int length = this.inventoryList.size();
        this.stock_quantity = 0;
        for(int i=0;i<length;i++)
            this.stock_quantity += this.inventoryList.get(i).getQuantity();
    }

    public void evaluateDiscount(){
        int length = this.price.size();
        for(int i=0;i<length;i++)
            this.price.get(i).evaluateDiscount();
    }
}