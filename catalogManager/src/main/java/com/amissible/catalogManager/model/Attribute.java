package com.amissible.catalogManager.model;

public class Attribute {
    private Long product_Id;
    private String attribute;
    private String attr_value;

    public Attribute(Long product_Id,String attribute, String attr_value) {
        this.product_Id = product_Id;
        this.attribute = attribute;
        this.attr_value = attr_value;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getAttr_value() {
        return attr_value;
    }

    public void setAttr_value(String attr_value) {
        this.attr_value = attr_value;
    }

    public Long getProduct_Id() {
        return product_Id;
    }

    public void setProduct_Id(Long product_Id) {
        this.product_Id = product_Id;
    }

    @Override
    public String toString() {
        return "Attribute{" +
                "attribute='" + attribute + '\'' +
                ", attr_value='" + attr_value + '\'' +
                '}';
    }
}
