package com.amissible.catalogManager.model;

public class Review {
    private String profile_image;
    private String cust_name;
    private String datetimestamp;
    private Float rating;
    private String review;
    private String review_image;

    public Review(String profile_image, String cust_name, String datetimestamp, Float rating, String review, String review_image) {
        this.profile_image = profile_image;
        this.cust_name = cust_name;
        this.datetimestamp = datetimestamp;
        this.rating = rating;
        this.review = review;
        this.review_image = review_image;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getDatetimestamp() {
        return datetimestamp;
    }

    public void setDatetimestamp(String datetimestamp) {
        this.datetimestamp = datetimestamp;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getReview_image() {
        return review_image;
    }

    public void setReview_image(String review_image) {
        this.review_image = review_image;
    }
}
