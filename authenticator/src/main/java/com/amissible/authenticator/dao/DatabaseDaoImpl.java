package com.amissible.authenticator.dao;

import com.amissible.beans.exception.AppException;
import com.amissible.beans.model.Customer;
import com.amissible.beans.model.Customer_login;
import com.amissible.beans.model.Roles;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.DatabaseMetaDataCallback;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.jdbc.support.MetaDataAccessException;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DatabaseDaoImpl implements DatabaseDao {

    @JsonProperty("table_map")
    Map<String, Integer> tableMap = new HashMap<>();

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    DataSource dataSource;

    public List<Customer> getAllCustomers(){
        List<Customer> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM customer", new Object[] { },
                (rs, rowNum) -> new Customer(
                        rs.getLong("id"),
                        rs.getString("first_name"),
                        rs.getString("last_name"),
                        rs.getString("email"),
                        rs.getString("contact_cc"),
                        rs.getString("contact_cn"),
                        rs.getString("image"),
                        rs.getString("datetimestamp")
                )
        ).forEach(record -> list.add(record));
        if (list.size() == 0)
            return null;
        else
            return list;
    }

    public Customer getCustomerByID(Long id){
        List<Customer> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM customer WHERE id = ?", new Object[] { id },
                (rs, rowNum) -> new Customer(
                        rs.getLong("id"),
                        rs.getString("first_name"),
                        rs.getString("last_name"),
                        rs.getString("email"),
                        rs.getString("contact_cc"),
                        rs.getString("contact_cn"),
                        rs.getString("image"),
                        rs.getString("datetimestamp")
                )
        ).forEach(record -> list.add(record));
        if (list.size() == 0)
            return null;
        else
            return list.get(0);
    }

    public Customer getCustomerByEmail(String email){
        List<Customer> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM customer WHERE email = ?", new Object[] { email },
                (rs, rowNum) -> new Customer(
                        rs.getLong("id"),
                        rs.getString("first_name"),
                        rs.getString("last_name"),
                        rs.getString("email"),
                        rs.getString("contact_cc"),
                        rs.getString("contact_cn"),
                        rs.getString("image"),
                        rs.getString("datetimestamp")
                )
        ).forEach(record -> list.add(record));
        if (list.size() == 0)
            return null;
        else
            return list.get(0);
    }

    public Long getCustomerIdByEmail(String email){
        List<Customer> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT id FROM customer WHERE email = ?", new Object[] { email },
                (rs, rowNum) -> new Customer(
                        rs.getLong("id")
                )
        ).forEach(record -> list.add(record));
        if (list.size() == 0)
            return 0L;
        else
            return list.get(0).getId();
    }

    //customer insertion
    public Long insertCustomer(String first_name,String last_name,String email,String contact_cc,String contact_cn,String image){
        Long id = 0L;
        try{
            Customer c = getCustomerByEmail(email);
            if(c != null)
                throw new AppException("Email ID already registered",HttpStatus.CONFLICT);
            jdbcTemplate.update(
                    "INSERT INTO customer(first_name,last_name,email,contact_cc,contact_cn,image) VALUES (?,?,?,?,?,?)",
                    new Object[] {
                            first_name,
                            last_name,
                            email,
                            contact_cc,
                            contact_cn,
                            image
                    });
            id = getCustomerIdByEmail(email);
        }
        catch (Exception e){
            throw new AppException("Customer Insertion Failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return id;
    }

    //customer deletion
    public boolean deleteCustomer(long id) {
        try {
            jdbcTemplate.update("DELETE FROM customer where id = ?", new Object[] { id });
        } catch (Exception e) {
            throw new AppException("Customer Deletion Failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return true;
    }

    public int getRows(String tableName) {
        try {
            String sql = "SELECT count(*) FROM " + tableName;

            int count = jdbcTemplate.queryForObject(
                    sql, new Object[] { }, Integer.class);
            return count;
        } catch (Exception e) {
            throw new AppException("Customer Deletion Failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public List<String> getTables() {
        String sql = "SHOW TABLES";

        return jdbcTemplate.query(
                sql,
                (rs, rowNum) ->
                        new String(
                                rs.getString("Tables_in_DEV")
                                )
        );
    }

    public Map<String, Integer> getTableRowMap() {
        List<String> tables = getTables();
        for(String tableName : tables) {
            tableMap.put(tableName, getRows(tableName));
        }
        return tableMap;
    }

    public Customer_login getLoginDetailsByID(Long id){
        List<Customer_login> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM customer_login WHERE id = ?", new Object[] { id },
                (rs, rowNum) -> new Customer_login(
                        rs.getLong("id"),
                        rs.getString("cust_username"),
                        rs.getString("cust_password"),
                        rs.getBoolean("cust_status")
                )
        ).forEach(record -> list.add(record));
        if (list.size() == 0)
            return null;
        else
            return list.get(0);
    }

    public Customer_login getLoginDetailsByUsername(String username){
        List<Customer_login> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM customer_login WHERE cust_username = ?", new Object[] { username },
                (rs, rowNum) -> new Customer_login(
                        rs.getLong("id"),
                        rs.getString("cust_username"),
                        rs.getString("cust_password"),
                        rs.getBoolean("cust_status")
                )
        ).forEach(record -> list.add(record));
        if (list.size() == 0)
            return null;
        else
            return list.get(0);
    }

    public Long getLoginDetailsIdByUsername(String username){
        List<Customer_login> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM customer_login WHERE cust_username = ?", new Object[] { username },
                (rs, rowNum) -> new Customer_login(
                        rs.getLong("id")
                )
        ).forEach(record -> list.add(record));
        if (list.size() == 0)
            return 0L;
        else
            return list.get(0).getId();
    }

    public Boolean isUsernameAvailable(String username){
        Long id = getLoginDetailsIdByUsername(username);
        if (id == 0L)
            return true;
        else
            return false;
    }

    public Long getCustomerIdByUsername(String username){
        List<Customer> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT a.id as cid FROM customer a INNER JOIN customer_login b INNER JOIN cust_login_map c ON a.id=c.cust_id AND b.id=c.login_id WHERE b.cust_username = ?", new Object[] { username },
                (rs, rowNum) -> new Customer(
                        rs.getLong("cid")
                )
        ).forEach(record -> list.add(record));
        if (list.size() == 0)
            return 0L;
        else
            return list.get(0).getId();
    }

    public Long insertCustomerLoginData(String cust_username,String cust_password,Boolean cust_status){
        Long id = 0L;
        try{
            if(!isUsernameAvailable(cust_username))
                throw new AppException("Username not available",HttpStatus.CONFLICT);
            jdbcTemplate.update(
                    "INSERT INTO customer_login(cust_username,cust_password,cust_status) VALUES (?,?,?)",
                    new Object[] {
                            cust_username,
                            cust_password,
                            cust_status
                    });
            id = getLoginDetailsIdByUsername(cust_username);
        }
        catch (Exception e){
            throw new AppException("Customer Login Data Insertion Failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return id;
    }

    public void insertCustomerLoginMap(Long cust_id, Long login_id){
        try{
            jdbcTemplate.update(
                    "INSERT INTO cust_login_map(cust_id,login_id) VALUES (?,?)",
                    new Object[] {
                            cust_id,
                            login_id
                    });
        }
        catch (Exception e){
            throw new AppException("Customer Login Data Mapping Failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public Roles getRoleByID(Long id){
        List<Roles> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM roles WHERE id = ?", new Object[] { id },
                (rs, rowNum) -> new Roles(
                        rs.getLong("id"),
                        rs.getString("role")
                )
        ).forEach(record -> list.add(record));
        if (list.size() == 0)
            return null;
        else
            return list.get(0);
    }

    public Long getRoleIDByRoleName(String role){
        List<Roles> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM roles WHERE role = ?", new Object[] { role },
                (rs, rowNum) -> new Roles(
                        rs.getLong("id"),
                        rs.getString("role")
                )
        ).forEach(record -> list.add(record));
        if (list.size() == 0)
            return 0L;
        else
            return list.get(0).getId();
    }

    public List<Roles> getAllRoles(){
        List<Roles> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT * FROM roles", new Object[] {  },
                (rs, rowNum) -> new Roles(
                        rs.getLong("id"),
                        rs.getString("role")
                )
        ).forEach(record -> list.add(record));
        return list;
    }

    public Long insertRole(String role){
        Long id = 0L;
        try{
            id = getRoleIDByRoleName(role);
            if(id != 0L)
                throw new AppException("Role already present",HttpStatus.CONFLICT);
            jdbcTemplate.update(
                    "INSERT INTO roles(role) VALUES (?)",
                    new Object[] {
                            role
                    });
            id = getRoleIDByRoleName(role);
        }
        catch (Exception e){
            throw new AppException("Role Insertion Failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return id;
    }

    public List<Roles> getCustomerRolesByCustomerID(Long id){
        List<Roles> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT b.id as rid,b.role as authority FROM cust_role_map a INNER JOIN roles b ON a.role_id=b.id WHERE a.cust_id = ?", new Object[] { id },
                (rs, rowNum) -> new Roles(
                        rs.getLong("rid"),
                        rs.getString("authority")
                )
        ).forEach(auth -> list.add(auth));
        return list;
    }

    public List<Roles> getCustomerRolesByUsername(String username){
        Long cust_id = getCustomerIdByUsername(username);
        if(cust_id == 0L)
            throw new AppException("Could not identify Customer by Username",HttpStatus.NOT_FOUND);
        return getCustomerRolesByCustomerID(cust_id);
    }

    public void insertCustomerRole(Long cust_id,Long role_id){
        Customer c = getCustomerByID(cust_id);
        if(c == null)
            throw new AppException("Customer not found",HttpStatus.NOT_FOUND);
        Roles r = getRoleByID(role_id);
        if(r == null)
            throw new AppException("Role not found",HttpStatus.NOT_FOUND);
        List<Roles> roles = getCustomerRolesByCustomerID(cust_id);
        if(roles.contains(r))
            throw new AppException("Role is already assigned",HttpStatus.CONFLICT);
        try{
            jdbcTemplate.update(
                    "INSERT INTO cust_role_map(cust_id,role_id) VALUES (?,?)",
                    new Object[] {
                            cust_id,
                            role_id
                    });
        }
        catch (Exception e){
            throw new AppException("Customer Role Mapping Insertion Failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public Long registerCustomer(String first_name,String last_name,String email,String contact_cc,String contact_cn,
                                 String image,String cust_username,String cust_password,Boolean cust_status){
        Long cust_id,login_id;
        try{
            cust_id = insertCustomer(first_name,last_name,email,contact_cc,contact_cn,image);
            login_id = insertCustomerLoginData(cust_username,cust_password,cust_status);
            insertCustomerLoginMap(cust_id,login_id);
        }
        catch (Exception e){
            throw new AppException("Customer Registration Failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return cust_id;
    }

    public Long registerCustomerWithRoles(String first_name,String last_name,String email,String contact_cc,
                                          String contact_cn,String image,String cust_username,String cust_password,
                                          Boolean cust_status,List<Roles> roles){
        Long cust_id,login_id;
        try{
            cust_id = insertCustomer(first_name,last_name,email,contact_cc,contact_cn,image);
            login_id = insertCustomerLoginData(cust_username,cust_password,cust_status);
            insertCustomerLoginMap(cust_id,login_id);
            for(Roles r : roles)
                insertCustomerRole(cust_id,r.getId());
        }
        catch (Exception e){
            throw new AppException("Customer Registration Failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return cust_id;
    }

    public Long insertCustomer(Customer c){
        return insertCustomer(c.getFirst_name(),
                c.getLast_name(),
                c.getEmail(),
                c.getContact_cc(),
                c.getContact_cn(),
                c.getImage()
        );
    }

    public Long insertCustomerLoginData(Customer_login cl){
        return insertCustomerLoginData(cl.getCust_username(),cl.getCust_password(),cl.isCust_status());
    }

    public Long registerCustomer(Customer c,Customer_login cl){
        Long cust_id,login_id;
        try{
            cust_id = insertCustomer(c);
            login_id = insertCustomerLoginData(cl);
            insertCustomerLoginMap(cust_id,login_id);
        }
        catch (Exception e){
            throw new AppException("Customer Registration Failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return cust_id;
    }

    public Long registerCustomerWithRoles(Customer c,Customer_login cl,List<Roles> roles){
        Long cust_id,login_id;
        try{
            cust_id = insertCustomer(c);
            login_id = insertCustomerLoginData(cl);
            insertCustomerLoginMap(cust_id,login_id);
            for(Roles r : roles)
                insertCustomerRole(cust_id,r.getId());
        }
        catch (Exception e){
            throw new AppException("Customer Registration Failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return cust_id;
    }
}