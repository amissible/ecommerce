package com.amissible.authenticator.dao;

import com.amissible.beans.model.Customer;
import com.amissible.beans.model.Customer_login;
import com.amissible.beans.model.Roles;

import java.util.List;

public interface DatabaseDao {
    Customer getCustomerByID(Long id);
    Customer getCustomerByEmail(String email);
    Long getCustomerIdByEmail(String email);
    Long insertCustomer(String first_name,String last_name,String email,String contact_cc,String contact_cn,String image);
    public boolean deleteCustomer(long id);
    Customer_login getLoginDetailsByID(Long id);
    Customer_login getLoginDetailsByUsername(String username);
    Long getLoginDetailsIdByUsername(String username);
    Boolean isUsernameAvailable(String username);
    Long getCustomerIdByUsername(String username);
    Long insertCustomerLoginData(String cust_username,String cust_password,Boolean cust_status);
    void insertCustomerLoginMap(Long cust_id, Long login_id);
    Roles getRoleByID(Long id);
    Long getRoleIDByRoleName(String role);
    List<Roles> getAllRoles();
    Long insertRole(String role);
    List<Roles> getCustomerRolesByCustomerID(Long id);
    List<Roles> getCustomerRolesByUsername(String username);
    void insertCustomerRole(Long cust_id,Long role_id);
    Long registerCustomer(String first_name,String last_name,String email,String contact_cc,String contact_cn,
                          String image,String cust_username,String cust_password,Boolean cust_status);
    Long registerCustomerWithRoles(String first_name,String last_name,String email,String contact_cc,
                                   String contact_cn,String image,String cust_username,String cust_password,
                                   Boolean cust_status,List<Roles> roles);
    Long insertCustomer(Customer c);
    Long insertCustomerLoginData(Customer_login cl);
    Long registerCustomer(Customer c,Customer_login cl);
    Long registerCustomerWithRoles(Customer c,Customer_login cl,List<Roles> roles);
}