package com.amissible.authenticator.exceptions;

import org.springframework.security.core.AuthenticationException;

/**
 * This exception is thrown in case of a not activated user trying to authenticate.
 */
public class UserNotActivatedException extends AuthenticationException {

   public UserNotActivatedException(String message) {
      super(message);
   }
}
