package com.amissible.authenticator.models;

public class CustomerDetails {
    //Customer_login
    String username;
    String password;
    boolean cust_status;

    //Customer
    String first_name;
    String last_name;
    String email;
    String contact_cc;
    String contact_cn;
    String image;

    //Default constructor
    public CustomerDetails() {
    }

    //parameterized without image
    public CustomerDetails(String username, String password, String first_name, String last_name, String email,
                           String contact_cc, String contact_cn) {
        this.username = username;
        this.password = password;
        this.cust_status = true;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.contact_cc = contact_cc;
        this.contact_cn = contact_cn;
        this.image = null;
    }

    //parameterized with image
    public CustomerDetails(String cust_username, String cust_password, String first_name, String last_name, String email,
                           String contact_cc, String contact_cn, String image) {
        this.username = cust_username;
        this.password = cust_password;
        this.cust_status = true;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.contact_cc = contact_cc;
        this.contact_cn = contact_cn;
        this.image = image;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Boolean getCust_status() { return cust_status; }

    public void setCust_status(boolean cust_status) {
        this.cust_status = cust_status;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getEmail() {
        return email;
    }

    public String getContact_cc() {
        return contact_cc;
    }

    public String getContact_cn() {
        return contact_cn;
    }

    public String getImage() {

        if(image == null) return "null";
        else return image;
    }

    @Override
    public String toString() {
        return "CustomerDetails{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", cust_status=" + cust_status +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", email='" + email + '\'' +
                ", contact_cc='" + contact_cc + '\'' +
                ", contact_cn='" + contact_cn + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
