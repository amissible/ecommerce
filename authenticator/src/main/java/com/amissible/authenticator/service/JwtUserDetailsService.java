package com.amissible.authenticator.service;

import com.amissible.authenticator.dao.DatabaseDao;
import com.amissible.authenticator.dao.DatabaseDaoImpl;
import com.amissible.authenticator.exceptions.UserNotActivatedException;
import com.amissible.beans.model.Customer_login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

//This implementation currently includes username as discussed. Can be modified for email verification
//Provide an interface for DatabaseDao, and a separate interface for strings

@Service
public class JwtUserDetailsService implements UserDetailsService {

    Customer_login customerLogin = null;


    @Autowired
    DatabaseDao databaseDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        customerLogin = databaseDao.getLoginDetailsByUsername(username);
        if (customerLogin == null) {
            System.out.println("null");
            throw new UsernameNotFoundException("No user with username " + username +
                    " exists");
        } else if (!customerLogin.isCust_status()) {
            System.out.println("deactivated");
            throw new UserNotActivatedException("User " + username.toLowerCase() +
                    " was not activated!");

        } else {
            List<GrantedAuthority> grantedAuthorityList = databaseDao.getCustomerRolesByUsername(username)
                    .stream().map(authority -> new SimpleGrantedAuthority(authority.getRole()))
                    .collect(Collectors.toList());
            UserDetails user = new User(customerLogin.getCust_username(), customerLogin.getCust_password(),
                    grantedAuthorityList);
            System.out.println("User found!");
            return user;
        }
    }
}