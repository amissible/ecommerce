package com.amissible.authenticator.rest;

import com.amissible.authenticator.dao.DatabaseDao;
import com.amissible.authenticator.models.User;
import com.amissible.authenticator.service.JwtUserDetailsService;
import com.amissible.beans.model.Customer_login;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/user-service")
public class UserController {
    @Autowired
    JwtUserDetailsService userDetailsService;

    private final PasswordEncoder encoder = new BCryptPasswordEncoder();

    @Autowired
    PasswordEncoder encoder2;

    @GetMapping("/get-user-details")
    User getDetails(@RequestParam String username) {
        UserDetails springUser = userDetailsService.loadUserByUsername(username);
        List<String> authList = new ArrayList<>();
        for(GrantedAuthority auth : springUser.getAuthorities()) {
            authList.add(auth.getAuthority());
        }
        return new User(springUser.getUsername(), springUser.getPassword(), authList);
    }
}
