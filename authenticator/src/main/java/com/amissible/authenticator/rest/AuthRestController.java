package com.amissible.authenticator.rest;

import com.amissible.authenticator.dao.DatabaseDaoImpl;
import com.amissible.authenticator.models.CustomerDetails;
import com.amissible.authenticator.models.JwtToken;
import com.amissible.authenticator.models.User;
import com.amissible.beans.model.Customer;
import com.amissible.beans.model.Customer_login;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

@RestController
@RequestMapping("/amissible")
public class AuthRestController {
    @Autowired
    private final AuthenticationManagerBuilder managerBuilder;
    @Autowired
    DatabaseDaoImpl databaseDao;

    public AuthRestController(AuthenticationManagerBuilder managerBuilder) {
        this.managerBuilder = managerBuilder;
    }

    @GetMapping("/status")
    String getStatus() {
        return "<center><h2>Authenticator service is up.</h2></center>" + databaseDao.getTableRowMap();
    }

    //for testing
    @PostMapping("/createdummyusers")
    String dummyData() {

        long id1 = (databaseDao.insertCustomer("Johnny","Gupta","JG@abc.xyz","+91","80854","https://bit.ly/3f8UIyp"));
        long id2 = (databaseDao.insertCustomer(new Customer("Dexter","Pandey","DP@abc.xyz","+91","56852","https://bit.ly/2UAyXxQ")));
        databaseDao.insertCustomerLoginMap(id1, id1);
        databaseDao.insertCustomerLoginMap(id2, id2);
        databaseDao.insertCustomerLoginData("John", "$2a$10$oi9GDv4UiprzEgQ6k7gttOZKEFWKIJ8YA0RMTGK/nWizhrLGaE2Ty", true);
        databaseDao.insertCustomerLoginData("Dexter", "$2a$10$oi9GDv4UiprzEgQ6k7gttOZKEFWKIJ8YA0RMTGK/nWizhrLGaE2Ty", true);
        databaseDao.insertCustomerRole(4l, 1l);
        databaseDao.insertCustomerRole(4l, 2l);
        databaseDao.insertCustomerRole(5l, 1l);
        return "Users added: " + databaseDao.getLoginDetailsByUsername("John").toString() + " and "
                 + databaseDao.getLoginDetailsByUsername("Dexter").toString();
    }

    @Transactional
    @PostMapping(value = "/signup")
    boolean createUser(@RequestBody CustomerDetails customerDetails) {
        customerDetails.setCust_status(true);
        Customer customer = new Customer(customerDetails.getFirst_name(),
                customerDetails.getLast_name(), customerDetails.getEmail(),
                customerDetails.getContact_cc(), customerDetails.getContact_cn(),
                customerDetails.getImage());
        //Bcrypt Password encoder
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        Customer_login customerLogin = new Customer_login(customerDetails.getUsername(),
                encoder.encode(customerDetails.getPassword()), customerDetails.getCust_status());
        long id1 = databaseDao.insertCustomer(customer);
        long id2;
        try {
            id2 = databaseDao.insertCustomerLoginData(customerLogin);
        } catch (Exception e) {
            System.out.println("Customer insertion unsuccessful. Deleting customer with id: " + id1);
            databaseDao.deleteCustomer(id1);
            throw e;
        }
        databaseDao.insertCustomerLoginMap(id1, id2);
        //Default privilege: USER
        databaseDao.insertCustomerRole(id1, 1l);
        return true;
    }

    //deletion
    /*@PostMapping("/delete_customer")
    public boolean delete(@RequestParam String email) {
        long id = databaseDao.getCustomerIdByEmail(email);
        databaseDao.deleteCustomer(id);
        return true;
    }*/

    public String adminService() {
        return "Welcome admin";
    }
}
