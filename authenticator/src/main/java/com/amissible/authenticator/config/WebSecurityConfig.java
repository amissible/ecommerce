package com.amissible.authenticator.config;

import com.amissible.authenticator.exceptions.JwtAccessDeniedHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.CorsFilter;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

   private final CorsFilter corsFilter;
   private final JwtAccessDeniedHandler jwtAccessDeniedHandler;

   public WebSecurityConfig(
           CorsFilter corsFilter,
           JwtAccessDeniedHandler jwtAccessDeniedHandler
   ) {
      this.corsFilter = corsFilter;
      this.jwtAccessDeniedHandler = jwtAccessDeniedHandler;
   }

   @Bean
   public PasswordEncoder passwordEncoder() {
      return new BCryptPasswordEncoder();
   }

   // configure requests that should be ignored by Spring Security
   @Override
   public void configure(WebSecurity web) {
      web.ignoring()
              .antMatchers(HttpMethod.OPTIONS, "/**")

              // allow anonymous resource requests
              .antMatchers(
                      "/",
                      "/*.html",
                      "/favicon.ico",
                      "/**/*.html",
                      "/**/*.css",
                      "/**/*.js",
                      "/h2-console/**"
              );
   }

   //yet to test
   //@Bean
   /*SecurityWebFilterChain securityFilterChain(ServerHttpSecurity http) {
      http
              // ...
              .redirectToHttps();
      return http.build();
   }*/

   //configure security settings
   @Override
   protected void configure(HttpSecurity httpSecurity) throws Exception {
      httpSecurity
              // we don't need CSRF because our token is invulnerable
              .csrf().disable()

              .addFilterBefore(corsFilter, UsernamePasswordAuthenticationFilter.class)

              .exceptionHandling()
              //      .authenticationEntryPoint(authenticationErrorHandler)
              .accessDeniedHandler(jwtAccessDeniedHandler)

              // enable h2-console
              .and()
              .headers()
              .frameOptions()
              .sameOrigin()

              // create no session
              .and()
              .sessionManagement()
              .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

              .and()
              .authorizeRequests()
              .antMatchers("/amissible/authenticate").permitAll()
              // .antMatchers("/amissible/createdummyusers").permitAll() //test user creation mechanics
              .antMatchers("/amissible/signup").permitAll()
              .antMatchers("/amissible/delete_customer").permitAll()
              .antMatchers("/amissible/resolve_token").permitAll()
              .antMatchers("/user-service/**").permitAll()
              .antMatchers("/amissible/status").permitAll()
              // .antMatchers("/amissible/register").permitAll()
              // .antMatchers("/amissible/activate").permitAll()
              // .antMatchers("/amissible/account/reset-password/init").permitAll()
              // .antMatchers("/amissible/account/reset-password/finish").permitAll()

              .antMatchers("/amissible/person").hasAuthority("USER")
              .antMatchers("/amissible/hiddenmessage").hasAuthority("ADMIN")

              .anyRequest().authenticated();
   }
}
