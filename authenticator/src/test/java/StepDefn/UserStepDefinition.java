package StepDefn;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class UserStepDefinition {

    WebDriver driver;
    RestTemplate restTemplate = new RestTemplate();

    @Given("the module is up")
    public void userLocation() {
        //
    }

    @When("the url is http:\\\\/\\\\/localhost:{int}\\\\/amissible\\\\/status and response is {int}")
    public void attemptLogin(Integer int1, Integer int2) {
        ResponseEntity<String> res = restTemplate.getForEntity("http://localhost:8080/amissible/status", String.class);
        Assert.assertEquals(HttpStatus.OK, res.getStatusCode());
    }

    @Then("display returned response")
    public void display() {

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Sparkie\\Downloads\\chromedriver1.exe");
        driver = new ChromeDriver();
        driver.get("http://localhost:8080/amissible/status");
    }
}