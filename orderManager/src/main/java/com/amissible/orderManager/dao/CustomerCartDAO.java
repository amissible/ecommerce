package com.amissible.orderManager.dao;

import com.amissible.beans.exception.AppException;
import com.amissible.beans.model.Customer_cart;
import com.amissible.beans.model.Product;
import com.amissible.orderManager.dao_interface.CustomerCartInterface;
import com.amissible.orderManager.model.Cart;
import com.amissible.orderManager.model.CartItems;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerCartDAO implements CustomerCartInterface {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<CartItems> getCartItems(Long user_id, String currency){
        List<CartItems> items = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT a.quantity,b.product_name,b.alias,b.brand,b.icon1_path,c.marked_price,c.sale_price FROM customer_cart a INNER JOIN product b INNER JOIN product_price c ON a.product_id=b.id AND a.product_id=c.product_id WHERE a.user_id = ? AND c.currency = ?", new Object[] { user_id, currency },
                (rs, rowNum) -> new CartItems(
                        rs.getString("product_name"),
                        rs.getString("alias"),
                        rs.getString("brand"),
                        rs.getString("icon1_path"),
                        rs.getFloat("marked_price"),
                        rs.getFloat("sale_price"),
                        rs.getInt("quantity")
                )
        ).forEach(record -> items.add(record));
        return items;
    }

    public Customer_cart getRecordQuantity(Long user_id,Long product_id){
        List<Customer_cart> list = new ArrayList<>();
        jdbcTemplate.query(
                "SELECT quantity FROM customer_cart WHERE user_id = ? AND product_id = ?", new Object[] { user_id, product_id },
                (rs, rowNum) -> new Customer_cart(
                        rs.getInt("quantity")
                )
        ).forEach(record -> list.add(record));
        if(list.size() == 0)
            return null;
        else if(list.size() > 1)
            throw new AppException("Duplicate record in Cart", HttpStatus.CONFLICT);
        else
            return list.get(0);
    }

    public void insertRecord(Long user_id,Long product_id,Integer quantity){
        try{
            jdbcTemplate.update(
                    "INSERT INTO customer_cart(user_id,product_id,quantity) VALUES(?,?,?)",
                    new Object[] { user_id, product_id, quantity });
        }
        catch (Exception e){
            throw new AppException("Failed to add Product in Cart", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public void updateRecord(Long user_id,Long product_id,Integer quantity){
        try{
            jdbcTemplate.update(
                    "UPDATE customer_cart SET quantity = ? WHERE user_id = ? AND product_id = ?",
                    new Object[] { quantity, user_id, product_id });
        }
        catch (Exception e){
            throw new AppException("Failed to update Product in Cart", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public void removeRecord(Long user_id,Long product_id){
        try{
            jdbcTemplate.update(
                    "DELETE FROM customer_cart WHERE user_id = ? AND product_id = ?",
                    new Object[] { user_id, product_id });
        }
        catch (Exception e){
            throw new AppException("Failed to remove Product in Cart", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public void flushCart(Long user_id){
        try{
            jdbcTemplate.update(
                    "DELETE FROM customer_cart WHERE user_id = ?",
                    new Object[] { user_id });
        }
        catch (Exception e){
            throw new AppException("Failed to clean Cart", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
