package com.amissible.orderManager.model;

import java.util.List;

public class Cart {
    private Float total;
    private Float savings;
    private Integer average_discount;
    private Integer quantity;
    private Integer count;
    private List<CartItems> items;

    public Cart() {
    }

    public Cart(List<CartItems> items) {
        this.items = items;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    public Float getSavings() {
        return savings;
    }

    public void setSavings(Float savings) {
        this.savings = savings;
    }

    public Integer getAverage_discount() {
        return average_discount;
    }

    public void setAverage_discount(Integer average_discount) {
        this.average_discount = average_discount;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<CartItems> getItems() {
        return items;
    }

    public void setItems(List<CartItems> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "Cart{" +
                "total=" + total +
                ", savings=" + savings +
                ", average_discount=" + average_discount +
                ", quantity=" + quantity +
                ", count=" + count +
                ", items=" + items +
                '}';
    }

    public void evaluate(){
        count = items.size();
        quantity = 0;
        Float total_marked_price=0.0F,total_sale_price=0.0F;
        for(int i=0;i<count;i++){
            CartItems item = items.get(i);
            Float sp = item.getSale_price();
            Float mp = item.getMarked_price();
            Integer q = item.getQuantity();
            item.setDiscount((int)Math.round(100*(1-sp/mp)));
            item.setSavings(Math.round((mp-sp)*q*100)/100.0F);
            item.setTotal(sp*q);
            total_marked_price += mp * q;
            total_sale_price += sp * q;
            quantity += q;
        }
        total = total_sale_price;
        savings = Math.round((total_marked_price - total_sale_price)*100)/100.0F;
        average_discount = (int)(100*(1-total_sale_price/total_marked_price));
    }
}
