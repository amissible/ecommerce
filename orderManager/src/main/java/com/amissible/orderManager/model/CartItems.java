package com.amissible.orderManager.model;

public class CartItems {
    private String product_name;
    private String alias;
    private String brand;
    private String icon;
    private Float marked_price;
    private Float sale_price;
    private Integer discount;
    private Integer quantity;
    private Float savings;
    private Float total;

    public CartItems() {
    }

    public CartItems(String product_name, String alias, String brand, String icon, Float marked_price, Float sale_price, Integer quantity) {
        this.product_name = product_name;
        this.alias = alias;
        this.brand = brand;
        this.icon = icon;
        this.marked_price = marked_price;
        this.sale_price = sale_price;
        this.quantity = quantity;
    }

    public CartItems(String product_name, String alias, String brand, String icon, Float marked_price, Float sale_price, Integer discount, Integer quantity, Float savings, Float total) {
        this.product_name = product_name;
        this.alias = alias;
        this.brand = brand;
        this.icon = icon;
        this.marked_price = marked_price;
        this.sale_price = sale_price;
        this.discount = discount;
        this.quantity = quantity;
        this.savings = savings;
        this.total = total;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Float getMarked_price() {
        return marked_price;
    }

    public void setMarked_price(Float marked_price) {
        this.marked_price = marked_price;
    }

    public Float getSale_price() {
        return sale_price;
    }

    public void setSale_price(Float sale_price) {
        this.sale_price = sale_price;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    public Float getSavings() {
        return savings;
    }

    public void setSavings(Float savings) {
        this.savings = savings;
    }

    @Override
    public String toString() {
        return "CartItems{" +
                "product_name='" + product_name + '\'' +
                ", alias='" + alias + '\'' +
                ", brand='" + brand + '\'' +
                ", icon='" + icon + '\'' +
                ", marked_price=" + marked_price +
                ", sale_price=" + sale_price +
                ", discount=" + discount +
                ", quantity=" + quantity +
                ", savings=" + savings +
                ", total=" + total +
                '}';
    }
}
