package com.amissible.orderManager.controller;

import com.amissible.orderManager.model.Cart;
import com.amissible.orderManager.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@CrossOrigin
@RequestMapping("/cart")
public class CartController {

    private String testMessage = "<h3> Order Manager is running </h3>";

    @Autowired
    CartService cartService;

    @RequestMapping("/test-service")
    public String response(HttpServletRequest request) {
        return "<center>" + testMessage + ", on address: " + request.getLocalAddr() + "</center>";
    }

    @RequestMapping(value = "/addItem")
    @ResponseBody
    public void pushItemInCart(@RequestParam Long user_id,@RequestParam String alias){
        cartService.pushItemInCart(user_id,alias);
    }

    @RequestMapping(value = "/addItems")
    @ResponseBody
    public void pushItemInCart(@RequestParam Long user_id,@RequestParam String alias,@RequestParam Integer quantity){
        cartService.pushItemInCart(user_id, alias, quantity);
    }

    @RequestMapping(value = "/setItems")
    @ResponseBody
    public void putItemInCart(@RequestParam Long user_id,@RequestParam String alias,@RequestParam Integer quantity){
        cartService.putItemInCart(user_id, alias, quantity);
    }

    @RequestMapping(value = "/reduceItemCount")
    @ResponseBody
    public void popItemFromCart(@RequestParam Long user_id,@RequestParam String alias){
        cartService.popItemFromCart(user_id, alias);
    }

    @RequestMapping(value = "/removeItem")
    @ResponseBody
    public void pullItemFromCart(@RequestParam Long user_id,@RequestParam String alias){
        cartService.pullItemFromCart(user_id, alias);
    }

    @RequestMapping(value = "/removeAll")
    @ResponseBody
    public void flushCart(@RequestParam Long user_id){
        cartService.flushCart(user_id);
    }

    @RequestMapping(value = "/getCartData")
    @ResponseBody
    public Cart getCartData(@RequestParam Long user_id, @RequestParam String currency){
        return cartService.getCartData(user_id, currency);
    }

}
