package com.amissible.orderManager.service;

import com.amissible.beans.configuration.MODULE_URL;
import com.amissible.beans.exception.AppException;
import com.amissible.beans.model.Customer_cart;
import com.amissible.orderManager.dao.CustomerCartDAO;
import com.amissible.orderManager.model.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class CartService {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    CustomerCartDAO customerCartDAO;

    public void pushItemInCart(Long user_id, String alias){
        pushItemInCart(user_id,alias,1);
    }

    public void pushItemInCart(Long user_id,String alias,Integer quantity){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity <Long> entity = new HttpEntity<Long>(headers);
        Long product_id = restTemplate.exchange(MODULE_URL.CATALOG_BASE_URL+"/db/functions/getProductIdByAlias/"+alias, HttpMethod.GET, entity, Long.class).getBody();
        //Also Verify if User ID is correct or not
        if(product_id == 0L)
            throw new AppException("Product Not Found",HttpStatus.NOT_FOUND);
        Customer_cart record = customerCartDAO.getRecordQuantity(user_id,product_id);
        if(record == null)
            customerCartDAO.insertRecord(user_id,product_id,quantity);
        else
            customerCartDAO.updateRecord(user_id,product_id,record.getQuantity()+quantity);
    }

    public void putItemInCart(Long user_id,String alias,Integer quantity){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity <Long> entity = new HttpEntity<Long>(headers);
        Long product_id = restTemplate.exchange(MODULE_URL.CATALOG_BASE_URL+"/db/functions/getProductIdByAlias/"+alias, HttpMethod.GET, entity, Long.class).getBody();
        //Also Verify if User ID is correct or not
        if(product_id == 0L)
            throw new AppException("Product Not Found",HttpStatus.NOT_FOUND);
        Customer_cart record = customerCartDAO.getRecordQuantity(user_id,product_id);
        if(record == null)
            customerCartDAO.insertRecord(user_id,product_id,quantity);
        else
            customerCartDAO.updateRecord(user_id,product_id,quantity);
    }

    public void popItemFromCart(Long user_id,String alias){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity <Long> entity = new HttpEntity<Long>(headers);
        Long product_id = restTemplate.exchange(MODULE_URL.CATALOG_BASE_URL+"/db/functions/getProductIdByAlias/"+alias, HttpMethod.GET, entity, Long.class).getBody();
        //Also Verify if User ID is correct or not
        if(product_id == 0L)
            throw new AppException("Product Not Found",HttpStatus.NOT_FOUND);
        Customer_cart record = customerCartDAO.getRecordQuantity(user_id,product_id);
        if(record == null)
            throw new AppException("Product not present in Cart",HttpStatus.NOT_FOUND);
        else if(record.getQuantity() == 1)
            customerCartDAO.removeRecord(user_id,product_id);
        else
            customerCartDAO.updateRecord(user_id,product_id,record.getQuantity()-1);
    }

    public void pullItemFromCart(Long user_id,String alias){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity <Long> entity = new HttpEntity<Long>(headers);
        Long product_id = restTemplate.exchange(MODULE_URL.CATALOG_BASE_URL+"/db/functions/getProductIdByAlias/"+alias, HttpMethod.GET, entity, Long.class).getBody();
        //Also Verify if User ID is correct or not
        if(product_id == 0L)
            throw new AppException("Product Not Found",HttpStatus.NOT_FOUND);
        Customer_cart record = customerCartDAO.getRecordQuantity(user_id,product_id);
        if(record == null)
            throw new AppException("Product not present in Cart",HttpStatus.NOT_FOUND);
        else
            customerCartDAO.removeRecord(user_id,product_id);
    }

    public void flushCart(Long user_id){
        //Also Verify if User ID is correct or not
        customerCartDAO.flushCart(user_id);
    }

    public Cart getCartData(Long user_id, String currency){
        Cart cart = new Cart();
        cart.setItems(customerCartDAO.getCartItems(user_id, currency));
        cart.evaluate();
        return cart;
    }
}
