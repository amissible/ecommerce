package com.amissible.beans.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String category_name;
    private Long parent_id;
    private Integer sort_order;

    public Category() {
    }

    public Category(Long id, Long parent_id) {
        this.id = id;
        this.parent_id = parent_id;
    }

    public Category(Long id, Long parent_id, Integer sort_order) {
        this.id = id;
        this.parent_id = parent_id;
        this.sort_order = sort_order;
    }

    public Category(String category_name, Long parent_id, Integer sort_order) {
        this.category_name = category_name;
        this.parent_id = parent_id;
        this.sort_order = sort_order;
    }

    public Category(Long id, String category_name, Integer sort_order) {
        this.id = id;
        this.category_name = category_name;
        this.sort_order = sort_order;
    }

    public Category(Long id, String category_name, Long parent_id, Integer sort_order) {
        this.id = id;
        this.category_name = category_name;
        this.parent_id = parent_id;
        this.sort_order = sort_order;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public Long getParent_id() {
        return parent_id;
    }

    public void setParent_id(Long parent_id) {
        this.parent_id = parent_id;
    }

    public Integer getSort_order() {
        return sort_order;
    }

    public void setSort_order(Integer sort_order) {
        this.sort_order = sort_order;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", category_name='" + category_name + '\'' +
                ", parent_id=" + parent_id +
                ", sort_order=" + sort_order +
                '}';
    }
}
