package com.amissible.beans.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.List;

@Entity
public class Menu {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String item_name;
    private String icon;
    private String route;
    private Long parent_id;
    private Integer sort_order;
    private List<Menu> submenu;

    public Menu() {
    }

    public Menu(String item_name, String icon, String route, Long parent_id, Integer sort_order) {
        this.item_name = item_name;
        this.icon = icon;
        this.route = route;
        this.parent_id = parent_id;
        this.sort_order = sort_order;
    }

    public Menu(Long id, String item_name, String icon, String route, Long parent_id, Integer sort_order) {
        this.id = id;
        this.item_name = item_name;
        this.icon = icon;
        this.route = route;
        this.parent_id = parent_id;
        this.sort_order = sort_order;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public Long getParent_id() {
        return parent_id;
    }

    public void setParent_id(Long parent_id) {
        this.parent_id = parent_id;
    }

    public Integer getSort_order() {
        return sort_order;
    }

    public void setSort_order(Integer sort_order) {
        this.sort_order = sort_order;
    }

    public List<Menu> getSubmenu() {
        return submenu;
    }

    public void setSubmenu(List<Menu> submenu) {
        this.submenu = submenu;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "id=" + id +
                ", item_name='" + item_name + '\'' +
                ", icon='" + icon + '\'' +
                ", route='" + route + '\'' +
                ", parent_id=" + parent_id +
                ", sort_order=" + sort_order +
                ", submenu=" + submenu +
                '}';
    }
}
