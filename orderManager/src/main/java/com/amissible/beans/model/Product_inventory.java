package com.amissible.beans.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Product_inventory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long product_id;
    private Long source_id;
    private Integer quantity;

    public Product_inventory() {
    }

    public Product_inventory(Long id, Long product_id, Long source_id, Integer quantity) {
        this.id = id;
        this.product_id = product_id;
        this.source_id = source_id;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    public Long getSource_id() {
        return source_id;
    }

    public void setSource_id(Long source_id) {
        this.source_id = source_id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Product_inventory{" +
                "id=" + id +
                ", product_id=" + product_id +
                ", source_id=" + source_id +
                ", quantity=" + quantity +
                '}';
    }
}
