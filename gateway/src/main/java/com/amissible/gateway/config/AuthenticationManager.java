package com.amissible.gateway.config;
import com.amissible.gateway.security.JwtUserDetailsService;
import com.amissible.gateway.security.TokenProvider;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class AuthenticationManager implements ReactiveAuthenticationManager {

	@Autowired
	private TokenProvider tokenProvider;
	@Autowired
	JwtUserDetailsService userDetailsService;
	private final ObjectMapper objectMapper = new ObjectMapper();

	@Override
	public Mono<Authentication> authenticate(Authentication authentication) {
		String authToken = authentication.getCredentials().toString();
		try {
			String username = tokenProvider.getUsernameFromToken(authToken);
			if (!tokenProvider.validateToken(authToken)) {
				return Mono.empty();
			}
			UserDetails user = userDetailsService.loadUserByUsername(username);
			return Mono.just(new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword(), user.getAuthorities()));
		} catch (UsernameNotFoundException usernameNotFoundException) {
			return Mono.empty();
		}
	}
}