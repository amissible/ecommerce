package com.amissible.gateway.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fallback")
public class FallbackController {

    private String message = " is temporarily unavailable";

    private
    @RequestMapping("/catalog")
    ResponseEntity<String> serviceUnavailable() {
        return new ResponseEntity<>("Catalog service" + message, HttpStatus.SERVICE_UNAVAILABLE);
    }
}