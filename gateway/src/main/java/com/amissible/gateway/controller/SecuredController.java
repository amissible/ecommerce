package com.amissible.gateway.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/secured")
public class SecuredController {

    @GetMapping("/test")
    Mono<ResponseEntity<String>> serviceTest(ServerHttpRequest request) {
        return Mono.just(new ResponseEntity("Your Hostname is: " + request.getRemoteAddress().getHostName(), HttpStatus.OK));
    }
}
