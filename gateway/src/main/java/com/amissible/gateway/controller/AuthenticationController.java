package com.amissible.gateway.controller;

import com.amissible.gateway.config.AuthenticationManager;
import com.amissible.gateway.security.JwtUserDetailsService;
import com.amissible.gateway.security.TokenProvider;
import com.amissible.gateway.model.User;
import com.amissible.gateway.model.JwtToken;
import com.amissible.gateway.model.CustomerDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/amissible")
public class AuthenticationController {

    @Autowired
    JwtUserDetailsService userDetailsService;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    private TokenProvider tokenProvider;

    @Autowired
    private PasswordEncoder encoder;

    private final WebClient webClient;

    //authenticator service address {need to optimize}
    AuthenticationController(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.baseUrl("http://localhost:8080/amissible").build();
    }

    //working as expected
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody User user) throws Exception {
        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(user.getUsername());
        if (encoder.matches(user.getPassword(), userDetails.getPassword())) {
            final String token = tokenProvider.generateToken(userDetails);
            System.out.println(token);
            return ResponseEntity.ok(new JwtToken(token));
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    //not working properly
    @PostMapping("/signup")
    public Mono<Boolean> signup(@RequestBody CustomerDetails customerDetails) {
        Boolean response = webClient.post()
                .uri("/signup")
                .body(Mono.just(customerDetails), CustomerDetails.class).retrieve().bodyToMono(Boolean.class).block();
        return Mono.just(response);
    }
}
