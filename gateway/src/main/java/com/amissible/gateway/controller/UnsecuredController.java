package com.amissible.gateway.controller;

import com.amissible.gateway.model.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/unsecured")
public class UnsecuredController {

    private final WebClient webClient;

    public UnsecuredController(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.baseUrl("http://localhost:8080/user-service/get-user-details").build();
    }


    @RequestMapping("/test")
    ResponseEntity<String> getStatus(ServerHttpRequest request) {
        return new ResponseEntity<String>("Server is active on " + request.getLocalAddress().getAddress().getHostName(), HttpStatus.OK);
    }

    @RequestMapping("/test-user")
    public Mono<User> getStatus(@RequestParam String username) {
        return this.webClient.get().uri("?username={username}", username)
                .retrieve().bodyToMono(User.class);
    }
}
