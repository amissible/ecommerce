package com.amissible.gateway.security;

import com.amissible.gateway.model.User;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.ArrayList;
import java.util.List;

@Service
public class JwtUserDetailsService  implements UserDetailsService {

    private final WebClient webClient;

    public JwtUserDetailsService(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.baseUrl("http://localhost:8080/user-service/get-user-details").build();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            User user = this.webClient.get().uri("?username={username}", username)
                    .retrieve().bodyToMono(User.class).block();
            List<SimpleGrantedAuthority> authorityList = new ArrayList<>();
            for(String itr : user.getAuthorities()) {
                authorityList.add(new SimpleGrantedAuthority(itr));
            }
            UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                    authorityList);
            return userDetails;
        } catch (UsernameNotFoundException e) {
            throw new UsernameNotFoundException("User does not exist");
        }
    }
}
