package StepDefn;

import com.amissible.gateway.model.User;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class GatewayStepDefinition {

    WebDriver driver;
    RestTemplate restTemplate = new RestTemplate();
    String token;

    @Given("the website is up")
    public void userLocation() {
        //
    }

   @When("the url is http:\\/\\/localhost:{int}\\/amissible\\/login and the user attempts to login with username and password")

    public void attemptLogin(Integer int1) {
        User user = new User("MishraSparsh", "Amissible");

        HttpHeaders headers = new HttpHeaders();
       headers.set("X-COM-PERSIST", "true");
       headers.set("X-COM-LOCATION", "India");

       HttpEntity<User> request = new HttpEntity<>(user, headers);

       ResponseEntity<String> res = restTemplate.postForEntity("http://localhost:8087/amissible/login",
                request, String.class);
        token = res.getBody();
        Assert.assertEquals(HttpStatus.OK, res.getStatusCode());
    }

    @Then("on successful login he gets a jwt token")
    public void display() {
        System.out.println(token);
    }
}