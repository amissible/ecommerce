package com.amissible.query;

public interface SampleQueries {
    final String SELECT_ALL = "SELECT * FROM TEST";
    final String SELECT_BY_ID = "SELECT * FROM TEST WHERE ID = ?";
    final String INSERT_TEST = "INSERT INTO TEST (NAME) values(?)";
}
