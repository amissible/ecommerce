package com.amissible.beans.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Orders {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long user_id;
    private Float amount;
    private Long payment_id;
    private String order_status;
    private String datetimestamp;

    public Orders() {
    }

    public Orders(Long id, Long user_id, Float amount, Long payment_id, String order_status, String datetimestamp) {
        this.id = id;
        this.user_id = user_id;
        this.amount = amount;
        this.payment_id = payment_id;
        this.order_status = order_status;
        this.datetimestamp = datetimestamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public Long getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(Long payment_id) {
        this.payment_id = payment_id;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getDatetimestamp() {
        return datetimestamp;
    }

    public void setDatetimestamp(String datetimestamp) {
        this.datetimestamp = datetimestamp;
    }

    @Override
    public String toString() {
        return "Orders{" +
                "id=" + id +
                ", user_id=" + user_id +
                ", amount=" + amount +
                ", payment_id=" + payment_id +
                ", order_status='" + order_status + '\'' +
                ", datetimestamp='" + datetimestamp + '\'' +
                '}';
    }
}
