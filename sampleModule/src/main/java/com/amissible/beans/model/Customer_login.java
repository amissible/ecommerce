package com.amissible.beans.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Customer_login {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String cust_username;
    private String cust_password;
    private boolean cust_status;

    public Customer_login() {
    }


    public Customer_login(Long id) {
        this.id = id;
    }

    public Customer_login(String cust_username, String cust_password, boolean cust_status) {
        this.cust_username = cust_username;
        this.cust_password = cust_password;
        this.cust_status = cust_status;
    }

    public Customer_login(Long id, String cust_username, String cust_password, boolean cust_status) {
        this.id = id;
        this.cust_username = cust_username;
        this.cust_password = cust_password;
        this.cust_status = cust_status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCust_username() {
        return cust_username;
    }

    public void setCust_username(String cust_username) {
        this.cust_username = cust_username;
    }

    public String getCust_password() {
        return cust_password;
    }

    public void setCust_password(String cust_password) {
        this.cust_password = cust_password;
    }

    public boolean isCust_status() {
        return cust_status;
    }

    public void setCust_status(boolean cust_status) {
        this.cust_status = cust_status;
    }



    @Override
    public String toString() {
        return "Customer_login{" +
                "id=" + id +
                ", cust_username='" + cust_username + '\'' +
                ", cust_password='" + cust_password + '\'' +
                ", cust_status=" + cust_status +
                '}';
    }
}
