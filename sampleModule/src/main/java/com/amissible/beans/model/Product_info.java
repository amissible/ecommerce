package com.amissible.beans.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Product_info {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long product_id;
    private Long info_head;
    private Long info_body;

    public Product_info() {
    }

    public Product_info(Long id, Long product_id, Long info_head, Long info_body) {
        this.id = id;
        this.product_id = product_id;
        this.info_head = info_head;
        this.info_body = info_body;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    public Long getInfo_head() {
        return info_head;
    }

    public void setInfo_head(Long info_head) {
        this.info_head = info_head;
    }

    public Long getInfo_body() {
        return info_body;
    }

    public void setInfo_body(Long info_body) {
        this.info_body = info_body;
    }

    @Override
    public String toString() {
        return "Product_info{" +
                "id=" + id +
                ", product_id=" + product_id +
                ", info_head=" + info_head +
                ", info_body=" + info_body +
                '}';
    }
}
