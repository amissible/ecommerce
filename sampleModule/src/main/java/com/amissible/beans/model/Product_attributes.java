package com.amissible.beans.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Product_attributes {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long product_id;
    private String attribute;
    private String attr_value;

    public Product_attributes() {
    }

    public Product_attributes(Long id, Long product_id, String attribute, String attr_value) {
        this.id = id;
        this.product_id = product_id;
        this.attribute = attribute;
        this.attr_value = attr_value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getAttr_value() {
        return attr_value;
    }

    public void setAttr_value(String attr_value) {
        this.attr_value = attr_value;
    }

    @Override
    public String toString() {
        return "Product_attributes{" +
                "id=" + id +
                ", product_id=" + product_id +
                ", attribute='" + attribute + '\'' +
                ", attr_value='" + attr_value + '\'' +
                '}';
    }
}
