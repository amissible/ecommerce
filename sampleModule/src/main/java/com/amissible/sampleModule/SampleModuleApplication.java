package com.amissible.sampleModule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.amissible.*")
public class SampleModuleApplication {

	private static final Logger logger = LoggerFactory.getLogger(SampleModuleApplication.class);


	public static void main(String[] args) {
		SpringApplication.run(SampleModuleApplication.class, args);
	}

}
