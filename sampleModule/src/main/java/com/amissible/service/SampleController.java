package com.amissible.service;

import com.amissible.beans.exception.AppException;
import com.amissible.beans.sample.Test;
import com.amissible.dao.SampleDao;
import com.amissible.dao.SampleDaoImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/sample")
public class SampleController {

    private static final Logger logger = LoggerFactory.getLogger(SampleDaoImpl.class);

    @Autowired
    private SampleDao sampleDao;

    @RequestMapping("/status")
    public @ResponseBody String getStatus(HttpServletRequest request) {
        return "<center>Service is up at "+ request.getLocalAddr() +"</center>";
    }

    @RequestMapping(method = RequestMethod.POST,path = "/getAll")
    public @ResponseBody List<Test> getAllTest(@RequestBody Test test){
        logger.info("getAllTest invoked with parameter Test = {}",test);
        return sampleDao.getAllTest();
    }

    @RequestMapping(method = RequestMethod.POST,path = "/getById")
    public @ResponseBody Test getById(@RequestBody Long id){
        logger.info("getById invoked with parameter Test = {}",id);
        return sampleDao.getById(id);
    }

    @RequestMapping(method = RequestMethod.POST,path = "/insertTest")
    public @ResponseBody Long insertTest(@RequestBody Test test){
        logger.info("getById invoked with parameter Test = {}",test);
        return sampleDao.insertTest(test);
    }

    @RequestMapping(method = RequestMethod.POST,path = "/getAllWithoutParam")
    public @ResponseBody List<Test> getAllTestWithoutParam(){
        logger.info("getAllTest invoked without parameter");
        return sampleDao.getAllTest();
    }

    @RequestMapping(method = RequestMethod.POST,path = "/getException")
    public @ResponseBody List<Test> getException(){
        logger.info("getException invoked without parameter");
        throw new AppException("Testing unauthorized exception", HttpStatus.UNAUTHORIZED);
    }
}
