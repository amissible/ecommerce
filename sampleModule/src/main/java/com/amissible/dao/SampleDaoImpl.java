package com.amissible.dao;

import com.amissible.beans.sample.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;

import static com.amissible.query.SampleQueries.*;

@Service
public class SampleDaoImpl implements SampleDao{
    private static final Logger logger = LoggerFactory.getLogger(SampleDaoImpl.class);
    @Autowired
    private JdbcTemplate template;

    @Override
    public List<Test> getAllTest() {
        logger.debug("Executing getAllTest");
        return template.query(SELECT_ALL,
                new Object[]{},
                (resultSet,i)->{
                Test obj = new Test();
                obj.setId(resultSet.getLong("ID"));
                obj.setTest(resultSet.getString("NAME"));
                return obj;
        });
    }

    @Override
    public Long insertTest(Test test) {
        KeyHolder holder = new GeneratedKeyHolder();
        String x= new String();

        template.update(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection connection)
                    throws SQLException {
                PreparedStatement ps = connection.prepareStatement(INSERT_TEST,
                        Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, test.getTest());
                return ps;
            }
        }, holder);

        Long newTestId = holder.getKey().longValue();
        logger.info("Object={} stored on key = {}",test,newTestId);
        return newTestId;
    }

    @Override
    public Test getById(Long id) {
        logger.debug("Executing getAllTest");
        Optional<Test> test = template.query(SELECT_BY_ID,
                new Object[]{id},
                (resultSet,i)->{
                    Test obj = new Test();
                    obj.setId(resultSet.getLong("ID"));
                    obj.setTest(resultSet.getString("NAME"));
                    return obj;
                }).stream().findFirst();
        if(test.isPresent()){
            return test.get();
        }else{
            return null;
        }
    }
}
