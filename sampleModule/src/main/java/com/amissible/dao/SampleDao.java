package com.amissible.dao;

import com.amissible.beans.sample.Test;

import java.util.List;

public interface SampleDao {
    List<Test> getAllTest();

    Long insertTest(Test test);

    Test getById(Long id);
}
