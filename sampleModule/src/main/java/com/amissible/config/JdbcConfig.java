package com.amissible.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
public class JdbcConfig {
    private static final Logger logger = LoggerFactory.getLogger(JdbcConfig.class);
    @Value( "${jdbc.url}" )
    private String jdbcUrl;
    @Value( "${jdbc.username}" )
    private String username;
    @Value( "${jdbc.password}" )
    private String password;


    @Bean
    public DataSource mysqlDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUrl(jdbcUrl);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        logger.debug("Datasource initialized with parameters url = {}, username = {}, password = {}",jdbcUrl,username,password);
        return dataSource;
    }

}
