package com.amissible.beans.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String first_name;
    private String last_name;
    private String email;
    private String contact_cc;
    private String contact_cn;
    private String image;
    private String datetimestamp;

    public Customer() {
    }

    public Customer(Long id) {
        this.id = id;
    }

    public Customer(String first_name, String last_name, String email, String contact_cc, String contact_cn, String image) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.contact_cc = contact_cc;
        this.contact_cn = contact_cn;
        this.image = image;
    }

    public Customer(Long id, String first_name, String last_name, String email, String contact_cc, String contact_cn, String image, String datetimestamp) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.contact_cc = contact_cc;
        this.contact_cn = contact_cn;
        this.image = image;
        this.datetimestamp = datetimestamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact_cc() {
        return contact_cc;
    }

    public void setContact_cc(String contact_cc) {
        this.contact_cc = contact_cc;
    }

    public String getContact_cn() {
        return contact_cn;
    }

    public void setContact_cn(String contact_cn) {
        this.contact_cn = contact_cn;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDatetimestamp() {
        return datetimestamp;
    }

    public void setDatetimestamp(String datetimestamp) {
        this.datetimestamp = datetimestamp;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", email='" + email + '\'' +
                ", contact_cc='" + contact_cc + '\'' +
                ", contact_cn='" + contact_cn + '\'' +
                ", image='" + image + '\'' +
                ", datetimestamp='" + datetimestamp + '\'' +
                '}';
    }
}
