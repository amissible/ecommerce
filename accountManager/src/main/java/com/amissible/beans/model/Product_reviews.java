package com.amissible.beans.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Product_reviews {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long user_id;
    private Long order_id;
    private Long product_id;
    private Float rating;
    private String review;
    private String image_path;
    private String datetimestamp;

    public Product_reviews() {
    }

    public Product_reviews(Long id, Long user_id, Long order_id, Long product_id, Float rating, String review, String image_path, String datetimestamp) {
        this.id = id;
        this.user_id = user_id;
        this.order_id = order_id;
        this.product_id = product_id;
        this.rating = rating;
        this.review = review;
        this.image_path = image_path;
        this.datetimestamp = datetimestamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Long getOrder_id() {
        return order_id;
    }

    public void setOrder_id(Long order_id) {
        this.order_id = order_id;
    }

    public Long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public String getDatetimestamp() {
        return datetimestamp;
    }

    public void setDatetimestamp(String datetimestamp) {
        this.datetimestamp = datetimestamp;
    }

    @Override
    public String toString() {
        return "Product_reviews{" +
                "id=" + id +
                ", user_id=" + user_id +
                ", order_id=" + order_id +
                ", product_id=" + product_id +
                ", rating=" + rating +
                ", review='" + review + '\'' +
                ", image_path='" + image_path + '\'' +
                ", datetimestamp='" + datetimestamp + '\'' +
                '}';
    }
}
