package com.amissible.beans.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Order_items {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long order_id;
    private Long product_id;
    private Float price;
    private Integer quantity;
    private Long shipping_id;

    public Order_items() {
    }

    public Order_items(Long id, Long order_id, Long product_id, Float price, Integer quantity, Long shipping_id) {
        this.id = id;
        this.order_id = order_id;
        this.product_id = product_id;
        this.price = price;
        this.quantity = quantity;
        this.shipping_id = shipping_id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrder_id() {
        return order_id;
    }

    public void setOrder_id(Long order_id) {
        this.order_id = order_id;
    }

    public Long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Long getShipping_id() {
        return shipping_id;
    }

    public void setShipping_id(Long shipping_id) {
        this.shipping_id = shipping_id;
    }

    @Override
    public String toString() {
        return "Order_items{" +
                "id=" + id +
                ", order_id=" + order_id +
                ", product_id=" + product_id +
                ", price=" + price +
                ", quantity=" + quantity +
                ", shipping_id=" + shipping_id +
                '}';
    }
}
