package com.amissible.accountManager.controller;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/dataupload")
public class DataUploadController {

    @RequestMapping("/uploadProducts")
    public String uploadCSVFile(@RequestParam("file") MultipartFile file, Model model) {
        String response = "<h1>Upload Started</h1>";
        if (file.isEmpty()) {
            model.addAttribute("message", "Please select a CSV file to upload.");
            model.addAttribute("status", false);
        } else {
            try{
                CSVParser parser = new CSVParserBuilder().withSeparator(',').build();
                Reader br = new BufferedReader(new InputStreamReader(file.getInputStream()));
                CSVReader reader = new CSVReaderBuilder(br).withCSVParser(parser).build();
                List<String[]> rows = reader.readAll();
                for (String[] row : rows) {

                    for (String e : row) {
                        System.out.format("%s ", e);
                    }
                }
            }
            catch (Exception e){

            }
        }
        return response;
    }
}
