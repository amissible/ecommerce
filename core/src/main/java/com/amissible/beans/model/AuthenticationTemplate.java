package com.amissible.beans.model;

import org.springframework.security.core.Authentication;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class AuthenticationTemplate implements Authentication {

    String name;
    private Object principal;
    private Object credentials;
    private boolean authenticated;
    private Collection<GrantedAuthority> authorities;
    private UsernamePasswordAuthenticationToken authenticationToken = null;

    public AuthenticationTemplate() {
    }

    public AuthenticationTemplate(Object principal, Object credentials, Collection<GrantedAuthority> authorities) {
        this.principal = principal;
        this.credentials = credentials;
        this.authorities = authorities;
        authenticationToken = new UsernamePasswordAuthenticationToken(principal, credentials, authorities);
    }

    @Override
    public String toString() {
        return "AuthenticationTemplate{" +
                "name='" + name + '\'' +
                ", principal=" + principal +
                ", credentials=" + credentials +
                ", authenticated=" + authenticated +
                ", authorities=" + authorities +
                ", authenticationToken=" + authenticationToken +
                '}';
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }

    public void setPrincipal(Object principal) {
        this.principal = principal;
    }

    @Override
    public Object getCredentials() {
        return credentials;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    public void setCredentials(Object credentials) {
        this.credentials = credentials;
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection<GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    public UsernamePasswordAuthenticationToken getAuthenticationToken() {
        return authenticationToken;
    }

    public void setAuthenticationToken(UsernamePasswordAuthenticationToken authenticationToken) {
        this.authenticationToken = authenticationToken;
    }
}