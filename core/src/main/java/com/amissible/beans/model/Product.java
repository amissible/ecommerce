package com.amissible.beans.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String product_name;
    private String alias;
    private String brand;
    private String category;
    private String url;
    private String icon1_path;
    private String icon2_path;
    private String image_path;
    private String datetimestamp;
    private String product_status;

    public Product() {
    }

    public Product(Long id) {
        this.id = id;
    }

    public Product(Long id, String product_name, String alias, String brand, String category, String url, String icon1_path, String icon2_path, String image_path, String datetimestamp, String product_status) {
        this.id = id;
        this.product_name = product_name;
        this.alias = alias;
        this.brand = brand;
        this.category = category;
        this.url = url;
        this.icon1_path = icon1_path;
        this.icon2_path = icon2_path;
        this.image_path = image_path;
        this.datetimestamp = datetimestamp;
        this.product_status = product_status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIcon1_path() {
        return icon1_path;
    }

    public void setIcon1_path(String icon1_path) {
        this.icon1_path = icon1_path;
    }

    public String getIcon2_path() {
        return icon2_path;
    }

    public void setIcon2_path(String icon2_path) {
        this.icon2_path = icon2_path;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public String getDatetimestamp() {
        return datetimestamp;
    }

    public void setDatetimestamp(String datetimestamp) {
        this.datetimestamp = datetimestamp;
    }

    public String getProduct_status() {
        return product_status;
    }

    public void setProduct_status(String product_status) {
        this.product_status = product_status;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", product_name='" + product_name + '\'' +
                ", alias='" + alias + '\'' +
                ", brand='" + brand + '\'' +
                ", category='" + category + '\'' +
                ", url='" + url + '\'' +
                ", icon1_path='" + icon1_path + '\'' +
                ", icon2_path='" + icon2_path + '\'' +
                ", image_path='" + image_path + '\'' +
                ", datetimestamp='" + datetimestamp + '\'' +
                ", product_status='" + product_status + '\'' +
                '}';
    }
}
