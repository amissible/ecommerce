package com.amissible.beans.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Cust_role_map {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long cust_id;
    private Long role_id;

    public Cust_role_map() {
    }

    public Cust_role_map(Long id, Long cust_id, Long role_id) {
        this.id = id;
        this.cust_id = cust_id;
        this.role_id = role_id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCust_id() {
        return cust_id;
    }

    public void setCust_id(Long cust_id) {
        this.cust_id = cust_id;
    }

    public Long getRole_id() {
        return role_id;
    }

    public void setRole_id(Long role_id) {
        this.role_id = role_id;
    }

    @Override
    public String toString() {
        return "Cust_role_map{" +
                "id=" + id +
                ", cust_id=" + cust_id +
                ", role_id=" + role_id +
                '}';
    }
}
