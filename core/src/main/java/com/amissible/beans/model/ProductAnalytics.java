package com.amissible.beans.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ProductAnalytics {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long product_id;

    private Long hits;
    private Float average_rating;
    private String search;

    public ProductAnalytics() {
    }

    public ProductAnalytics(Long product_id, Long hits, Float average_rating) {
        this.product_id = product_id;
        this.hits = hits;
        this.average_rating = average_rating;
    }

    public ProductAnalytics(Long hits, Float average_rating) {
        this.hits = hits;
        this.average_rating = average_rating;
    }

    public ProductAnalytics(Long hits, Float average_rating, String search) {
        this.hits = hits;
        this.average_rating = average_rating;
        this.search = search;
    }

    public ProductAnalytics(Long product_id, Long hits, Float average_rating, String search) {
        this.product_id = product_id;
        this.hits = hits;
        this.average_rating = average_rating;
        this.search = search;
    }

    public Long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    public Long getHits() {
        return hits;
    }

    public void setHits(Long hits) {
        this.hits = hits;
    }

    public Float getAverage_rating() {
        return average_rating;
    }

    public void setAverage_rating(Float average_rating) {
        this.average_rating = average_rating;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    @Override
    public String toString() {
        return "Product_analytics{" +
                "product_id=" + product_id +
                ", hits=" + hits +
                ", average_rating=" + average_rating +
                ", search='" + search + '\'' +
                '}';
    }
}
